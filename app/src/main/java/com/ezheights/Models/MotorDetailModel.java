package com.ezheights.Models;

/**
 * Created by Bilal Khan on 13/4/17.
 */

public class MotorDetailModel {

    String newTitle;
    String postedDate;
    String newVerified;
    String newPrice;
    String newColor;
    String newDoors;
    String newBodyCondition;
    String newMechCondition;
    String newMotorTrim;
    String newBodyType;
    String newTotalCylinder;
    String newTransType;
    String newHorsePower;
    String newFuelType;
    String newWarranty;
    String newServiceHistory;
    String newMfgYear;
    String newShortDesc;
    String newLongDesc;
    String newMobileNumber;
    String newWhatsApp;
    String newUserID;
    String PrimaryImage;


    public String getNewTitle() {
        return newTitle;
    }

    public void setNewTitle(String newTitle) {
        this.newTitle = newTitle;
    }

    public String getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }

    public String getNewVerified() {
        return newVerified;
    }

    public void setNewVerified(String newVerified) {
        this.newVerified = newVerified;
    }

    public String getNewPrice() {
        return newPrice;
    }

    public void setNewPrice(String newPrice) {
        this.newPrice = newPrice;
    }

    public String getNewColor() {
        return newColor;
    }

    public void setNewColor(String newColor) {
        this.newColor = newColor;
    }

    public String getNewDoors() {
        return newDoors;
    }

    public void setNewDoors(String newDoors) {
        this.newDoors = newDoors;
    }

    public String getNewBodyCondition() {
        return newBodyCondition;
    }

    public void setNewBodyCondition(String newBodyCondition) {
        this.newBodyCondition = newBodyCondition;
    }

    public String getNewMechCondition() {
        return newMechCondition;
    }

    public void setNewMechCondition(String newMechCondition) {
        this.newMechCondition = newMechCondition;
    }

    public String getNewMotorTrim() {
        return newMotorTrim;
    }

    public void setNewMotorTrim(String newMotorTrim) {
        this.newMotorTrim = newMotorTrim;
    }

    public String getNewBodyType() {
        return newBodyType;
    }

    public void setNewBodyType(String newBodyType) {
        this.newBodyType = newBodyType;
    }

    public String getNewTotalCylinder() {
        return newTotalCylinder;
    }

    public void setNewTotalCylinder(String newTotalCylinder) {
        this.newTotalCylinder = newTotalCylinder;
    }

    public String getNewTransType() {
        return newTransType;
    }

    public void setNewTransType(String newTransType) {
        this.newTransType = newTransType;
    }

    public String getNewHorsePower() {
        return newHorsePower;
    }

    public void setNewHorsePower(String newHorsePower) {
        this.newHorsePower = newHorsePower;
    }

    public String getNewFuelType() {
        return newFuelType;
    }

    public void setNewFuelType(String newFuelType) {
        this.newFuelType = newFuelType;
    }

    public String getNewWarranty() {
        return newWarranty;
    }

    public void setNewWarranty(String newWarranty) {
        this.newWarranty = newWarranty;
    }

    public String getNewServiceHistory() {
        return newServiceHistory;
    }

    public void setNewServiceHistory(String newServiceHistory) {
        this.newServiceHistory = newServiceHistory;
    }

    public String getNewMfgYear() {
        return newMfgYear;
    }

    public void setNewMfgYear(String newMfgYear) {
        this.newMfgYear = newMfgYear;
    }

    public String getNewShortDesc() {
        return newShortDesc;
    }

    public void setNewShortDesc(String newShortDesc) {
        this.newShortDesc = newShortDesc;
    }

    public String getNewLongDesc() {
        return newLongDesc;
    }

    public void setNewLongDesc(String newLongDesc) {
        this.newLongDesc = newLongDesc;
    }

    public String getNewMobileNumber() {
        return newMobileNumber;
    }

    public void setNewMobileNumber(String newMobileNumber) {
        this.newMobileNumber = newMobileNumber;
    }

    public String getNewWhatsApp() {
        return newWhatsApp;
    }

    public void setNewWhatsApp(String newWhatsApp) {
        this.newWhatsApp = newWhatsApp;
    }

    public String getNewUserID() {
        return newUserID;
    }

    public void setNewUserID(String newUserID) {
        this.newUserID = newUserID;
    }

    public String getPrimaryImage() {
        return PrimaryImage;
    }

    public void setPrimaryImage(String primaryImage) {
        PrimaryImage = primaryImage;
    }
}
