package com.ezheights.Models;

import java.util.ArrayList;

/**
 * Created by Bilal Khan on 11/4/17.
 */

public class PropSaleDetailModel {

    String title, price, totalArea, noOfBed, noOfbath, longDes, shortDesc, feature, countryName, towerNum, maplat, maplang, propRef, commName, subCommName, cityName, primaryImage;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTotalArea() {
        return totalArea;
    }

    public void setTotalArea(String totalArea) {
        this.totalArea = totalArea;
    }

    public String getNoOfBed() {
        return noOfBed;
    }

    public void setNoOfBed(String noOfBed) {
        this.noOfBed = noOfBed;
    }

    public String getNoOfbath() {
        return noOfbath;
    }

    public void setNoOfbath(String noOfbath) {
        this.noOfbath = noOfbath;
    }

    public String getLongDes() {
        return longDes;
    }

    public void setLongDes(String longDes) {
        this.longDes = longDes;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getTowerNum() {
        return towerNum;
    }

    public void setTowerNum(String towerNum) {
        this.towerNum = towerNum;
    }


    public String getMaplat() {
        return maplat;
    }

    public void setMaplat(String maplat) {
        this.maplat = maplat;
    }

    public String getMaplang() {
        return maplang;
    }

    public void setMaplang(String maplang) {
        this.maplang = maplang;
    }

    public String getPropRef() {
        return propRef;
    }

    public void setPropRef(String propRef) {
        this.propRef = propRef;
    }

    public String getCommName() {
        return commName;
    }

    public void setCommName(String commName) {
        this.commName = commName;
    }

    public String getSubCommName() {
        return subCommName;
    }

    public void setSubCommName(String subCommName) {
        this.subCommName = subCommName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getPrimaryImage() {
        return primaryImage;
    }

    public void setPrimaryImage(String primaryImage) {
        this.primaryImage = primaryImage;
    }


}
