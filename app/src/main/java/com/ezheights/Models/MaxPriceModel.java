package com.ezheights.Models;

/**
 * Created by Bilal Khan on 11/4/17.
 */

public class MaxPriceModel {

    String maxPriceId;
    String maxPriceName;

    public String getMaxPriceId() {
        return maxPriceId;
    }

    public void setMaxPriceId(String maxPriceId) {
        this.maxPriceId = maxPriceId;
    }

    public String getMaxPriceName() {
        return maxPriceName;
    }

    public void setMaxPriceName(String maxPriceName) {
        this.maxPriceName = maxPriceName;
    }
}
