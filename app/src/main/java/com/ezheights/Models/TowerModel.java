package com.ezheights.Models;

/**
 * Created by Bilal Khan on 11/4/17.
 */

public class TowerModel {

    String towerId;
    String towerName;

    public String getTowerId() {
        return towerId;
    }

    public void setTowerId(String towerId) {
        this.towerId = towerId;
    }

    public String getTowerName() {
        return towerName;
    }

    public void setTowerName(String towerName) {
        this.towerName = towerName;
    }
}
