package com.ezheights.Models;

/**
 * Created by Bilal Khan on 11/4/17.
 */

public class CategoryFilterModel {

    String categoryId;
    String category_name;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }
}
