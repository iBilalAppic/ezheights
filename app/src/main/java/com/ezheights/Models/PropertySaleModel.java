package com.ezheights.Models;

/**
 * Created by Bilal Khan on 6/4/17.
 */

public class PropertySaleModel {

    String property_id;
    String property_name;
    String property_description;
    String property_price;
    String property_no_bed;
    String property_no_bath;
    String property_area;
    String property_image;
    String property_like_status;


    public String getProperty_id() {
        return property_id;
    }

    public void setProperty_id(String property_id) {
        this.property_id = property_id;
    }

    public String getProperty_name() {
        return property_name;
    }

    public void setProperty_name(String property_name) {
        this.property_name = property_name;
    }

    public String getProperty_description() {
        return property_description;
    }

    public void setProperty_description(String property_description) {
        this.property_description = property_description;
    }

    public String getProperty_price() {
        return property_price;
    }

    public void setProperty_price(String property_price) {
        this.property_price = property_price;
    }

    public String getProperty_no_bed() {
        return property_no_bed;
    }

    public void setProperty_no_bed(String property_no_bed) {
        this.property_no_bed = property_no_bed;
    }

    public String getProperty_no_bath() {
        return property_no_bath;
    }

    public void setProperty_no_bath(String property_no_bath) {
        this.property_no_bath = property_no_bath;
    }

    public String getProperty_area() {
        return property_area;
    }

    public void setProperty_area(String property_area) {
        this.property_area = property_area;
    }

    public String getProperty_image() {
        return property_image;
    }

    public void setProperty_image(String property_image) {
        this.property_image = property_image;
    }

    public String getProperty_like_status() {
        return property_like_status;
    }

    public void setProperty_like_status(String property_like_status) {
        this.property_like_status = property_like_status;
    }
}
