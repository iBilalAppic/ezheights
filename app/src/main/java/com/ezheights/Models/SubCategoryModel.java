package com.ezheights.Models;

/**
 * Created by Bilal Khan on 24/4/17.
 */

public class SubCategoryModel {

    String catId;
    String catname;

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getCatname() {
        return catname;
    }

    public void setCatname(String catname) {
        this.catname = catname;
    }
}
