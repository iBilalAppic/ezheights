package com.ezheights.Models;

/**
 * Created by Bilal Khan on 11/4/17.
 */

public class MinAreaModel {

    String minAreaId;
    String minAreaName;

    public String getMinAreaId() {
        return minAreaId;
    }

    public void setMinAreaId(String minAreaId) {
        this.minAreaId = minAreaId;
    }

    public String getMinAreaName() {
        return minAreaName;
    }

    public void setMinAreaName(String minAreaName) {
        this.minAreaName = minAreaName;
    }
}
