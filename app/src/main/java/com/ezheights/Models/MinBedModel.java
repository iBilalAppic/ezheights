package com.ezheights.Models;

/**
 * Created by Bilal Khan on 11/4/17.
 */

public class MinBedModel {

    String minBedId;
    String minBedName;

    public String getMinBedId() {
        return minBedId;
    }

    public void setMinBedId(String minBedId) {
        this.minBedId = minBedId;
    }

    public String getMinBedName() {
        return minBedName;
    }

    public void setMinBedName(String minBedName) {
        this.minBedName = minBedName;
    }
}
