package com.ezheights.Models;

/**
 * Created by Bilal Khan on 11/4/17.
 */

public class MaxBedModel {

    String maxBedId;
    String maxBedName;

    public String getMaxBedId() {
        return maxBedId;
    }

    public void setMaxBedId(String maxBedId) {
        this.maxBedId = maxBedId;
    }

    public String getMaxBedName() {
        return maxBedName;
    }

    public void setMaxBedName(String maxBedName) {
        this.maxBedName = maxBedName;
    }
}
