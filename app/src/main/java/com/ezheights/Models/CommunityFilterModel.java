package com.ezheights.Models;

/**
 * Created by Bilal Khan on 10/4/17.
 */

public class CommunityFilterModel {

    String community_id;
    String community_Name;

    public String getCommunity_id() {
        return community_id;
    }

    public void setCommunity_id(String community_id) {
        this.community_id = community_id;
    }

    public String getCommunity_Name() {
        return community_Name;
    }

    public void setCommunity_Name(String community_Name) {
        this.community_Name = community_Name;
    }
}
