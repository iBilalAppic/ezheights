package com.ezheights.Models;

/**
 * Created by Bilal Khan on 15/4/17.
 */

public class JobMainModel {

    String title;
    String jobid;
    String newexperiance;
    String price;
    String emptype;
    String neweducation;
    String favourite;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getJobid() {
        return jobid;
    }

    public void setJobid(String jobid) {
        this.jobid = jobid;
    }

    public String getNewexperiance() {
        return newexperiance;
    }

    public void setNewexperiance(String newexperiance) {
        this.newexperiance = newexperiance;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getEmptype() {
        return emptype;
    }

    public void setEmptype(String emptype) {
        this.emptype = emptype;
    }

    public String getNeweducation() {
        return neweducation;
    }

    public void setNeweducation(String neweducation) {
        this.neweducation = neweducation;
    }

    public String getFavourite() {
        return favourite;
    }

    public void setFavourite(String favourite) {
        this.favourite = favourite;
    }
}
