package com.ezheights.Models;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by Bilal Khan on 21/4/17.
 */

public class AddPostCategory  {

    String catId;
    String catName;
    JSONArray subCat;

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public JSONArray getSubCat() {
        return subCat;
    }

    public void setSubCat(JSONArray subCat) {
        this.subCat = subCat;
    }
}
