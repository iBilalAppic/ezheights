package com.ezheights.Models;

/**
 * Created by Bilal Khan on 11/4/17.
 */

public class MinPriceModel {

    String minPriceId;
    String minPriceName;

    public String getMinPriceId() {
        return minPriceId;
    }

    public void setMinPriceId(String minPriceId) {
        this.minPriceId = minPriceId;
    }

    public String getMinPriceName() {
        return minPriceName;
    }

    public void setMinPriceName(String minPriceName) {
        this.minPriceName = minPriceName;
    }
}
