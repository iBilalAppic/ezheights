package com.ezheights.Models;

/**
 * Created by Bilal Khan on 13/4/17.
 */

public class ClassifiedModel {

    String ItemID;
    String title;
    String price;
    String age;
    String condition;
    String ItemUseage;
    String image;
    String favourite;

    public String getItemID() {
        return ItemID;
    }

    public void setItemID(String itemID) {
        ItemID = itemID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getItemUseage() {
        return ItemUseage;
    }

    public void setItemUseage(String itemUseage) {
        ItemUseage = itemUseage;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFavourite() {
        return favourite;
    }

    public void setFavourite(String favourite) {
        this.favourite = favourite;
    }
}
