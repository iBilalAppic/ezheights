package com.ezheights.Models;

/**
 * Created by Bilal Khan on 11/4/17.
 */

public class CityModel {
    String cityId;
    String cityName;

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
