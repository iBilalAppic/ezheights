package com.ezheights.Models;

/**
 * Created by Bilal Khan on 13/4/17.
 */

public class MotorSaleModel {

    String motor_id;
    String motor_title;
    String motor_verfied_status;
    String motor_price;
    String manufac_Year;
    String motor_Color;
    String No_Of_Door;
    String motor_image;
    String motor_fav;

    public String getMotor_id() {
        return motor_id;
    }

    public void setMotor_id(String motor_id) {
        this.motor_id = motor_id;
    }

    public String getMotor_title() {
        return motor_title;
    }

    public void setMotor_title(String motor_title) {
        this.motor_title = motor_title;
    }

    public String getMotor_verfied_status() {
        return motor_verfied_status;
    }

    public void setMotor_verfied_status(String motor_verfied_status) {
        this.motor_verfied_status = motor_verfied_status;
    }

    public String getMotor_price() {
        return motor_price;
    }

    public void setMotor_price(String motor_price) {
        this.motor_price = motor_price;
    }

    public String getManufac_Year() {
        return manufac_Year;
    }

    public void setManufac_Year(String manufac_Year) {
        this.manufac_Year = manufac_Year;
    }

    public String getMotor_Color() {
        return motor_Color;
    }

    public void setMotor_Color(String motor_Color) {
        this.motor_Color = motor_Color;
    }

    public String getNo_Of_Door() {
        return No_Of_Door;
    }

    public void setNo_Of_Door(String no_Of_Door) {
        No_Of_Door = no_Of_Door;
    }

    public String getMotor_image() {
        return motor_image;
    }

    public void setMotor_image(String motor_image) {
        this.motor_image = motor_image;
    }

    public String getMotor_fav() {
        return motor_fav;
    }

    public void setMotor_fav(String motor_fav) {
        this.motor_fav = motor_fav;
    }
}
