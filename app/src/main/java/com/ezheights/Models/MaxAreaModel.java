package com.ezheights.Models;

/**
 * Created by Bilal Khan on 11/4/17.
 */

public class MaxAreaModel {

    String maxAreaId;
    String maxAreaName;

    public String getMaxAreaId() {
        return maxAreaId;
    }

    public void setMaxAreaId(String maxAreaId) {
        this.maxAreaId = maxAreaId;
    }

    public String getMaxAreaName() {
        return maxAreaName;
    }

    public void setMaxAreaName(String maxAreaName) {
        this.maxAreaName = maxAreaName;
    }
}
