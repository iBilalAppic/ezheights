package com.ezheights.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ezheights.Activity.Classified.ClassifiedActivity;
import com.ezheights.Activity.Job.JobActivity;
import com.ezheights.Activity.Motor.MotorSaleActivity;
import com.ezheights.Activity.Property.PropertyRentActivity;
import com.ezheights.Activity.Property.PropertySaleActivity;
import com.ezheights.R;

public class SearchActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView img_Close;
    private EditText edt_Search;
    private TextView tv_SearchInPropertySale, tv_SearchInPropertyRent, tv_SearchInMotors, tv_SearchInClassifieds, tv_SearchInJobs;
    private View view1, view2, view3, view4, view5;
    private Activity mContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        mContext = SearchActivity.this;

        init();

    }

    private void init() {

        img_Close = (ImageView) findViewById(R.id.img_Close);
        edt_Search = (EditText) findViewById(R.id.edt_Search);

        tv_SearchInPropertySale = (TextView) findViewById(R.id.tv_SearchInPropertySale);
        tv_SearchInPropertyRent = (TextView) findViewById(R.id.tv_SearchInPropertyRent);
        tv_SearchInMotors = (TextView) findViewById(R.id.tv_SearchInMotors);
        tv_SearchInClassifieds = (TextView) findViewById(R.id.tv_SearchInClassifieds);
        tv_SearchInJobs = (TextView) findViewById(R.id.tv_SearchInJobs);

        view1 = (View) findViewById(R.id.view1);
        view2 = (View) findViewById(R.id.view2);
        view3 = (View) findViewById(R.id.view3);
        view4 = (View) findViewById(R.id.view4);
        view5 = (View) findViewById(R.id.view5);


        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                tv_SearchInPropertySale.setVisibility(View.VISIBLE);
                view1.setVisibility(View.VISIBLE);
            }
        }, 10);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                tv_SearchInPropertyRent.setVisibility(View.VISIBLE);
                view2.setVisibility(View.VISIBLE);
            }
        }, 150);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                tv_SearchInMotors.setVisibility(View.VISIBLE);
                view3.setVisibility(View.VISIBLE);
            }
        }, 300);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                tv_SearchInClassifieds.setVisibility(View.VISIBLE);
                view4.setVisibility(View.VISIBLE);
            }
        }, 450);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                tv_SearchInJobs.setVisibility(View.VISIBLE);
                view5.setVisibility(View.VISIBLE);
            }
        }, 600);


        tv_SearchInPropertySale.setOnClickListener(this);
        tv_SearchInPropertyRent.setOnClickListener(this);
        tv_SearchInMotors.setOnClickListener(this);
        tv_SearchInClassifieds.setOnClickListener(this);
        tv_SearchInJobs.setOnClickListener(this);
        img_Close.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if (v == img_Close) {
            edt_Search.setText("");

        } else if (v == tv_SearchInPropertySale) {

            Intent intent = new Intent(mContext, PropertySaleActivity.class);
            intent.putExtra("Search", edt_Search.getText().toString());
            startActivity(intent);
        } else if (v == tv_SearchInPropertyRent) {
            Intent intent = new Intent(mContext, PropertyRentActivity.class);
            intent.putExtra("Search", edt_Search.getText().toString());
            startActivity(intent);
        } else if (v == tv_SearchInMotors) {
            Intent intent = new Intent(mContext, MotorSaleActivity.class);
            intent.putExtra("Search", edt_Search.getText().toString());
            startActivity(intent);
        } else if (v == tv_SearchInClassifieds) {
            Intent intent = new Intent(mContext, ClassifiedActivity.class);
            intent.putExtra("Search", edt_Search.getText().toString());
            startActivity(intent);
        } else if (v == tv_SearchInJobs) {
            Intent intent = new Intent(mContext, JobActivity.class);
            intent.putExtra("Search", edt_Search.getText().toString());
            startActivity(intent);
        }
    }
}
