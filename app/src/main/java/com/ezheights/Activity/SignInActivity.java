package com.ezheights.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ezheights.R;
import com.ezheights.Services.RequestGenerator;
import com.ezheights.Services.ResponseListener;
import com.ezheights.Utils.AppConstants;
import com.ezheights.Utils.AppPreferences;

import org.json.JSONException;
import org.json.JSONObject;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView txt_SignUp, txt_ForgotPassword;
    Button signIn_btn;
    private EditText edt_EmailId, edt_Pass;
    private AppPreferences appPreferance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        appPreferance = new AppPreferences();

        init();
    }

    private void init() {

        txt_SignUp = (TextView) findViewById(R.id.txt_SignUp);
        txt_ForgotPassword = (TextView) findViewById(R.id.txt_ForgotPassword);

        edt_EmailId = (EditText) findViewById(R.id.edt_EmailId);
        edt_Pass = (EditText) findViewById(R.id.edt_Pass);

        signIn_btn = (Button) findViewById(R.id.signIn_btn);

        signIn_btn.setOnClickListener(this);
        txt_SignUp.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v == txt_SignUp) {
            startActivity(new Intent(SignInActivity.this, SignUpActivity.class));
            finish();
        }
        if (v == signIn_btn) {
            boolean flag = true;
            if (edt_EmailId.getText().toString().equals("")) {
                flag = false;
                edt_EmailId.setError("Enter Email Id");
            } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(edt_EmailId.getText().toString()).matches()) {
                flag = false;
                edt_EmailId.setError("Enter Valid Email Id");
            } else if (edt_Pass.getText().toString().equals("")) {
                flag = false;
                edt_Pass.setError("Enter Password");
            } else {
                SignIn();
            }

        }
    }

    private void SignIn() {

        String UrlLogin = AppConstants.Login + "&txt_username=" + edt_EmailId.getText().toString() + "&txt_pass=" + edt_Pass.getText().toString();

        Log.e("Login Request", UrlLogin);

        RequestGenerator.makeGetRequest(SignInActivity.this, UrlLogin, true, new ResponseListener() {
            @Override
            public void onError(VolleyError error) {
                Toast.makeText(SignInActivity.this, "Some Problem Occured", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(String string) throws JSONException {
                if (!string.equals("")) {

                    Log.e("Login Response", string);

                    JSONObject jsonObject1 = new JSONObject(string);
                    String status = jsonObject1.getString("status");
                    String msg = jsonObject1.getString("msg");
                    if (status.equals("success")) {
                        appPreferance.setPreferences(getApplicationContext(), AppConstants.isLogin, "Login");
                        appPreferance.setPreferences(getApplicationContext(), AppConstants.emailId, jsonObject1.getString("email"));
                        Toast.makeText(SignInActivity.this, msg, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(SignInActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(SignInActivity.this, msg, Toast.LENGTH_SHORT).show();

                    }
                }
            }
        });

    }
}
