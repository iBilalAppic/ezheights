package com.ezheights.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ezheights.R;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tv_Skip, tv_FacebookLogin, tv_EmailLogin;

    private LoginButton login_button;
    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();

//        login_button.setReadPermissions(Arrays.asList("public_profile, email, user_birthday, user_friends"));
//        callbackManager = CallbackManager.Factory.create();
    }

    private void init() {

        tv_Skip = (TextView) findViewById(R.id.tv_Skip);
        tv_EmailLogin = (TextView) findViewById(R.id.tv_EmailLogin);
        tv_FacebookLogin = (TextView) findViewById(R.id.tv_FacebookLogin);
        login_button = (LoginButton) findViewById(R.id.login_button);


        tv_Skip.setOnClickListener(this);
        tv_EmailLogin.setOnClickListener(this);
        tv_FacebookLogin.setOnClickListener(this);
        login_button.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v == tv_Skip) {
            onBackPressed();
        }

        if (v == tv_FacebookLogin) {
            login_button.callOnClick();

        }

        if (v == login_button) {
            loginWithFacebook();

        }

        if (v == tv_EmailLogin){
            startActivity(new Intent(LoginActivity.this, SignInActivity.class));
        }
    }

    private void loginWithFacebook() {

        login_button.setReadPermissions(Arrays.asList("public_profile, email, user_birthday, user_friends"));
        callbackManager = CallbackManager.Factory.create();

        login_button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {

                                Log.d("Facebook Responce", object.toString());
                                try {
                                    String id = object.getString("id");
                                    String name = object.getString("name");
                                    String email = object.getString("email");
//                                    String birthday = object.getString("birthday");
                                    String imageurl = "https://graph.facebook.com/" + id + "/picture?type=large";

                                    Log.d("Facebook Responce1", "Name - " + name + " EmailId - " + email + " Id - " + id + " Image - " + imageurl);
//                                    Toast.makeText(getApplicationContext(), "Facebook Sign Up Successful \n" + "Welcome " + name + "!", Toast.LENGTH_SHORT).show();

//                                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
//                                    intent.putExtra("Token", id);
//                                    intent.putExtra("Name", name);
//                                    intent.putExtra("Email", email);
//                                    startActivity(intent);
//                                    finish();


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        });


                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email");
                request.setParameters(parameters);
                request.executeAsync();


/**
 * AccessTokenTracker to manage logout
 */
                accessTokenTracker = new AccessTokenTracker() {
                    @Override
                    protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                        if (currentAccessToken == null) {
                            //tv_profile_name.setText("");
                            // iv_profile_pic.setImageResource(R.drawable.maleicon);

                        }
                    }
                };
            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext(), "Facebook sign up Cancelled", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getApplicationContext(), "Facebook sign up Failed", Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

}
