package com.ezheights.Activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ezheights.Activity.Classified.ClassifiedActivity;
import com.ezheights.Activity.Job.JobActivity;
import com.ezheights.Activity.Motor.AfterFilteredMotor;
import com.ezheights.Activity.Motor.MotorSaleActivity;
import com.ezheights.Activity.Property.PropertyRentActivity;
import com.ezheights.Activity.Property.PropertySaleActivity;
import com.ezheights.Adapter.AddPostCategories;
import com.ezheights.Adapter.ClassifiedsAdapter;
import com.ezheights.Adapter.MotorsAdapter;
import com.ezheights.Adapter.NavDrawerAdapter;
import com.ezheights.Adapter.PropertyRentAdapter;
import com.ezheights.Adapter.PropertySaleAdapter;
import com.ezheights.AnimatorUtils;
import com.ezheights.Models.AddPostCategory;
import com.ezheights.Models.ClassifiedModel;
import com.ezheights.Models.MotorSaleModel;
import com.ezheights.Models.NavDrawerModel;
import com.ezheights.Models.PropertyRentModel;
import com.ezheights.Models.PropertySaleModel;
import com.ezheights.Models.SubCategoryModel;
import com.ezheights.R;
import com.ezheights.Services.RequestGenerator;
import com.ezheights.Services.ResponseListener;
import com.ezheights.Utils.AppConstants;
import com.ezheights.Utils.AppPreferences;
import com.ogaclejapan.arclayout.ArcLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by NileshM on 21/3/17.
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private RelativeLayout drawerPane;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;
    private ArrayList<NavDrawerModel> mNavDrawerModel;

    private ArrayList<PropertySaleModel> arrayListSale = new ArrayList<>();
    private ArrayList<PropertyRentModel> arrayListRent = new ArrayList<>();
    private ArrayList<MotorSaleModel> arrayListMotor = new ArrayList<>();
    private ArrayList<ClassifiedModel> arrayListClassi = new ArrayList<>();

    RecyclerView recyclerViewCategory;
    LinearLayoutManager linearLayoutManager;
    private ArrayList<AddPostCategory> arrayListPostClassifiedAdd = new ArrayList<>();

    private ArrayList<SubCategoryModel> subCategoryModelArrayList = new ArrayList<>();
    private NavDrawerAdapter mAdapter;
    private static long back_pressed;
    View menuLayout;
    ArcLayout arcLayout;

    CircleImageView addProp, addMotor, addClassified, addJob;
    private RecyclerView recyclerView1, recyclerView2, recyclerView3, recyclerView4;
    private PropertySaleAdapter mAdapter1;
    private PropertyRentAdapter mAdapter2;
    private ClassifiedsAdapter mAdapter3;
    private MotorsAdapter mAdapter4;
    private AddPostCategories addPostCategoriesAdapter;

    TextView logout;

    private Button btn_SeeMore1, btn_SeeMore2, btn_SeeMore3, btn_SeeMore4;
    private LinearLayout lL_PropertySale, lL_PropertyRent, lL_Motors, lL_Classifieds, lL_Jobs;
    private RelativeLayout rL_Search;
    private FloatingActionButton fab_Login;
    android.support.v7.app.AlertDialog customAlertDialog;

    AppPreferences appPreferance;
    String isLogin, emailId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        appPreferance = new AppPreferences();
        isLogin = appPreferance.getPreferences(MainActivity.this, AppConstants.isLogin);
        emailId = appPreferance.getPreferences(MainActivity.this, AppConstants.emailId);

        init();
//        generateFacebookKeyHash();
    }

    private void init() {

        recyclerView1 = (RecyclerView) findViewById(R.id.recyclerView1);
        recyclerView2 = (RecyclerView) findViewById(R.id.recyclerView2);
        recyclerView3 = (RecyclerView) findViewById(R.id.recyclerView3);
        recyclerView4 = (RecyclerView) findViewById(R.id.recyclerView4);

        lL_PropertySale = (LinearLayout) findViewById(R.id.lL_PropertySale);
        lL_PropertyRent = (LinearLayout) findViewById(R.id.lL_PropertyRent);
        lL_Motors = (LinearLayout) findViewById(R.id.lL_Motors);
        lL_Classifieds = (LinearLayout) findViewById(R.id.lL_Classifieds);
        lL_Jobs = (LinearLayout) findViewById(R.id.lL_Jobs);

        rL_Search = (RelativeLayout) findViewById(R.id.rL_Search);
        fab_Login = (FloatingActionButton) findViewById(R.id.fab_Login);

        addProp = (CircleImageView) findViewById(R.id.propertyFab);
        addMotor = (CircleImageView) findViewById(R.id.motorFab);
        addClassified = (CircleImageView) findViewById(R.id.classifiedFab);
        addJob = (CircleImageView) findViewById(R.id.jobFab);

        logout = (TextView) findViewById(R.id.logout);

        menuLayout = findViewById(R.id.menu_layout);
        arcLayout = (ArcLayout) findViewById(R.id.arc_layout);

        btn_SeeMore1 = (Button) findViewById(R.id.btn_SeeMore1);
        btn_SeeMore2 = (Button) findViewById(R.id.btn_SeeMore2);
        btn_SeeMore3 = (Button) findViewById(R.id.btn_SeeMore3);
        btn_SeeMore4 = (Button) findViewById(R.id.btn_SeeMore4);

        LinearLayoutManager mLayoutManager1 = new LinearLayoutManager(this);
        recyclerView1.setLayoutManager(mLayoutManager1);
        mLayoutManager1.setOrientation(LinearLayoutManager.HORIZONTAL);


        LinearLayoutManager mLayoutManager2 = new LinearLayoutManager(this);
        recyclerView2.setLayoutManager(mLayoutManager2);
        mLayoutManager2.setOrientation(LinearLayoutManager.HORIZONTAL);


        LinearLayoutManager mLayoutManager3 = new LinearLayoutManager(this);
        recyclerView3.setLayoutManager(mLayoutManager3);
        mLayoutManager3.setOrientation(LinearLayoutManager.HORIZONTAL);


        LinearLayoutManager mLayoutManager4 = new LinearLayoutManager(this);
        recyclerView4.setLayoutManager(mLayoutManager4);
        mLayoutManager4.setOrientation(LinearLayoutManager.HORIZONTAL);


        lL_PropertySale.setOnClickListener(this);
        lL_PropertyRent.setOnClickListener(this);
        lL_Motors.setOnClickListener(this);
        lL_Classifieds.setOnClickListener(this);
        lL_Jobs.setOnClickListener(this);
        rL_Search.setOnClickListener(this);
        fab_Login.setOnClickListener(this);
        addProp.setOnClickListener(this);
        addMotor.setOnClickListener(this);
        addClassified.setOnClickListener(this);
        addJob.setOnClickListener(this);
        logout.setOnClickListener(this);
        btn_SeeMore1.setOnClickListener(this);
        btn_SeeMore2.setOnClickListener(this);
        btn_SeeMore3.setOnClickListener(this);
        btn_SeeMore4.setOnClickListener(this);

        menuLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideMenu();
            }
        });

        getAllHomeData();

    }

    private void getAllHomeData() {

        RequestGenerator.makeGetRequest(MainActivity.this, AppConstants.HomeData, true, new ResponseListener() {
            @Override
            public void onError(VolleyError error) {
                Toast.makeText(MainActivity.this, "Some Problem Occured", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(String string) throws JSONException {
                if (!string.equals("")) {
                    JSONObject jsonObject = new JSONObject(string);
                    Log.e("response", string);
                    JSONArray jsonproSale = jsonObject.getJSONArray("sale_content");
                    arrayListSale.clear();
                    for (int i = 0; i < jsonproSale.length(); i++) {
                        JSONObject jsonObject2 = jsonproSale.getJSONObject(i);

                        PropertySaleModel propertySaleModel = new PropertySaleModel();
                        propertySaleModel.setProperty_id(jsonObject2.getString("ItemID"));
                        propertySaleModel.setProperty_name(jsonObject2.getString("newTitle"));
                        propertySaleModel.setProperty_image(jsonObject2.getString("image"));

                        arrayListSale.add(propertySaleModel);

                    }
                    mAdapter1 = new PropertySaleAdapter(MainActivity.this, arrayListSale);
                    recyclerView1.setAdapter(mAdapter1);


                    JSONArray jsonproRent = jsonObject.getJSONArray("rent_content");
                    arrayListRent.clear();
                    for (int i = 0; i < jsonproRent.length(); i++) {
                        JSONObject jsonObject2 = jsonproRent.getJSONObject(i);

                        PropertyRentModel propertyRentModel = new PropertyRentModel();
                        propertyRentModel.setProperty_id(jsonObject2.getString("ItemID"));
                        propertyRentModel.setProperty_name(jsonObject2.getString("title"));
                        propertyRentModel.setProperty_image(jsonObject2.getString("image"));


                        arrayListRent.add(propertyRentModel);

                    }
                    mAdapter2 = new PropertyRentAdapter(MainActivity.this, arrayListRent);
                    recyclerView2.setAdapter(mAdapter2);

                    JSONArray jsonproClassi = jsonObject.getJSONArray("classifieds_content");
                    arrayListClassi.clear();
                    for (int i = 0; i < jsonproClassi.length(); i++) {
                        JSONObject jsonObject2 = jsonproClassi.getJSONObject(i);

                        ClassifiedModel classifiedModel = new ClassifiedModel();
                        classifiedModel.setItemID(jsonObject2.getString("ItemID"));
                        classifiedModel.setTitle(jsonObject2.getString("title"));
                        classifiedModel.setImage(jsonObject2.getString("image"));


                        arrayListClassi.add(classifiedModel);

                    }
                    mAdapter3 = new ClassifiedsAdapter(MainActivity.this, arrayListClassi);
                    recyclerView3.setAdapter(mAdapter3);

                    JSONArray jsonproMotor = jsonObject.getJSONArray("motors_content");
                    arrayListMotor.clear();
                    for (int i = 0; i < jsonproMotor.length(); i++) {
                        JSONObject jsonObject2 = jsonproMotor.getJSONObject(i);

                        MotorSaleModel motorSaleModel = new MotorSaleModel();
                        motorSaleModel.setMotor_id(jsonObject2.getString("ItemID"));
                        motorSaleModel.setMotor_title(jsonObject2.getString("title"));
                        motorSaleModel.setMotor_image(jsonObject2.getString("image"));


                        arrayListMotor.add(motorSaleModel);

                    }
                    mAdapter4 = new MotorsAdapter(MainActivity.this, arrayListMotor);
                    recyclerView4.setAdapter(mAdapter4);
                }
            }
        });

    }


    @Override
    public void onClick(View v) {

        if (v == lL_PropertySale) {
            Intent intent = new Intent(MainActivity.this, PropertySaleActivity.class);
            startActivity(intent);

        }


        if (v == lL_PropertyRent) {
            Intent intent = new Intent(MainActivity.this, PropertyRentActivity.class);
            startActivity(intent);

        }
        if (v == lL_Motors) {
            Intent intent = new Intent(MainActivity.this, MotorSaleActivity.class);
            startActivity(intent);

        }

        if (v == lL_Classifieds) {
            Intent intent = new Intent(MainActivity.this, ClassifiedActivity.class);
            startActivity(intent);

        }

        if (v == lL_Jobs) {
            Intent intent = new Intent(MainActivity.this, JobActivity.class);
            startActivity(intent);

        }
        if (v == rL_Search) {
            Intent intent = new Intent(MainActivity.this, SearchActivity.class);
            startActivity(intent);

        }

        if (v == fab_Login) {

            if (!isLogin.equals("")) {
                OpenDialogForAdd(v);
            } else {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        }
        if (v == addProp) {
            addPostProperty();
        }
        if (v == addMotor) {

            addPostMotor();
        }
        if (v == addClassified) {
            addPostClassified();
//            getAllCategories();

        }
        if (v == addJob) {
            addPostJob();
        }
        if (v == logout) {
            appPreferance.clearSharedPreference(getApplicationContext());
            Intent intent = new Intent(MainActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        if (v == btn_SeeMore1) {
            Intent intent = new Intent(MainActivity.this, PropertySaleActivity.class);
            startActivity(intent);

        }
        if (v == btn_SeeMore2) {
            Intent intent = new Intent(MainActivity.this, PropertyRentActivity.class);
            startActivity(intent);
        }
        if (v == btn_SeeMore3) {
            Intent intent = new Intent(MainActivity.this, ClassifiedActivity.class);
            startActivity(intent);
        }
        if (v == btn_SeeMore4) {
            Intent intent = new Intent(MainActivity.this, MotorSaleActivity.class);
            startActivity(intent);
        }

    }


    private void OpenDialogForAdd(View v) {
        if (v.isSelected()) {
            hideMenu();
        } else {
            showMenu();
        }
        v.setSelected(!v.isSelected());

    }


    private void addPostProperty() {
        Toast.makeText(MainActivity.this, "Add Property", Toast.LENGTH_SHORT).show();
    }


    private void addPostMotor() {
        Toast.makeText(MainActivity.this, "Add Motor", Toast.LENGTH_SHORT).show();
    }


    private void addPostClassified() {
        hideMenu();
        final Dialog alertDialog = new Dialog(this);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.add_classified_post);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        alertDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);

        final TextView freeAdd, featureAdd, premiumAdd, dynamicAdd, chooseTyoeHead, chooseCatHead;
        final FrameLayout frameLayout;

        final LinearLayout layoutAddType;
        final ImageView btn_cancel, cancel_Dialog;

        layoutAddType = (LinearLayout) alertDialog.findViewById(R.id.layoutAddType);
        freeAdd = (TextView) alertDialog.findViewById(R.id.adFree);
        featureAdd = (TextView) alertDialog.findViewById(R.id.adFeature);
        premiumAdd = (TextView) alertDialog.findViewById(R.id.adPremium);
        dynamicAdd = (TextView) alertDialog.findViewById(R.id.adDyna);
        btn_cancel = (ImageView) alertDialog.findViewById(R.id.btn_cancel);
        cancel_Dialog = (ImageView) alertDialog.findViewById(R.id.cancel_Dialog);
        chooseTyoeHead = (TextView) alertDialog.findViewById(R.id.chooseAddType);
        chooseCatHead = (TextView) alertDialog.findViewById(R.id.chooseCat);


        frameLayout = (FrameLayout) alertDialog.findViewById(R.id.frameLayout);
        recyclerViewCategory = (RecyclerView) alertDialog.findViewById(R.id.categoryAdd);


        cancel_Dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.cancel();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseTyoeHead.setVisibility(View.VISIBLE);
                frameLayout.setVisibility(View.GONE);
                layoutAddType.setVisibility(View.VISIBLE);
                cancel_Dialog.setVisibility(View.VISIBLE);

            }
        });

        freeAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel_Dialog.setVisibility(View.GONE);
                chooseTyoeHead.setVisibility(View.GONE);
                frameLayout.setVisibility(View.VISIBLE);
                layoutAddType.setVisibility(View.GONE);
                getAllCategories();

            }
        });
        featureAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel_Dialog.setVisibility(View.GONE);
                chooseTyoeHead.setVisibility(View.GONE);
                frameLayout.setVisibility(View.VISIBLE);
                layoutAddType.setVisibility(View.GONE);
                getAllCategories();
            }
        });
        premiumAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel_Dialog.setVisibility(View.GONE);
                chooseTyoeHead.setVisibility(View.GONE);
                frameLayout.setVisibility(View.VISIBLE);
                layoutAddType.setVisibility(View.GONE);
                getAllCategories();
            }
        });
        dynamicAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel_Dialog.setVisibility(View.GONE);
                chooseTyoeHead.setVisibility(View.GONE);
                frameLayout.setVisibility(View.VISIBLE);
                layoutAddType.setVisibility(View.GONE);
                getAllCategories();
            }
        });


        alertDialog.show();

    }

    private void getAllCategories() {
        try {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("id", "4");

            RequestGenerator.makePostRequest(MainActivity.this, AppConstants.GetCategoriesForAddPost, jsonObject, true, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {
                    Toast.makeText(MainActivity.this, "Some Problem Occured", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(String string) throws JSONException {

                    if (!string.equals("")) {
                        Log.e("Response", string);
                        JSONObject jsonObject1 = new JSONObject(string);
                        JSONArray jsonArray_ClasssiCat = jsonObject1.getJSONArray("category");
                        arrayListPostClassifiedAdd.clear();
                        for (int i = 0; i < jsonArray_ClasssiCat.length(); i++) {
                            JSONObject jsonObject2 = jsonArray_ClasssiCat.getJSONObject(i);

                            AddPostCategory addPostCategoriesmodel = new AddPostCategory();
                            addPostCategoriesmodel.setCatId(jsonObject2.getString("cat_id"));
                            addPostCategoriesmodel.setCatName(jsonObject2.getString("cat_name"));
                            addPostCategoriesmodel.setSubCat(jsonObject2.getJSONArray("subcategory"));


                            arrayListPostClassifiedAdd.add(addPostCategoriesmodel);

                        }
                        recyclerViewCategory.setLayoutManager(new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false));
                        addPostCategoriesAdapter = new AddPostCategories(MainActivity.this, arrayListPostClassifiedAdd );
                        recyclerViewCategory.setAdapter(addPostCategoriesAdapter);

                    }
                }
            });

        } catch (JSONException e) {
        }
    }

    private void addPostJob() {
        Toast.makeText(MainActivity.this, "Add Job", Toast.LENGTH_SHORT).show();

    }


    private void showMenu() {
        menuLayout.setVisibility(View.VISIBLE);

        List<Animator> animList = new ArrayList<>();

        for (int i = 0, len = arcLayout.getChildCount(); i < len; i++) {
            animList.add(createShowItemAnimator(arcLayout.getChildAt(i)));
        }

        AnimatorSet animSet = new AnimatorSet();
        animSet.setDuration(400);
        animSet.setInterpolator(new OvershootInterpolator());
        animSet.playTogether(animList);
        animSet.start();
    }


    private void hideMenu() {

        List<Animator> animList = new ArrayList<>();

        for (int i = arcLayout.getChildCount() - 1; i >= 0; i--) {
            animList.add(createHideItemAnimator(arcLayout.getChildAt(i)));
        }

        AnimatorSet animSet = new AnimatorSet();
        animSet.setDuration(400);
        animSet.setInterpolator(new AnticipateInterpolator());
        animSet.playTogether(animList);
        animSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                menuLayout.setVisibility(View.INVISIBLE);
            }
        });
        animSet.start();

    }


    private Animator createShowItemAnimator(View item) {

        float dx = fab_Login.getX() - item.getX();
        float dy = fab_Login.getY() - item.getY();

        item.setRotation(0f);
        item.setTranslationX(dx);
        item.setTranslationY(dy);

        Animator anim = ObjectAnimator.ofPropertyValuesHolder(
                item,
                AnimatorUtils.rotation(0f, 720f),
                AnimatorUtils.translationX(dx, 0f),
                AnimatorUtils.translationY(dy, 0f)
        );

        return anim;
    }

    private Animator createHideItemAnimator(final View item) {
        float dx = fab_Login.getX() - item.getX();
        float dy = fab_Login.getY() - item.getY();

        Animator anim = ObjectAnimator.ofPropertyValuesHolder(
                item,
                AnimatorUtils.rotation(720f, 0f),
                AnimatorUtils.translationX(0f, dx),
                AnimatorUtils.translationY(0f, dy)
        );

        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                item.setTranslationX(0f);
                item.setTranslationY(0f);
            }
        });

        return anim;
    }

    public void generateFacebookKeyHash() {
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.ezheights",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if (back_pressed + 2000 > System.currentTimeMillis()) {

            finish();
            super.onBackPressed();
        } else

        {
            hideMenu();
            Snackbar snackbar = Snackbar.make(fab_Login, "Press once again to close app.", Snackbar.LENGTH_SHORT);
            View snackbarView = snackbar.getView();
            TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbarView.setBackgroundColor(Color.BLACK);
            snackbar.show();
            back_pressed = System.currentTimeMillis();

        }
    }
}
