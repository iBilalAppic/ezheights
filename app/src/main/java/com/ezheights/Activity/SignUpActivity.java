package com.ezheights.Activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ezheights.Activity.Motor.AfterFilteredMotor;
import com.ezheights.R;
import com.ezheights.Services.RequestGenerator;
import com.ezheights.Services.ResponseListener;
import com.ezheights.Utils.AppConstants;
import com.ezheights.Utils.AppPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by NileshM on 27/3/17.
 */

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView txt_SignIn;
    Button signUp_btn;
    private EditText edt_Title, edt_FName, edt_LName, edt_Gender, edt_Nationality, edt_DOB, edt_EmailId, edt_MobileNo, edt_Pass, edt_CPass;
    private Spinner sp_Title, sp_Gender, sp_Nationality;
    private CheckBox checkboxTerm;

    private DatePickerDialog dateOFEvnet;
    private SimpleDateFormat dateFormatter;
    private String[] title = {"Title", "Mr.", "Mrs.", "Miss"};
    private String[] gender = {"Gender", "Male", "Female"};
    private String[] nationality = {"Nationality", "Dubai", "India"};
    private AppPreferences appPreferance;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        appPreferance = new AppPreferences();

        init();

    }

    private void init() {

        signUp_btn = (Button) findViewById(R.id.signUp_btn);

        txt_SignIn = (TextView) findViewById(R.id.txt_SignIn);


        edt_Title = (EditText) findViewById(R.id.edt_Title);
        edt_FName = (EditText) findViewById(R.id.edt_FName);
        edt_LName = (EditText) findViewById(R.id.edt_LName);
        edt_DOB = (EditText) findViewById(R.id.edt_DOB);
        edt_Gender = (EditText) findViewById(R.id.edt_Gender);
        edt_Nationality = (EditText) findViewById(R.id.edt_Nationality);
        edt_EmailId = (EditText) findViewById(R.id.edt_EmailId);
        edt_MobileNo = (EditText) findViewById(R.id.edt_MobileNo);
        edt_Pass = (EditText) findViewById(R.id.edt_Pass);
        edt_CPass = (EditText) findViewById(R.id.edt_CPass);

        sp_Title = (Spinner) findViewById(R.id.sp_Title);
        sp_Gender = (Spinner) findViewById(R.id.sp_Gender);
        sp_Nationality = (Spinner) findViewById(R.id.sp_Nationality);

        checkboxTerm = (CheckBox) findViewById(R.id.checkReq);

        ArrayAdapter<String> titleAdapter = new ArrayAdapter<String>(SignUpActivity.this, R.layout.item_gender_type, title);
        sp_Title.setAdapter(titleAdapter);

        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(SignUpActivity.this, R.layout.item_gender_type, gender);
        sp_Gender.setAdapter(genderAdapter);

        ArrayAdapter<String> nationalityAdapter = new ArrayAdapter<String>(SignUpActivity.this, R.layout.item_gender_type, nationality);
        sp_Nationality.setAdapter(nationalityAdapter);

        sp_Title.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Change the selected item's text color
                if (sp_Title.getSelectedItem().toString().equals("Title")) {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.colorAccent));
                } else {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.black));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        sp_Gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Change the selected item's text color
                if (sp_Gender.getSelectedItem().toString().equals("Gender")) {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.colorAccent));
                } else {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.black));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        sp_Nationality.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Change the selected item's text color
                if (sp_Nationality.getSelectedItem().toString().equals("Nationality")) {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.colorAccent));
                } else {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.black));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        edt_DOB.setOnClickListener(this);

        signUp_btn.setOnClickListener(this);
        txt_SignIn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v == txt_SignIn) {
            startActivity(new Intent(SignUpActivity.this, SignInActivity.class));
            finish();
        }
        if (v == edt_DOB) {
            Calendar newCalendar = Calendar.getInstance();
            dateOFEvnet = new DatePickerDialog(SignUpActivity.this, new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, monthOfYear, dayOfMonth);
                    dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                    edt_DOB.setText(dateFormatter.format(newDate.getTime()));
                }

            }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
            dateOFEvnet.show();
        }

        if (v == signUp_btn) {
            String title, gender, nationality;
            title = sp_Title.getSelectedItem().toString();
            gender = sp_Gender.getSelectedItem().toString();
            nationality = sp_Nationality.getSelectedItem().toString();

            if (title.equals("Title")) {
                Toast.makeText(this, "Title Required", Toast.LENGTH_SHORT).show();
                sp_Title.requestFocus();

            } else if (edt_FName.getText().toString().equals("")) {
                edt_FName.setError("First Name Required");
                edt_FName.requestFocus();
            } else if (edt_LName.getText().toString().equals("")) {
                edt_LName.setError("Last Name Required");
                edt_LName.requestFocus();
            } else if (edt_DOB.getText().toString().equals("")) {
                edt_DOB.setError("Birth Date Required");
                edt_DOB.requestFocus();
            } else if (gender.equals("Gender")) {
                Toast.makeText(this, "Choose Gender", Toast.LENGTH_SHORT).show();
                sp_Title.requestFocus();
            } else if (nationality.equals("Nationality")) {
                Toast.makeText(this, "Choose Country", Toast.LENGTH_SHORT).show();
                sp_Title.requestFocus();

            } else if (edt_EmailId.getText().toString().equals("")) {
                edt_EmailId.setError("Email Id Required");
            } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(edt_EmailId.getText().toString()).matches()) {
                edt_EmailId.setError("Enter valid Email");
                edt_EmailId.requestFocus();
            } else if (edt_MobileNo.getText().toString().equals("")) {
                edt_MobileNo.setError("Contact number required");

                edt_MobileNo.requestFocus();
            } else if (!edt_MobileNo.getText().toString().trim().matches("\\d{10}")) {
                edt_MobileNo.setError("Valid 10 Number required");
                edt_MobileNo.requestFocus();
            } else if (edt_Pass.getText().toString().equals("")) {
                edt_Pass.setError("Enter Password");
                edt_Pass.requestFocus();
            } else if (edt_CPass.getText().toString().equals("")) {
                edt_CPass.setError("Re-Type Password required");
                edt_CPass.requestFocus();

            } else if (!edt_Pass.getText().toString().equals(edt_CPass.getText().toString())) {
                edt_CPass.setError("Password not matched");
                edt_CPass.requestFocus();

            } else {
                SignUp();
            }


        }
    }

    private void SignUp() {
        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("txt_title", sp_Title.getSelectedItem().toString());
            jsonObject.put("txt_firstName", edt_FName.getText().toString());
            jsonObject.put("txt_lastName", edt_LName.getText().toString());
            jsonObject.put("txt_dob", edt_DOB.getText().toString());
            jsonObject.put("txt_gender", sp_Gender.getSelectedItem().toString());
            jsonObject.put("txt_nationality", sp_Nationality.getSelectedItem().toString());
            jsonObject.put("txt_email", edt_EmailId.getText().toString());
            jsonObject.put("txt_mobile", edt_MobileNo.getText().toString());
            jsonObject.put("txt_pass1", edt_Pass.getText().toString());
            jsonObject.put("txt_pass2", edt_CPass.getText().toString());
            Log.e("Register Request", jsonObject.toString());

            RequestGenerator.makePostRequest(this, AppConstants.Registration, jsonObject, true, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {
                    Toast.makeText(SignUpActivity.this, "Some Problem Occured", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(String string) throws JSONException {

                    if (!string.equals("")) {
                        Log.e("Register Response", string);
                        JSONObject jsonObject1 = new JSONObject(string);
                        String status = jsonObject1.getString("status");
                        String msg = jsonObject1.getString("msg");
                        if (status.equals("regiSuccess")) {
                            appPreferance.setPreferences(getApplicationContext(), AppConstants.isLogin, "Login");
                            Toast.makeText(SignUpActivity.this, msg, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(SignUpActivity.this, msg, Toast.LENGTH_SHORT).show();
                        }
                    }

                }
            });
        } catch (JSONException e) {
        }
    }
}
