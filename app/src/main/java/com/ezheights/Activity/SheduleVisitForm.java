package com.ezheights.Activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.ezheights.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class SheduleVisitForm extends AppCompatActivity implements View.OnClickListener {

    TextView sendVisit;
    EditText visiterName, visiterEmail, visiterQuery, visiterDate, visiterTime;

    DatePickerDialog dateOFEvnet;
    private SimpleDateFormat dateFormatter;
    String emaild;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shedule_visit_form);

        emaild = getIntent().getStringExtra("emailId");
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        init();
    }

    private void init() {

        visiterDate = (EditText) findViewById(R.id.chooseDate);
        visiterTime = (EditText) findViewById(R.id.chooseTime);

        visiterName = (EditText) findViewById(R.id.name);
        visiterEmail = (EditText) findViewById(R.id.emialid);
        visiterQuery = (EditText) findViewById(R.id.description);

        sendVisit = (TextView) findViewById(R.id.sendVisit);

        visiterDate.setOnClickListener(this);
        visiterTime.setOnClickListener(this);
        sendVisit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {


        if (v == visiterDate) {
            getDate();
        }
        if (v == visiterTime) {
            getTime();
        }
        if (v == sendVisit) {
            sendVisitToServer();
        }


    }

    private void getDate() {

        Calendar newCalendar = Calendar.getInstance();
        dateOFEvnet = new DatePickerDialog(SheduleVisitForm.this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                visiterDate.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


        visiterDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                dateOFEvnet.show();
                return false;
            }
        });
    }

    private void getTime() {

        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(SheduleVisitForm.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String AM_PM = " am";
                String mm_precede = "";
                if (selectedHour >= 12) {
                    AM_PM = "pm";
                    if (selectedHour >= 13 && selectedHour < 24) {
                        selectedHour -= 12;
                    } else {
                        selectedHour = 12;
                    }
                } else if (selectedHour == 00) {
                    selectedHour = 12;
                }
                if (selectedMinute < 10) {
                    mm_precede = "0";
                }
                visiterTime.setText(selectedHour + ":" + selectedMinute + ":00  " + AM_PM);
            }
        }, hour, minute, false);
        mTimePicker.show();
    }

    private void sendVisitToServer() {
    }


}
