package com.ezheights.Activity.Classified;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ezheights.Adapter.ClassifiedMainAdapter;

import com.ezheights.Models.ClassifiedModel;
import com.ezheights.R;
import com.ezheights.Services.RequestGenerator;
import com.ezheights.Services.ResponseListener;
import com.ezheights.Utils.AppConstants;
import com.ezheights.Utils.AppPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ClassifiedActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView recyclerView_classified;
    private ClassifiedMainAdapter mAdapter;

    ArrayList<ClassifiedModel> arrayList = new ArrayList<>();
    Activity mContext;
    String ClassfiedId;
    private ProgressBar progressBar1;
    FloatingActionButton filterButton;
    LinearLayoutManager mLayoutManager1;
    String searchKeyword, isLogin, emailId;
    private AppPreferences appPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classified);
        mContext = ClassifiedActivity.this;
        appPreferences = new AppPreferences();
        isLogin = appPreferences.getPreferences(getApplicationContext(), AppConstants.isLogin);
        emailId = appPreferences.getPreferences(getApplicationContext(), AppConstants.emailId);
        init();
    }

    private void init() {

        filterButton = (FloatingActionButton) findViewById(R.id.filter_classified_sale);
        progressBar1 = (ProgressBar) findViewById(R.id.progress_bar);
        recyclerView_classified = (RecyclerView) findViewById(R.id.recyclerView_clasified);

        mLayoutManager1 = new LinearLayoutManager(this);
        recyclerView_classified.setLayoutManager(mLayoutManager1);
        mLayoutManager1.setOrientation(LinearLayoutManager.VERTICAL);

        filterButton.setOnClickListener(this);
        searchKeyword = getIntent().getStringExtra("Search");
        if (searchKeyword == null) {
            searchKeyword = "";
        }
        if (emailId == null) {
            emailId = "";
        }

        getClassifiedSaleData();
    }

    private void getClassifiedSaleData() {

//        try {
//            JSONObject jsonObject = new JSONObject();
//            jsonObject.put("search", searchKeyword);
//            jsonObject.put("email", "");

        String UrlClassifiedMainData = AppConstants.ClassifiedSaleData + "&search=" + searchKeyword + "&email=" + emailId;
        Log.e("Classified Request", UrlClassifiedMainData);

        RequestGenerator.makeGetRequest(mContext, UrlClassifiedMainData, true, new ResponseListener() {
            @Override
            public void onError(VolleyError error) {
                Toast.makeText(mContext, "Some Problem Occured", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(String string) throws JSONException {
                if (!string.equals("")) {

                    Log.e("Classfied Response", string);
                    JSONObject jsonObject1 = new JSONObject(string);

                    JSONArray jsonArray = jsonObject1.getJSONArray("classifieds");
                    arrayList.clear();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
/**/
                        ClassifiedModel classifiedModel = new ClassifiedModel();
                        classifiedModel.setItemID(jsonObject2.getString("ItemID"));
                        classifiedModel.setTitle(jsonObject2.getString("title"));
                        classifiedModel.setPrice(jsonObject2.getString("price"));
                        classifiedModel.setAge(jsonObject2.getString("age"));
                        classifiedModel.setCondition(jsonObject2.getString("condition"));
                        classifiedModel.setItemUseage(jsonObject2.getString("ItemUseage"));
                        classifiedModel.setImage(jsonObject2.getString("image"));
                        classifiedModel.setFavourite(jsonObject2.getString("favourite"));

                        arrayList.add(classifiedModel);

                    }

                    mAdapter = new ClassifiedMainAdapter(ClassifiedActivity.this, arrayList);
//                    Log.d("MotorIID", arrayList.get(0).getMotor_id());
                    recyclerView_classified.setAdapter(mAdapter);
                }
            }
        });


    }

    @Override
    public void onClick(View v) {
        if (v == filterButton) {
            Intent intent = new Intent(mContext, FilterClassifiedData.class);
            startActivity(intent);

        }
    }

}

