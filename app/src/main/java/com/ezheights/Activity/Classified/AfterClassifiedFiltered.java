package com.ezheights.Activity.Classified;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ezheights.Activity.Property.AfterFilteredPropertyActivity;
import com.ezheights.Adapter.ClassifiedMainAdapter;
import com.ezheights.Models.ClassifiedModel;
import com.ezheights.R;
import com.ezheights.Services.RequestGenerator;
import com.ezheights.Services.ResponseListener;
import com.ezheights.Utils.AppConstants;
import com.ezheights.Utils.AppPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AfterClassifiedFiltered extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ClassifiedMainAdapter mAdapter;
    LinearLayoutManager mLayoutManager1;
    AppPreferences preferences;
    ArrayList<ClassifiedModel> arrayList = new ArrayList<>();
    String cati, cty, lkFr, minP, maxP, condi, ag, usg, emailId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classified_filtered_data);
        preferences = new AppPreferences();

        cati = getIntent().getStringExtra("txt_cat");
        cty = getIntent().getStringExtra("txt_city");
        lkFr = getIntent().getStringExtra("txt_title");
        minP = getIntent().getStringExtra("minPrice");
        maxP = getIntent().getStringExtra("maxPrice");
        condi = getIntent().getStringExtra("txt_condition");
        ag = getIntent().getStringExtra("txt_age");
        usg = getIntent().getStringExtra("txt_usage");
        emailId = preferences.getPreferences(getApplicationContext(), AppConstants.emailId);
        init();

    }

    private void init() {

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_filtered);

        mLayoutManager1 = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager1);
        mLayoutManager1.setOrientation(LinearLayoutManager.VERTICAL);

        if (cati == null) {
            cati = "";
        }
        if (cty == null) {
            cty = "";
        }
        if (lkFr == null) {
            lkFr = "";
        }
        if (minP == null) {
            minP = "";
        }
        if (maxP == null) {
            maxP = "";
        }
        if (condi == null) {
            condi = "";
        }
        if (emailId == null) {
            emailId = "";
        }

        if (ag == null) {
            ag = "";
        }
        if (usg == null) {
            usg = "";
        }


        apllyClassifiedFilter();

    }

    private void apllyClassifiedFilter() {


        String UrlClassFilterApply = AppConstants.ApplyClassifiedFilter + "&txt_cat=" + cati + "&txt_city=" + cty + "&txt_title=" + lkFr + "&minPrice=" + minP + "&maxPrice=" + maxP +
                "&txt_condition=" + condi + "&txt_age=" + ag + "&txt_usage=" + usg + "&email=" + emailId;

        Log.e("Class Filter Aply", UrlClassFilterApply);

        RequestGenerator.makeGetRequest(AfterClassifiedFiltered.this, UrlClassFilterApply, true, new ResponseListener() {
            @Override
            public void onError(VolleyError error) {
                Toast.makeText(AfterClassifiedFiltered.this, "Some Problem Occured", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(String string) throws JSONException {
                if (!string.equals("")) {

                    Log.e("Classi Filter", string);
                    JSONObject jsonObject1 = new JSONObject(string);

                    JSONArray jsonArray = jsonObject1.getJSONArray("classified_data");
                    arrayList.clear();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                        ClassifiedModel classifiedModel = new ClassifiedModel();
                        classifiedModel.setItemID(jsonObject2.getString("itemID"));
                        classifiedModel.setTitle(jsonObject2.getString("title"));
                        classifiedModel.setPrice(jsonObject2.getString("price"));
                        classifiedModel.setAge(jsonObject2.getString("age"));
                        classifiedModel.setCondition(jsonObject2.getString("condition"));
                        classifiedModel.setItemUseage(jsonObject2.getString("usage"));
                        classifiedModel.setImage(jsonObject2.getString("image"));
                        classifiedModel.setFavourite(jsonObject2.getString("favourite"));

                        arrayList.add(classifiedModel);

                    }

                    mAdapter = new ClassifiedMainAdapter(AfterClassifiedFiltered.this, arrayList);
                    recyclerView.setAdapter(mAdapter);
                }
            }
        });

    }

}
