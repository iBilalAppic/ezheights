package com.ezheights.Activity.Classified;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ezheights.R;
import com.ezheights.Services.RequestGenerator;
import com.ezheights.Services.ResponseListener;
import com.ezheights.Utils.AppConstants;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class ClassifiedDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tv_ReadMore, tv_Title, tv_price, tvPostedBy, tvpostedOn, tvYear, tvUsage, tvCondi, tvWarranty, tvShortDesc;
    private ImageView img_Facebook, img_Twitter, img_LinkedIn, img_Google, img_Share, img_Like, img_Back, mainImaage;
    CircleImageView imgShort;
    FloatingActionMenu materialDesignFAM;
    FloatingActionButton makeCall, makeSms;

    String ClassId, lat, lang, shortDesc, title, longDesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classified_details);

        ClassId = getIntent().getStringExtra("ClassiFiedId");
        Log.e("ClassiFiedId", ClassId);
        init();
    }


    private void init() {

        tv_ReadMore = (TextView) findViewById(R.id.tv_ReadMore);
        tv_Title = (TextView) findViewById(R.id.tv_Title_ClassDetil);
        tv_price = (TextView) findViewById(R.id.tv_Price_ClassiDeatil);
        tvPostedBy = (TextView) findViewById(R.id.postedBy);
        tvpostedOn = (TextView) findViewById(R.id.postedOn);
        tvYear = (TextView) findViewById(R.id.yearUsed);
        tvUsage = (TextView) findViewById(R.id.usage);
        tvCondi = (TextView) findViewById(R.id.condi);
        tvWarranty = (TextView) findViewById(R.id.warranty);
        tvShortDesc = (TextView) findViewById(R.id.shortDess);

        imgShort = (CircleImageView) findViewById(R.id.img_Owner_Clssd);

        mainImaage = (ImageView) findViewById(R.id.img_Main_Class);
        img_Like = (ImageView) findViewById(R.id.img_Like);
        img_Share = (ImageView) findViewById(R.id.img_Share);
        img_Back = (ImageView) findViewById(R.id.img_Back);

        materialDesignFAM = (FloatingActionMenu) findViewById(R.id.floating_action_menu);
        makeCall = (FloatingActionButton) findViewById(R.id.make_call);
        makeSms = (FloatingActionButton) findViewById(R.id.make_sms);


        tv_ReadMore.setOnClickListener(this);
        img_Like.setOnClickListener(this);
        img_Share.setOnClickListener(this);
        img_Back.setOnClickListener(this);
        makeCall.setOnClickListener(this);
        makeSms.setOnClickListener(this);

        getClassifiedDetail();
    }

    private void getClassifiedDetail() {

        String UrlDetail = AppConstants.ClassifiedDetailsData + "&ID=" + ClassId;

        Log.e("Classsifed Requset", UrlDetail);
        RequestGenerator.makeGetRequest(ClassifiedDetailsActivity.this, UrlDetail, true, new ResponseListener() {
            @Override
            public void onError(VolleyError error) {
                Toast.makeText(ClassifiedDetailsActivity.this, "Some Problem Occured", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(String string) throws JSONException {

                if (!string.equals("")) {
                    JSONObject jsonObject1 = new JSONObject(string);

                    tv_Title.setText(jsonObject1.getString("title"));
                    tv_price.setText(jsonObject1.getString("newPrice"));
                    tvPostedBy.setText("Posted By :" + jsonObject1.getString("newMobileNumber"));
                    tvpostedOn.setText("Posted On :" + jsonObject1.getString("postedDate"));
                    tvYear.setText(jsonObject1.getString("newItemAge"));
                    tvUsage.setText(jsonObject1.getString("newItemUseage"));
                    tvCondi.setText(jsonObject1.getString("newItemCondition"));
                    tvWarranty.setText(jsonObject1.getString("newWarranty"));
                    tvShortDesc.setText(jsonObject1.getString("newShortDesc"));

                    title = jsonObject1.getString("title");

                    longDesc = jsonObject1.getString("newLongDesc");
                    Picasso.with(ClassifiedDetailsActivity.this).load(jsonObject1.getString("images")).placeholder(R.drawable.watermark_property).into(imgShort);
                    Picasso.with(ClassifiedDetailsActivity.this).load(jsonObject1.getString("images")).placeholder(R.drawable.watermark_property).into(mainImaage);

                }
            }
        });


    }

    @Override
    public void onClick(View v) {

        if (v == img_Back) {
            onBackPressed();

        }

        if (v == tv_ReadMore) {
            readMoreDialog();

        }

        if (v == img_Like) {


        }

        if (v == img_Share) {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_TEXT, "EZHeights");
            startActivity(Intent.createChooser(shareIntent, "Share link using"));

        }

        if (v == makeCall) {

            Toast.makeText(this, "Call Under process", Toast.LENGTH_SHORT).show();
        }
        if (v == makeSms) {

            Toast.makeText(this, "SMS Under process", Toast.LENGTH_SHORT).show();
        }

    }

    private void readMoreDialog() {

        final Dialog dialog = new Dialog(ClassifiedDetailsActivity.this, R.style.MyDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.read_more_dialog);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogSlideUpDownAnim;

        TextView toolbar_title = (TextView) dialog.findViewById(R.id.toolbar_title);
        TextView textView = (TextView) dialog.findViewById(R.id.tv_ReadMore);

        toolbar_title.setText(title);
        textView.setText(longDesc);
        dialog.show();

    }

}
