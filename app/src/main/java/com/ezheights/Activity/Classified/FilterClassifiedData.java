package com.ezheights.Activity.Classified;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ezheights.R;
import com.ezheights.Services.RequestGenerator;
import com.ezheights.Services.ResponseListener;
import com.ezheights.Utils.AppConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FilterClassifiedData extends AppCompatActivity implements View.OnClickListener {


    Context mContext;
    private LinearLayout category, city, lookinFor, minPrice, maxPrice, condition, age, usage;
    private RelativeLayout categoryLL, cityLL, lookinForLL, minPriceLL, maxPriceLL, conditionLL, ageLL, usageLL;
    RadioGroup radiogrp_category, radiogrp_city, radiogrp_minPrice, radiogrp_maxPrice, radiogrp_conditon, radiogrp_age, radiogrp_usage;
    private EditText lookingFor;
    private View view1, view2, view3, view4, view5, view6, view7, view8;
    private TextView txt1, txt2, txt3, txt4, txt5, txt6, txt7, txt8;
    private TextView clearAll, cancelBtn, appltFilter;

    String cati, cty, lkFr, minP, maxP, condi, ag, usg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_classified_data);
        mContext = FilterClassifiedData.this;

        init();
    }

    private void init() {

        categoryLL = (RelativeLayout) findViewById(R.id.CategoryMainLayout);
        cityLL = (RelativeLayout) findViewById(R.id.cityNameMainLayout);
        lookinForLL = (RelativeLayout) findViewById(R.id.lookinForMainLayout);
        minPriceLL = (RelativeLayout) findViewById(R.id.minPriceMainLayout);
        maxPriceLL = (RelativeLayout) findViewById(R.id.maxPriceMainLayout);
        conditionLL = (RelativeLayout) findViewById(R.id.conditionMainLayout);
        ageLL = (RelativeLayout) findViewById(R.id.ageMainLayout);
        usageLL = (RelativeLayout) findViewById(R.id.usageMainLayout);

        category = (LinearLayout) findViewById(R.id.Category);
        city = (LinearLayout) findViewById(R.id.cityName);
        lookinFor = (LinearLayout) findViewById(R.id.lookinFor);
        minPrice = (LinearLayout) findViewById(R.id.classfiedMinPrice);
        maxPrice = (LinearLayout) findViewById(R.id.classfiedMaxPrice);
        condition = (LinearLayout) findViewById(R.id.condition);
        age = (LinearLayout) findViewById(R.id.age);
        usage = (LinearLayout) findViewById(R.id.usage);


        view1 = (View) findViewById(R.id.viewClassfilter1);
        view2 = (View) findViewById(R.id.viewClassfilter2);
        view3 = (View) findViewById(R.id.viewClassfilter3);
        view4 = (View) findViewById(R.id.viewClassfilter4);
        view5 = (View) findViewById(R.id.viewClassfilter5);
        view6 = (View) findViewById(R.id.viewClassfilter6);
        view7 = (View) findViewById(R.id.viewClassfilter7);
        view8 = (View) findViewById(R.id.viewClassfilter8);

        txt1 = (TextView) findViewById(R.id.txtClassi1);
        txt2 = (TextView) findViewById(R.id.txtClassi2);
        txt3 = (TextView) findViewById(R.id.txtClassi3);
        txt4 = (TextView) findViewById(R.id.txtClassi4);
        txt5 = (TextView) findViewById(R.id.txtClassi5);
        txt6 = (TextView) findViewById(R.id.txtClassi6);
        txt7 = (TextView) findViewById(R.id.txtClassi7);
        txt8 = (TextView) findViewById(R.id.txtClassi8);

        lookingFor = (EditText) findViewById(R.id.lookinForText);

        radiogrp_category = (RadioGroup) findViewById(R.id.radiogrp_category);
        radiogrp_city = (RadioGroup) findViewById(R.id.radiogrp_cityName);
        radiogrp_minPrice = (RadioGroup) findViewById(R.id.radiogrp_minPrice);
        radiogrp_maxPrice = (RadioGroup) findViewById(R.id.radiogrp_maxPrice);
        radiogrp_conditon = (RadioGroup) findViewById(R.id.radiogrp_condition);
        radiogrp_age = (RadioGroup) findViewById(R.id.radiogrp_age);
        radiogrp_usage = (RadioGroup) findViewById(R.id.radiogrp_usage);

        clearAll = (TextView) findViewById(R.id.clearAll);
        cancelBtn = (TextView) findViewById(R.id.cancel);
        appltFilter = (TextView) findViewById(R.id.applyfilter);

        category.setOnClickListener(this);
        city.setOnClickListener(this);
        lookinFor.setOnClickListener(this);
        minPrice.setOnClickListener(this);
        maxPrice.setOnClickListener(this);
        condition.setOnClickListener(this);
        age.setOnClickListener(this);
        usage.setOnClickListener(this);

        clearAll.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);
        appltFilter.setOnClickListener(this);

        if (cati == null) {
            cati = "";
        }
        if (cty == null) {
            cty = "";
        }
        if (lkFr == null) {
            lkFr = "";
        }
        if (minP == null) {
            minP = "";
        }
        if (maxP == null) {
            maxP = "";
        }
        if (condi == null) {
            condi = "";
        }

        if (ag == null) {
            ag = "";
        }
        if (usg == null) {
            usg = "";
        }

        lkFr = lookingFor.getText().toString();
        getClassifiedFilterContent();
    }

    private void getClassifiedFilterContent() {

        RequestGenerator.makeGetRequest(FilterClassifiedData.this, AppConstants.PreClassifiedFilter, true, new ResponseListener() {
            @Override
            public void onError(VolleyError error) {
                Toast.makeText(FilterClassifiedData.this, "Problem Occured", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(String string) throws JSONException {
                if (!string.equals("")) {
                    JSONObject jsonObject = new JSONObject(string);
                    JSONArray jsonArrayCat = jsonObject.getJSONArray("category");
                    for (int i = 0; i < jsonArrayCat.length(); i++) {
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        final RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonArrayCat.getString(i));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_category.addView(rdbtn_for_cat, rprms);

                        radiogrp_category.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        cati = (String) btn.getText();

                                        return;
                                    }
                                }
                            }
                        });
                    }
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
                    JSONArray jsonArrayCity = jsonObject.getJSONArray("city_detail");
                    for (int i = 0; i < jsonArrayCity.length(); i++) {
                        JSONObject jsonObject2 = jsonArrayCity.getJSONObject(i);
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonObject2.getString("cityName"));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_city.addView(rdbtn_for_cat, rprms);
                        radiogrp_city.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        cty = (String) btn.getText();
                                        return;
                                    }
                                }
                            }
                        });

                    }

/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

                    JSONArray jsonArrayMinP = jsonObject.getJSONArray("price");
                    radiogrp_minPrice.removeAllViews();
                    for (int i = 0; i < jsonArrayMinP.length(); i++) {
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonArrayMinP.getString(i));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_minPrice.addView(rdbtn_for_cat, rprms);
                        radiogrp_minPrice.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        minP = (String) btn.getText();
                                        return;
                                    }
                                }
                            }
                        });

                    }

/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

                    JSONArray jsonArrayMaxP = jsonObject.getJSONArray("price");
                    radiogrp_maxPrice.removeAllViews();
                    for (int i = 0; i < jsonArrayMaxP.length(); i++) {
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonArrayMaxP.getString(i));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_maxPrice.addView(rdbtn_for_cat, rprms);

                        radiogrp_maxPrice.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        maxP = (String) btn.getText();
                                        return;
                                    }
                                }
                            }
                        });

                    }
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
                    JSONArray jsonArrayCondi = jsonObject.getJSONArray("condition");
                    radiogrp_conditon.removeAllViews();
                    for (int i = 0; i < jsonArrayCondi.length(); i++) {
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonArrayCondi.getString(i));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_conditon.addView(rdbtn_for_cat, rprms);

                        radiogrp_conditon.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        condi = (String) btn.getText();
                                        return;
                                    }
                                }
                            }
                        });
                    }
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
                    JSONArray jsonArrayAge = jsonObject.getJSONArray("age");
                    radiogrp_age.removeAllViews();
                    for (int i = 0; i < jsonArrayAge.length(); i++) {
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonArrayAge.getString(i));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_age.addView(rdbtn_for_cat, rprms);

                        radiogrp_age.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        ag = (String) btn.getText();
                                        return;
                                    }
                                }
                            }
                        });
                    }
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
                    JSONArray jsonArrayUsage = jsonObject.getJSONArray("ItemUseage");
                    radiogrp_usage.removeAllViews();
                    for (int i = 0; i < jsonArrayUsage.length(); i++) {
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonArrayUsage.getString(i));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_usage.addView(rdbtn_for_cat, rprms);

                        radiogrp_usage.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        usg = (String) btn.getText();
                                        return;
                                    }
                                }
                            }
                        });
                    }

                }

            }
        });

    }

    @Override
    public void onClick(View v) {

        if (v == clearAll) {

        }
        if (v == cancelBtn) {
            onBackPressed();

        }
        if (v == appltFilter) {

            applyClassFilterMethod();
        }
        if (v == category) {
            categoryLL.setVisibility(View.VISIBLE);
            cityLL.setVisibility(View.GONE);
            lookinForLL.setVisibility(View.GONE);
            minPriceLL.setVisibility(View.GONE);
            maxPriceLL.setVisibility(View.GONE);
            conditionLL.setVisibility(View.GONE);
            ageLL.setVisibility(View.GONE);
            usageLL.setVisibility(View.GONE);

            view1.setVisibility(View.VISIBLE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);
            view8.setVisibility(View.GONE);

            txt1.setTextColor(Color.parseColor("#000000"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
            txt8.setTextColor(Color.parseColor("#bdbdbd"));
        }
        if (v == city) {
            categoryLL.setVisibility(View.GONE);
            cityLL.setVisibility(View.VISIBLE);
            lookinForLL.setVisibility(View.GONE);
            minPriceLL.setVisibility(View.GONE);
            maxPriceLL.setVisibility(View.GONE);
            conditionLL.setVisibility(View.GONE);
            ageLL.setVisibility(View.GONE);
            usageLL.setVisibility(View.GONE);

            view1.setVisibility(View.GONE);
            view2.setVisibility(View.VISIBLE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);
            view8.setVisibility(View.GONE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#000000"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
            txt8.setTextColor(Color.parseColor("#bdbdbd"));
        }
        if (v == lookinFor) {

            categoryLL.setVisibility(View.GONE);
            cityLL.setVisibility(View.GONE);
            lookinForLL.setVisibility(View.VISIBLE);
            minPriceLL.setVisibility(View.GONE);
            maxPriceLL.setVisibility(View.GONE);
            conditionLL.setVisibility(View.GONE);
            ageLL.setVisibility(View.GONE);
            usageLL.setVisibility(View.GONE);

            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.VISIBLE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);
            view8.setVisibility(View.GONE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#000000"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
            txt8.setTextColor(Color.parseColor("#bdbdbd"));
        }
        if (v == minPrice) {
            categoryLL.setVisibility(View.GONE);
            cityLL.setVisibility(View.GONE);
            lookinForLL.setVisibility(View.GONE);
            minPriceLL.setVisibility(View.VISIBLE);
            maxPriceLL.setVisibility(View.GONE);
            conditionLL.setVisibility(View.GONE);
            ageLL.setVisibility(View.GONE);
            usageLL.setVisibility(View.GONE);

            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.VISIBLE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);
            view8.setVisibility(View.GONE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#000000"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
            txt8.setTextColor(Color.parseColor("#bdbdbd"));
        }
        if (v == maxPrice) {
            categoryLL.setVisibility(View.GONE);
            cityLL.setVisibility(View.GONE);
            lookinForLL.setVisibility(View.GONE);
            minPriceLL.setVisibility(View.GONE);
            maxPriceLL.setVisibility(View.VISIBLE);
            conditionLL.setVisibility(View.GONE);
            ageLL.setVisibility(View.GONE);
            usageLL.setVisibility(View.GONE);

            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.VISIBLE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);
            view8.setVisibility(View.GONE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#000000"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
            txt8.setTextColor(Color.parseColor("#bdbdbd"));
        }
        if (v == condition) {
            categoryLL.setVisibility(View.GONE);
            cityLL.setVisibility(View.GONE);
            lookinForLL.setVisibility(View.GONE);
            minPriceLL.setVisibility(View.GONE);
            maxPriceLL.setVisibility(View.GONE);
            conditionLL.setVisibility(View.VISIBLE);
            ageLL.setVisibility(View.GONE);
            usageLL.setVisibility(View.GONE);

            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.VISIBLE);
            view7.setVisibility(View.GONE);
            view8.setVisibility(View.GONE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#000000"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
            txt8.setTextColor(Color.parseColor("#bdbdbd"));
        }
        if (v == age) {
            categoryLL.setVisibility(View.GONE);
            cityLL.setVisibility(View.GONE);
            lookinForLL.setVisibility(View.GONE);
            minPriceLL.setVisibility(View.GONE);
            maxPriceLL.setVisibility(View.GONE);
            conditionLL.setVisibility(View.GONE);
            ageLL.setVisibility(View.VISIBLE);
            usageLL.setVisibility(View.GONE);

            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.VISIBLE);
            view8.setVisibility(View.GONE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#000000"));
            txt8.setTextColor(Color.parseColor("#bdbdbd"));
        }

        if (v == usage) {
            categoryLL.setVisibility(View.GONE);
            cityLL.setVisibility(View.GONE);
            lookinForLL.setVisibility(View.GONE);
            minPriceLL.setVisibility(View.GONE);
            maxPriceLL.setVisibility(View.GONE);
            conditionLL.setVisibility(View.GONE);
            ageLL.setVisibility(View.GONE);
            usageLL.setVisibility(View.VISIBLE);

            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);
            view8.setVisibility(View.VISIBLE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
            txt8.setTextColor(Color.parseColor("#000000"));
        }


    }

    private void applyClassFilterMethod() {

        Intent intent = new Intent(FilterClassifiedData.this, AfterClassifiedFiltered.class);
        intent.putExtra("txt_cat", cati);
        intent.putExtra("txt_city", cty);
        intent.putExtra("txt_title", lkFr);
        intent.putExtra("txt_condition", condi);
        intent.putExtra("minPrice", minP);
        intent.putExtra("maxPrice", maxP);
        intent.putExtra("txt_age", ag);
        intent.putExtra("txt_usage", usg);
        startActivity(intent);
        finish();
    }
}
