package com.ezheights.Activity.Property;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ezheights.Models.CategoryFilterModel;
import com.ezheights.Models.CityModel;
import com.ezheights.Models.CommunityFilterModel;
import com.ezheights.Models.MaxAreaModel;
import com.ezheights.Models.MaxBedModel;
import com.ezheights.Models.MaxPriceModel;
import com.ezheights.Models.MinAreaModel;
import com.ezheights.Models.MinBedModel;
import com.ezheights.Models.MinPriceModel;
import com.ezheights.Models.TowerModel;
import com.ezheights.R;
import com.ezheights.Services.RequestGenerator;
import com.ezheights.Services.ResponseListener;
import com.ezheights.Utils.AppConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FilterActivity extends AppCompatActivity implements View.OnClickListener {


    Context mContext;
    private RelativeLayout propertyLL, categoryLL, towerNameLL, cityLL, minPriceLL, maxPriceLL, minAreaLL, maxAreaLL, minBedLL, maxBedLL, sortByLL;
    private LinearLayout refNoLL, communityLL, postedByLL;
    RadioGroup radiogrp_property, radiogrp_category, radiogrp_towerName, radiogrp_city, radiogrp_minPrice, radiogrp_maxPrice, radiogrp_minArea, radiogrp_maxArea, radiogrp_minBed, radiogrp_maxBed, radiogrp_SortBy, radiogrp_postedBy;
    private EditText refNoEdit;
    private LinearLayout property, category, community, towerName, city, minPrice, maxPrice, minArea, maxArea, minBed, maxBed, sortBy, postedBy, refNo;
    private View view1, view2, view3, view4, view5, view6, view7, view8, view9, view10, view11, view12, view13, view14;
    private TextView txt1, txt2, txt3, txt4, txt5, txt6, txt7, txt8, txt9, txt10, txt11, txt12, txt13, txt14, nothingText;
    private TextView clearAll, cancelBtn, appltFilter;
    ArrayList<CategoryFilterModel> arrayListCat;
    ArrayList<CommunityFilterModel> arrayListComm;
    ArrayList<TowerModel> arrayListTower;
    ArrayList<CityModel> arrayListCity;
    ArrayList<MinPriceModel> arrayListMinP;
    ArrayList<MaxPriceModel> arrayListMaxP;
    ArrayList<MinAreaModel> arrayListMinA;
    ArrayList<MaxAreaModel> arrayListMaxA;
    ArrayList<MinBedModel> arrayListMinB;
    ArrayList<MaxBedModel> arrayListMaxB;
    String prop, cati, comm, towr, cty, minP, maxP, minA, maxA, minB, maxB, postBy, srtBy, rfNo;
    private Spinner spinner_postedBy;
    private String[] postedByString = {"all", "agent", "developer", "owner"};
    String propTypeChecked;

    RadioButton saleBtn, rentBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        mContext = FilterActivity.this;
        propTypeChecked = getIntent().getStringExtra("propType");


        init();
    }

    private void init() {

        propertyLL = (RelativeLayout) findViewById(R.id.proprtyMainLayout);
        categoryLL = (RelativeLayout) findViewById(R.id.propertyCategoryMain);
        communityLL = (LinearLayout) findViewById(R.id.communityMainLayout);
        towerNameLL = (RelativeLayout) findViewById(R.id.towerCategoryMain);
        cityLL = (RelativeLayout) findViewById(R.id.cityCategoryMain);
        minPriceLL = (RelativeLayout) findViewById(R.id.minPriceCategoryMain);
        maxPriceLL = (RelativeLayout) findViewById(R.id.maxPriceCategoryMain);
        minAreaLL = (RelativeLayout) findViewById(R.id.minAreaCategoryMain);
        maxAreaLL = (RelativeLayout) findViewById(R.id.maxAreaCategoryMain);
        minBedLL = (RelativeLayout) findViewById(R.id.minBedCategoryMain);
        maxBedLL = (RelativeLayout) findViewById(R.id.maxBedCategoryMain);
        sortByLL = (RelativeLayout) findViewById(R.id.sortByMainLayout);
        postedByLL = (LinearLayout) findViewById(R.id.postedByMainLayout);
        refNoLL = (LinearLayout) findViewById(R.id.refNoMainLayout);


        property = (LinearLayout) findViewById(R.id.propertyFilter);
        category = (LinearLayout) findViewById(R.id.propertyCategory);
        community = (LinearLayout) findViewById(R.id.propertyCommunity);
        towerName = (LinearLayout) findViewById(R.id.propertyTowerName);
        city = (LinearLayout) findViewById(R.id.propertyCity);
        minPrice = (LinearLayout) findViewById(R.id.propertyMinPrice);
        maxPrice = (LinearLayout) findViewById(R.id.propertyMaxPrice);
        minArea = (LinearLayout) findViewById(R.id.propertyMinArea);
        maxArea = (LinearLayout) findViewById(R.id.propertyMaxArea);
        minBed = (LinearLayout) findViewById(R.id.propertyMinBedRoom);
        maxBed = (LinearLayout) findViewById(R.id.propertyMaxBedRoom);
        sortBy = (LinearLayout) findViewById(R.id.propertySortBy);
        postedBy = (LinearLayout) findViewById(R.id.propertyPostedBy);
        refNo = (LinearLayout) findViewById(R.id.propertyRefrenceNo);


        view1 = (View) findViewById(R.id.viewfilter1);
        view2 = (View) findViewById(R.id.viewfilter2);
        view3 = (View) findViewById(R.id.viewfilter3);
        view4 = (View) findViewById(R.id.viewfilter4);
        view5 = (View) findViewById(R.id.viewfilter5);
        view6 = (View) findViewById(R.id.viewfilter6);
        view7 = (View) findViewById(R.id.viewfilter7);
        view8 = (View) findViewById(R.id.viewfilter8);
        view9 = (View) findViewById(R.id.viewfilter9);
        view10 = (View) findViewById(R.id.viewfilter10);
        view11 = (View) findViewById(R.id.viewfilter11);
        view12 = (View) findViewById(R.id.viewfilter12);
        view13 = (View) findViewById(R.id.viewfilter13);
        view14 = (View) findViewById(R.id.viewfilter14);

        txt1 = (TextView) findViewById(R.id.txt1);
        txt2 = (TextView) findViewById(R.id.txt2);
        txt3 = (TextView) findViewById(R.id.txt3);
        txt4 = (TextView) findViewById(R.id.txt4);
        txt5 = (TextView) findViewById(R.id.txt5);
        txt6 = (TextView) findViewById(R.id.txt6);
        txt7 = (TextView) findViewById(R.id.txt7);
        txt8 = (TextView) findViewById(R.id.txt8);
        txt9 = (TextView) findViewById(R.id.txt9);
        txt10 = (TextView) findViewById(R.id.txt10);
        txt11 = (TextView) findViewById(R.id.txt11);
        txt12 = (TextView) findViewById(R.id.txt12);
        txt13 = (TextView) findViewById(R.id.txt13);
        txt14 = (TextView) findViewById(R.id.txt14);

        saleBtn = (RadioButton) findViewById(R.id.radio_sale);
        rentBtn = (RadioButton) findViewById(R.id.radio_rent);

        nothingText = (TextView) findViewById(R.id.nothingText);
        refNoEdit = (EditText) findViewById(R.id.refNoEdit);

        radiogrp_property = (RadioGroup) findViewById(R.id.points_radio_group);
        radiogrp_category = (RadioGroup) findViewById(R.id.radiogrp_category);
        radiogrp_towerName = (RadioGroup) findViewById(R.id.radiogrp_tower);
        radiogrp_city = (RadioGroup) findViewById(R.id.radiogrp_city);
        radiogrp_minPrice = (RadioGroup) findViewById(R.id.radiogrp_minPrice);
        radiogrp_maxPrice = (RadioGroup) findViewById(R.id.radiogrp_maxPrice);
        radiogrp_minArea = (RadioGroup) findViewById(R.id.radiogrp_minArea);
        radiogrp_maxArea = (RadioGroup) findViewById(R.id.radiogrp_maxArea);
        radiogrp_minBed = (RadioGroup) findViewById(R.id.radiogrp_minBed);
        radiogrp_maxBed = (RadioGroup) findViewById(R.id.radiogrp_maxBed);
        radiogrp_SortBy = (RadioGroup) findViewById(R.id.shortBy_radio_group);
        radiogrp_postedBy = (RadioGroup) findViewById(R.id.radiogrp_postedBy);


        clearAll = (TextView) findViewById(R.id.clearAll);
        cancelBtn = (TextView) findViewById(R.id.cancel);
        appltFilter = (TextView) findViewById(R.id.applyfilter);

        property.setOnClickListener(this);
        category.setOnClickListener(this);
        community.setOnClickListener(this);
        towerName.setOnClickListener(this);
        city.setOnClickListener(this);
        minPrice.setOnClickListener(this);
        maxPrice.setOnClickListener(this);
        minArea.setOnClickListener(this);
        maxArea.setOnClickListener(this);
        minBed.setOnClickListener(this);
        maxBed.setOnClickListener(this);
        sortBy.setOnClickListener(this);
        postedBy.setOnClickListener(this);
        refNo.setOnClickListener(this);

        clearAll.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);
        appltFilter.setOnClickListener(this);

        if (propTypeChecked.equals("sale")) {

            saleBtn.setChecked(true);
            prop = "sale";
        } else if (propTypeChecked.equals("rent")) {
            rentBtn.setChecked(true);
            prop = "rent";
        }


        if (cati == null) {
            cati = "";
        }
        if (comm == null) {
            comm = "";
        }
        if (towr == null) {
            towr = "";
        }
        if (cty == null) {
            cty = "";
        }
        if (minP == null) {
            minP = "";
        }
        if (maxP == null) {
            maxP = "";
        }
        if (minA == null) {
            minA = "";
        }
        if (maxA == null) {
            maxA = "";
        }
        if (minB == null) {
            minB = "";
        }
        if (maxB == null) {
            maxB = "";
        }
        if (postBy == null) {
            postBy = "";
        }
        if (rfNo == null) {
            rfNo = "";
        }

        rfNo = refNoEdit.getText().toString();
        radiogrp_property.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {

                if (checkedId == R.id.radio_rent) {
                    prop = "rent";
                    if (prop == null) {
                        prop = "";
                    }
                } else if (checkedId == R.id.radio_sale) {
                    prop = "sale";
                    if (prop == null) {
                        prop = "";
                    }
                }
            }
        });

        srtBy = "";
        radiogrp_SortBy.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (checkedId == R.id.radio_newPost) {
                    srtBy = "newly posted";
                } else if (checkedId == R.id.radio_priceLowToHigh) {
                    srtBy = "price low to high";
                } else if (checkedId == R.id.radio_priceHighToLow) {
                    srtBy = "price high to low";
                }
            }
        });


        spinner_postedBy = (Spinner) findViewById(R.id.spinner_PostBy);
        ArrayAdapter<String> aAdapter = new ArrayAdapter<String>(FilterActivity.this, android.R.layout.simple_spinner_dropdown_item, postedByString);
        spinner_postedBy.setAdapter(aAdapter);

        spinner_postedBy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                    nothingText.setVisibility(View.VISIBLE);
                    radiogrp_postedBy.setVisibility(View.GONE);
                } else if (position == 1) {
                    nothingText.setVisibility(View.VISIBLE);
                    radiogrp_postedBy.setVisibility(View.VISIBLE);
                    showAgentRadioData("agent");

                } else if (position == 2) {
                    nothingText.setVisibility(View.VISIBLE);
                    radiogrp_postedBy.setVisibility(View.GONE);
                } else if (position == 3) {
                    nothingText.setVisibility(View.VISIBLE);
                    radiogrp_postedBy.setVisibility(View.GONE);
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        getALlUIOfFilter();

    }

    private void showAgentRadioData(String passData) {


        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("posted_by", passData);


            String UrlPostBy = "http://35.154.160.221/mobileService.php?servicename=post_by";

            RequestGenerator.makePostRequest(FilterActivity.this, UrlPostBy, jsonObject, true, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {
                    Toast.makeText(FilterActivity.this, "Problem Occured", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(String string) throws JSONException {

                    if (!string.equals("")) {
                        JSONObject jsonObject = new JSONObject(string);
                        JSONArray jsonArray = jsonObject.getJSONArray("agent");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                            final RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                            rdbtn_for_cat.setText(jsonArray.getString(i));
                            Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                            drawable.setBounds(0, 0, 60, 80);
                            rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                            rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                            rdbtn_for_cat.setButtonDrawable(null);
                            rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                            rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                            radiogrp_postedBy.addView(rdbtn_for_cat, rprms);

                            radiogrp_postedBy.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                    for (int i = 0; i < rg.getChildCount(); i++) {
                                        RadioButton btn = (RadioButton) rg.getChildAt(i);
                                        if (btn.getId() == checkedId) {
                                            postBy = (String) btn.getText();
                                            return;
                                        }
                                    }
                                }
                            });
                        }
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }







  /*-----------------------------------------------------------------------------------------------*/


    private void getALlUIOfFilter() {


        RequestGenerator.makeGetRequest(FilterActivity.this, AppConstants.PrePropertyFilter, true, new ResponseListener() {
            @Override
            public void onError(VolleyError error) {
                Toast.makeText(FilterActivity.this, "Problem Occured", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(String string) throws JSONException {

                if (!string.equals("")) {
                    JSONObject jsonObject = new JSONObject(string);
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
                    JSONArray jsonArrayCat = jsonObject.getJSONArray("category");

                    for (int i = 0; i < jsonArrayCat.length(); i++) {
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        final RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonArrayCat.getString(i));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_category.addView(rdbtn_for_cat, rprms);

                        radiogrp_category.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        cati = (String) btn.getText();

                                        return;
                                    }
                                }
                            }
                        });
                    }

/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
                    JSONArray jsonArrayComm = jsonObject.getJSONArray("community");
                    communityLL.removeAllViews();
                    for (int i = 0; i < jsonArrayComm.length(); i++) {
                        JSONObject jsonObject2 = jsonArrayComm.getJSONObject(i);
                        CheckBox chkTeamName = new CheckBox(FilterActivity.this);
                        chkTeamName.setId(Integer.parseInt(jsonObject2.getString("communityID")));
                        chkTeamName.setText(jsonObject2.getString("communityname"));
                        chkTeamName.setTextColor(Color.BLACK);
                        communityLL.addView(chkTeamName);


//                        chkTeamName.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                            @Override
//                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//
//                                Toast.makeText(FilterActivity.this, "Clicked " + comm, Toast.LENGTH_SHORT).show();
//
//                            }
//                        });
                    }
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
                    JSONArray jsonArrayToewer = jsonObject.getJSONArray("tower");
                    for (int i = 0; i < jsonArrayToewer.length(); i++) {
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonArrayToewer.getString(i));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_towerName.addView(rdbtn_for_cat, rprms);

                        radiogrp_towerName.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        towr = (String) btn.getText();
                                        return;
                                    }
                                }
                            }
                        });

                    }
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
                    JSONArray jsonArrayCity = jsonObject.getJSONArray("city");
                    radiogrp_city.removeAllViews();
                    for (int i = 0; i < jsonArrayCity.length(); i++) {
                        JSONObject jsonObject2 = jsonArrayCity.getJSONObject(i);
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonObject2.getString("name"));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_city.addView(rdbtn_for_cat, rprms);
                        radiogrp_city.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        cty = (String) btn.getText();
                                        return;
                                    }
                                }
                            }
                        });

                    }
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

                    JSONArray jsonArrayMinP = jsonObject.getJSONArray("price_sale");
                    radiogrp_minPrice.removeAllViews();
                    for (int i = 0; i < jsonArrayMinP.length(); i++) {
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonArrayMinP.getString(i));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_minPrice.addView(rdbtn_for_cat, rprms);
                        radiogrp_minPrice.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        minP = (String) btn.getText();
                                        return;
                                    }
                                }
                            }
                        });

                    }

/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

                    JSONArray jsonArrayMaxP = jsonObject.getJSONArray("price_sale");
                    radiogrp_maxPrice.removeAllViews();
                    for (int i = 0; i < jsonArrayMaxP.length(); i++) {
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonArrayMaxP.getString(i));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_maxPrice.addView(rdbtn_for_cat, rprms);

                        radiogrp_maxPrice.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        maxP = (String) btn.getText();
                                        return;
                                    }
                                }
                            }
                        });

                    }
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
                    JSONArray jsonArrayminA = jsonObject.getJSONArray("area");
                    radiogrp_minArea.removeAllViews();
                    for (int i = 0; i < jsonArrayminA.length(); i++) {
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonArrayminA.getString(i));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_minArea.addView(rdbtn_for_cat, rprms);

                        radiogrp_minArea.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        minA = (String) btn.getText();
                                        return;
                                    }
                                }
                            }
                        });
                    }

/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

                    JSONArray jsonArraymaxA = jsonObject.getJSONArray("area");
                    radiogrp_maxArea.removeAllViews();
                    for (int i = 0; i < jsonArraymaxA.length(); i++) {
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonArraymaxA.getString(i));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_maxArea.addView(rdbtn_for_cat, rprms);

                        radiogrp_maxArea.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        maxA = (String) btn.getText();
                                        return;
                                    }
                                }
                            }
                        });
                    }
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

                    JSONArray jsonArrayminB = jsonObject.getJSONArray("bed");
                    radiogrp_minBed.removeAllViews();
                    for (int i = 0; i < jsonArrayminB.length(); i++) {
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonArrayminB.getString(i));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_minBed.addView(rdbtn_for_cat, rprms);

                        radiogrp_minBed.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        minB = (String) btn.getText();
                                        return;
                                    }
                                }
                            }
                        });
                    }
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

                    JSONArray jsonArraymaxB = jsonObject.getJSONArray("bed");
                    radiogrp_maxBed.removeAllViews();
                    for (int i = 0; i < jsonArraymaxB.length(); i++) {
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonArraymaxB.getString(i));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_maxBed.addView(rdbtn_for_cat, rprms);

                        radiogrp_maxBed.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        maxB = (String) btn.getText();
                                        return;
                                    }
                                }
                            }
                        });
                    }
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
                }
            }

        });

    }

    @Override
    public void onClick(View v) {

        if (v == clearAll) {

        }
        if (v == cancelBtn) {
            onBackPressed();

        }
        if (v == appltFilter) {

            applyFilterMethod();
        }

        if (v == property) {

            propertyLL.setVisibility(View.VISIBLE);
            categoryLL.setVisibility(View.GONE);
            communityLL.setVisibility(View.GONE);
            towerNameLL.setVisibility(View.GONE);
            cityLL.setVisibility(View.GONE);
            minPriceLL.setVisibility(View.GONE);
            maxPriceLL.setVisibility(View.GONE);
            minAreaLL.setVisibility(View.GONE);
            maxAreaLL.setVisibility(View.GONE);
            minBedLL.setVisibility(View.GONE);
            maxBedLL.setVisibility(View.GONE);
            sortByLL.setVisibility(View.GONE);
            postedByLL.setVisibility(View.GONE);
            refNoLL.setVisibility(View.GONE);


            view1.setVisibility(View.VISIBLE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);
            view8.setVisibility(View.GONE);
            view9.setVisibility(View.GONE);
            view10.setVisibility(View.GONE);
            view11.setVisibility(View.GONE);
            view12.setVisibility(View.GONE);
            view13.setVisibility(View.GONE);
            view14.setVisibility(View.GONE);
            txt1.setTextColor(Color.parseColor("#000000"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
            txt8.setTextColor(Color.parseColor("#bdbdbd"));
            txt9.setTextColor(Color.parseColor("#bdbdbd"));
            txt10.setTextColor(Color.parseColor("#bdbdbd"));
            txt11.setTextColor(Color.parseColor("#bdbdbd"));
            txt12.setTextColor(Color.parseColor("#bdbdbd"));
            txt13.setTextColor(Color.parseColor("#bdbdbd"));
            txt14.setTextColor(Color.parseColor("#bdbdbd"));


        }

        if (v == category) {


            propertyLL.setVisibility(View.GONE);
            categoryLL.setVisibility(View.VISIBLE);
            communityLL.setVisibility(View.GONE);
            towerNameLL.setVisibility(View.GONE);
            cityLL.setVisibility(View.GONE);
            minPriceLL.setVisibility(View.GONE);
            maxPriceLL.setVisibility(View.GONE);
            minAreaLL.setVisibility(View.GONE);
            maxAreaLL.setVisibility(View.GONE);
            minBedLL.setVisibility(View.GONE);
            maxBedLL.setVisibility(View.GONE);
            sortByLL.setVisibility(View.GONE);
            postedByLL.setVisibility(View.GONE);
            refNoLL.setVisibility(View.GONE);


            view1.setVisibility(View.GONE);
            view2.setVisibility(View.VISIBLE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);
            view8.setVisibility(View.GONE);
            view9.setVisibility(View.GONE);
            view10.setVisibility(View.GONE);
            view11.setVisibility(View.GONE);
            view12.setVisibility(View.GONE);
            view13.setVisibility(View.GONE);
            view14.setVisibility(View.GONE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#000000"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
            txt8.setTextColor(Color.parseColor("#bdbdbd"));
            txt9.setTextColor(Color.parseColor("#bdbdbd"));
            txt10.setTextColor(Color.parseColor("#bdbdbd"));
            txt11.setTextColor(Color.parseColor("#bdbdbd"));
            txt12.setTextColor(Color.parseColor("#bdbdbd"));
            txt13.setTextColor(Color.parseColor("#bdbdbd"));
            txt14.setTextColor(Color.parseColor("#bdbdbd"));


        }
        if (v == community) {
            propertyLL.setVisibility(View.GONE);
            categoryLL.setVisibility(View.GONE);
            communityLL.setVisibility(View.VISIBLE);
            towerNameLL.setVisibility(View.GONE);
            cityLL.setVisibility(View.GONE);
            minPriceLL.setVisibility(View.GONE);
            maxPriceLL.setVisibility(View.GONE);
            minAreaLL.setVisibility(View.GONE);
            maxAreaLL.setVisibility(View.GONE);
            minBedLL.setVisibility(View.GONE);
            maxBedLL.setVisibility(View.GONE);
            sortByLL.setVisibility(View.GONE);
            postedByLL.setVisibility(View.GONE);
            refNoLL.setVisibility(View.GONE);


            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.VISIBLE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);
            view8.setVisibility(View.GONE);
            view9.setVisibility(View.GONE);
            view10.setVisibility(View.GONE);
            view11.setVisibility(View.GONE);
            view12.setVisibility(View.GONE);
            view13.setVisibility(View.GONE);
            view14.setVisibility(View.GONE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#000000"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
            txt8.setTextColor(Color.parseColor("#bdbdbd"));
            txt9.setTextColor(Color.parseColor("#bdbdbd"));
            txt10.setTextColor(Color.parseColor("#bdbdbd"));
            txt11.setTextColor(Color.parseColor("#bdbdbd"));
            txt12.setTextColor(Color.parseColor("#bdbdbd"));
            txt13.setTextColor(Color.parseColor("#bdbdbd"));
            txt14.setTextColor(Color.parseColor("#bdbdbd"));

        }
        if (v == towerName) {
            propertyLL.setVisibility(View.GONE);
            categoryLL.setVisibility(View.GONE);
            communityLL.setVisibility(View.GONE);
            towerNameLL.setVisibility(View.VISIBLE);
            cityLL.setVisibility(View.GONE);
            minPriceLL.setVisibility(View.GONE);
            maxPriceLL.setVisibility(View.GONE);
            minAreaLL.setVisibility(View.GONE);
            maxAreaLL.setVisibility(View.GONE);
            minBedLL.setVisibility(View.GONE);
            maxBedLL.setVisibility(View.GONE);
            sortByLL.setVisibility(View.GONE);
            postedByLL.setVisibility(View.GONE);
            refNoLL.setVisibility(View.GONE);


            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.VISIBLE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);
            view8.setVisibility(View.GONE);
            view9.setVisibility(View.GONE);
            view10.setVisibility(View.GONE);
            view11.setVisibility(View.GONE);
            view12.setVisibility(View.GONE);
            view13.setVisibility(View.GONE);
            view14.setVisibility(View.GONE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#000000"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
            txt8.setTextColor(Color.parseColor("#bdbdbd"));
            txt9.setTextColor(Color.parseColor("#bdbdbd"));
            txt10.setTextColor(Color.parseColor("#bdbdbd"));
            txt11.setTextColor(Color.parseColor("#bdbdbd"));
            txt12.setTextColor(Color.parseColor("#bdbdbd"));
            txt13.setTextColor(Color.parseColor("#bdbdbd"));
            txt14.setTextColor(Color.parseColor("#bdbdbd"));

        }
        if (v == city) {
            propertyLL.setVisibility(View.GONE);
            categoryLL.setVisibility(View.GONE);
            communityLL.setVisibility(View.GONE);
            towerNameLL.setVisibility(View.GONE);
            cityLL.setVisibility(View.VISIBLE);
            minPriceLL.setVisibility(View.GONE);
            maxPriceLL.setVisibility(View.GONE);
            minAreaLL.setVisibility(View.GONE);
            maxAreaLL.setVisibility(View.GONE);
            minBedLL.setVisibility(View.GONE);
            maxBedLL.setVisibility(View.GONE);
            sortByLL.setVisibility(View.GONE);
            postedByLL.setVisibility(View.GONE);
            refNoLL.setVisibility(View.GONE);


            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.VISIBLE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);
            view8.setVisibility(View.GONE);
            view9.setVisibility(View.GONE);
            view10.setVisibility(View.GONE);
            view11.setVisibility(View.GONE);
            view12.setVisibility(View.GONE);
            view13.setVisibility(View.GONE);
            view14.setVisibility(View.GONE);
            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#000000"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
            txt8.setTextColor(Color.parseColor("#bdbdbd"));
            txt9.setTextColor(Color.parseColor("#bdbdbd"));
            txt10.setTextColor(Color.parseColor("#bdbdbd"));
            txt11.setTextColor(Color.parseColor("#bdbdbd"));
            txt12.setTextColor(Color.parseColor("#bdbdbd"));
            txt13.setTextColor(Color.parseColor("#bdbdbd"));
            txt14.setTextColor(Color.parseColor("#bdbdbd"));


        }
        if (v == minPrice) {
            propertyLL.setVisibility(View.GONE);
            categoryLL.setVisibility(View.GONE);
            communityLL.setVisibility(View.GONE);
            towerNameLL.setVisibility(View.GONE);
            cityLL.setVisibility(View.GONE);
            minPriceLL.setVisibility(View.VISIBLE);
            maxPriceLL.setVisibility(View.GONE);
            minAreaLL.setVisibility(View.GONE);
            maxAreaLL.setVisibility(View.GONE);
            minBedLL.setVisibility(View.GONE);
            maxBedLL.setVisibility(View.GONE);
            sortByLL.setVisibility(View.GONE);
            postedByLL.setVisibility(View.GONE);
            refNoLL.setVisibility(View.GONE);


            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.VISIBLE);
            view7.setVisibility(View.GONE);
            view8.setVisibility(View.GONE);
            view9.setVisibility(View.GONE);
            view10.setVisibility(View.GONE);
            view11.setVisibility(View.GONE);
            view12.setVisibility(View.GONE);
            view13.setVisibility(View.GONE);
            view14.setVisibility(View.GONE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#000000"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
            txt8.setTextColor(Color.parseColor("#bdbdbd"));
            txt9.setTextColor(Color.parseColor("#bdbdbd"));
            txt10.setTextColor(Color.parseColor("#bdbdbd"));
            txt11.setTextColor(Color.parseColor("#bdbdbd"));
            txt12.setTextColor(Color.parseColor("#bdbdbd"));
            txt13.setTextColor(Color.parseColor("#bdbdbd"));
            txt14.setTextColor(Color.parseColor("#bdbdbd"));


        }
        if (v == maxPrice) {
            propertyLL.setVisibility(View.GONE);
            categoryLL.setVisibility(View.GONE);
            communityLL.setVisibility(View.GONE);
            towerNameLL.setVisibility(View.GONE);
            cityLL.setVisibility(View.GONE);
            minPriceLL.setVisibility(View.GONE);
            maxPriceLL.setVisibility(View.VISIBLE);
            minAreaLL.setVisibility(View.GONE);
            maxAreaLL.setVisibility(View.GONE);
            minBedLL.setVisibility(View.GONE);
            maxBedLL.setVisibility(View.GONE);
            sortByLL.setVisibility(View.GONE);
            postedByLL.setVisibility(View.GONE);
            refNoLL.setVisibility(View.GONE);


            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.VISIBLE);
            view8.setVisibility(View.GONE);
            view9.setVisibility(View.GONE);
            view10.setVisibility(View.GONE);
            view11.setVisibility(View.GONE);
            view12.setVisibility(View.GONE);
            view13.setVisibility(View.GONE);
            view14.setVisibility(View.GONE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#000000"));
            txt8.setTextColor(Color.parseColor("#bdbdbd"));
            txt9.setTextColor(Color.parseColor("#bdbdbd"));
            txt10.setTextColor(Color.parseColor("#bdbdbd"));
            txt11.setTextColor(Color.parseColor("#bdbdbd"));
            txt12.setTextColor(Color.parseColor("#bdbdbd"));
            txt13.setTextColor(Color.parseColor("#bdbdbd"));
            txt14.setTextColor(Color.parseColor("#bdbdbd"));


        }
        if (v == minArea) {
            propertyLL.setVisibility(View.GONE);
            categoryLL.setVisibility(View.GONE);
            communityLL.setVisibility(View.GONE);
            towerNameLL.setVisibility(View.GONE);
            cityLL.setVisibility(View.GONE);
            minPriceLL.setVisibility(View.GONE);
            maxPriceLL.setVisibility(View.GONE);
            minAreaLL.setVisibility(View.VISIBLE);
            maxAreaLL.setVisibility(View.GONE);
            minBedLL.setVisibility(View.GONE);
            maxBedLL.setVisibility(View.GONE);
            sortByLL.setVisibility(View.GONE);
            postedByLL.setVisibility(View.GONE);
            refNoLL.setVisibility(View.GONE);


            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);
            view8.setVisibility(View.VISIBLE);
            view9.setVisibility(View.GONE);
            view10.setVisibility(View.GONE);
            view11.setVisibility(View.GONE);
            view12.setVisibility(View.GONE);
            view13.setVisibility(View.GONE);
            view14.setVisibility(View.GONE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
            txt8.setTextColor(Color.parseColor("#000000"));
            txt9.setTextColor(Color.parseColor("#bdbdbd"));
            txt10.setTextColor(Color.parseColor("#bdbdbd"));
            txt11.setTextColor(Color.parseColor("#bdbdbd"));
            txt12.setTextColor(Color.parseColor("#bdbdbd"));
            txt13.setTextColor(Color.parseColor("#bdbdbd"));
            txt14.setTextColor(Color.parseColor("#bdbdbd"));


        }
        if (v == maxArea) {
            propertyLL.setVisibility(View.GONE);
            categoryLL.setVisibility(View.GONE);
            communityLL.setVisibility(View.GONE);
            towerNameLL.setVisibility(View.GONE);
            cityLL.setVisibility(View.GONE);
            minPriceLL.setVisibility(View.GONE);
            maxPriceLL.setVisibility(View.GONE);
            minAreaLL.setVisibility(View.GONE);
            maxAreaLL.setVisibility(View.VISIBLE);
            minBedLL.setVisibility(View.GONE);
            maxBedLL.setVisibility(View.GONE);
            sortByLL.setVisibility(View.GONE);
            postedByLL.setVisibility(View.GONE);
            refNoLL.setVisibility(View.GONE);


            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);
            view8.setVisibility(View.GONE);
            view9.setVisibility(View.VISIBLE);
            view10.setVisibility(View.GONE);
            view11.setVisibility(View.GONE);
            view12.setVisibility(View.GONE);
            view13.setVisibility(View.GONE);
            view14.setVisibility(View.GONE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
            txt8.setTextColor(Color.parseColor("#bdbdbd"));
            txt9.setTextColor(Color.parseColor("#000000"));
            txt10.setTextColor(Color.parseColor("#bdbdbd"));
            txt11.setTextColor(Color.parseColor("#bdbdbd"));
            txt12.setTextColor(Color.parseColor("#bdbdbd"));
            txt13.setTextColor(Color.parseColor("#bdbdbd"));
            txt14.setTextColor(Color.parseColor("#bdbdbd"));


        }
        if (v == minBed) {
            propertyLL.setVisibility(View.GONE);
            categoryLL.setVisibility(View.GONE);
            communityLL.setVisibility(View.GONE);
            towerNameLL.setVisibility(View.GONE);
            cityLL.setVisibility(View.GONE);
            minPriceLL.setVisibility(View.GONE);
            maxPriceLL.setVisibility(View.GONE);
            minAreaLL.setVisibility(View.GONE);
            maxAreaLL.setVisibility(View.GONE);
            minBedLL.setVisibility(View.VISIBLE);
            maxBedLL.setVisibility(View.GONE);
            sortByLL.setVisibility(View.GONE);
            postedByLL.setVisibility(View.GONE);
            refNoLL.setVisibility(View.GONE);


            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);
            view8.setVisibility(View.GONE);
            view9.setVisibility(View.GONE);
            view10.setVisibility(View.VISIBLE);
            view11.setVisibility(View.GONE);
            view12.setVisibility(View.GONE);
            view13.setVisibility(View.GONE);
            view14.setVisibility(View.GONE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
            txt8.setTextColor(Color.parseColor("#bdbdbd"));
            txt9.setTextColor(Color.parseColor("#bdbdbd"));
            txt10.setTextColor(Color.parseColor("#000000"));
            txt11.setTextColor(Color.parseColor("#bdbdbd"));
            txt12.setTextColor(Color.parseColor("#bdbdbd"));
            txt13.setTextColor(Color.parseColor("#bdbdbd"));
            txt14.setTextColor(Color.parseColor("#bdbdbd"));

        }
        if (v == maxBed) {
            propertyLL.setVisibility(View.GONE);
            categoryLL.setVisibility(View.GONE);
            communityLL.setVisibility(View.GONE);
            towerNameLL.setVisibility(View.GONE);
            cityLL.setVisibility(View.GONE);
            minPriceLL.setVisibility(View.GONE);
            maxPriceLL.setVisibility(View.GONE);
            minAreaLL.setVisibility(View.GONE);
            maxAreaLL.setVisibility(View.GONE);
            minBedLL.setVisibility(View.GONE);
            maxBedLL.setVisibility(View.VISIBLE);
            sortByLL.setVisibility(View.GONE);
            postedByLL.setVisibility(View.GONE);
            refNoLL.setVisibility(View.GONE);


            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);
            view8.setVisibility(View.GONE);
            view9.setVisibility(View.GONE);
            view10.setVisibility(View.GONE);
            view11.setVisibility(View.VISIBLE);
            view12.setVisibility(View.GONE);
            view13.setVisibility(View.GONE);
            view14.setVisibility(View.GONE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
            txt8.setTextColor(Color.parseColor("#bdbdbd"));
            txt9.setTextColor(Color.parseColor("#bdbdbd"));
            txt10.setTextColor(Color.parseColor("#bdbdbd"));
            txt11.setTextColor(Color.parseColor("#000000"));
            txt12.setTextColor(Color.parseColor("#bdbdbd"));
            txt13.setTextColor(Color.parseColor("#bdbdbd"));
            txt14.setTextColor(Color.parseColor("#bdbdbd"));

        }
        if (v == sortBy) {
            propertyLL.setVisibility(View.GONE);
            categoryLL.setVisibility(View.GONE);
            communityLL.setVisibility(View.GONE);
            refNoLL.setVisibility(View.GONE);
            towerNameLL.setVisibility(View.GONE);
            cityLL.setVisibility(View.GONE);
            minPriceLL.setVisibility(View.GONE);
            maxPriceLL.setVisibility(View.GONE);
            minAreaLL.setVisibility(View.GONE);
            maxAreaLL.setVisibility(View.GONE);
            minBedLL.setVisibility(View.GONE);
            sortByLL.setVisibility(View.VISIBLE);
            postedByLL.setVisibility(View.GONE);
            maxBedLL.setVisibility(View.GONE);


            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);
            view8.setVisibility(View.GONE);
            view9.setVisibility(View.GONE);
            view10.setVisibility(View.GONE);
            view11.setVisibility(View.GONE);
            view12.setVisibility(View.VISIBLE);
            view13.setVisibility(View.GONE);
            view14.setVisibility(View.GONE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
            txt8.setTextColor(Color.parseColor("#bdbdbd"));
            txt9.setTextColor(Color.parseColor("#bdbdbd"));
            txt10.setTextColor(Color.parseColor("#bdbdbd"));
            txt11.setTextColor(Color.parseColor("#bdbdbd"));
            txt12.setTextColor(Color.parseColor("#000000"));
            txt13.setTextColor(Color.parseColor("#bdbdbd"));
            txt14.setTextColor(Color.parseColor("#bdbdbd"));
        }
        if (v == postedBy) {
            propertyLL.setVisibility(View.GONE);
            categoryLL.setVisibility(View.GONE);
            communityLL.setVisibility(View.GONE);
            towerNameLL.setVisibility(View.GONE);
            cityLL.setVisibility(View.GONE);
            minPriceLL.setVisibility(View.GONE);
            maxPriceLL.setVisibility(View.GONE);
            minAreaLL.setVisibility(View.GONE);
            maxAreaLL.setVisibility(View.GONE);
            minBedLL.setVisibility(View.GONE);
            maxBedLL.setVisibility(View.GONE);
            sortByLL.setVisibility(View.GONE);
            postedByLL.setVisibility(View.VISIBLE);
            refNoLL.setVisibility(View.GONE);


            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);
            view8.setVisibility(View.GONE);
            view9.setVisibility(View.GONE);
            view10.setVisibility(View.GONE);
            view11.setVisibility(View.GONE);
            view12.setVisibility(View.GONE);
            view13.setVisibility(View.VISIBLE);
            view14.setVisibility(View.GONE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
            txt8.setTextColor(Color.parseColor("#bdbdbd"));
            txt9.setTextColor(Color.parseColor("#bdbdbd"));
            txt10.setTextColor(Color.parseColor("#bdbdbd"));
            txt11.setTextColor(Color.parseColor("#bdbdbd"));
            txt12.setTextColor(Color.parseColor("#bdbdbd"));
            txt13.setTextColor(Color.parseColor("#000000"));
            txt14.setTextColor(Color.parseColor("#bdbdbd"));


        }
        if (v == refNo) {
            propertyLL.setVisibility(View.GONE);
            categoryLL.setVisibility(View.GONE);
            communityLL.setVisibility(View.GONE);
            towerNameLL.setVisibility(View.GONE);
            cityLL.setVisibility(View.GONE);
            minPriceLL.setVisibility(View.GONE);
            maxPriceLL.setVisibility(View.GONE);
            minAreaLL.setVisibility(View.GONE);
            maxAreaLL.setVisibility(View.GONE);
            minBedLL.setVisibility(View.GONE);
            maxBedLL.setVisibility(View.GONE);
            sortByLL.setVisibility(View.GONE);
            postedByLL.setVisibility(View.GONE);
            refNoLL.setVisibility(View.VISIBLE);


            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);
            view8.setVisibility(View.GONE);
            view9.setVisibility(View.GONE);
            view10.setVisibility(View.GONE);
            view11.setVisibility(View.GONE);
            view12.setVisibility(View.GONE);
            view13.setVisibility(View.GONE);
            view14.setVisibility(View.VISIBLE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
            txt8.setTextColor(Color.parseColor("#bdbdbd"));
            txt9.setTextColor(Color.parseColor("#bdbdbd"));
            txt10.setTextColor(Color.parseColor("#bdbdbd"));
            txt11.setTextColor(Color.parseColor("#bdbdbd"));
            txt12.setTextColor(Color.parseColor("#bdbdbd"));
            txt13.setTextColor(Color.parseColor("#bdbdbd"));
            txt14.setTextColor(Color.parseColor("#000000"));


        }


    }

    private void applyFilterMethod() {


        Intent intent = new Intent(FilterActivity.this, AfterFilteredPropertyActivity.class);
        intent.putExtra("prop", prop);
        intent.putExtra("cati", cati);
        intent.putExtra("comm", comm);
        intent.putExtra("towr", towr);
        intent.putExtra("cty ", cty);
        intent.putExtra("minP", minP);
        intent.putExtra("maxP", maxP);
        intent.putExtra("minA", minA);
        intent.putExtra("maxA", maxA);
        intent.putExtra("minB", minB);
        intent.putExtra("maxB", maxB);
        intent.putExtra("select_agents1", postBy);
        intent.putExtra("postBy", spinner_postedBy.getSelectedItem().toString());
        intent.putExtra("srtBy ", srtBy);
        startActivity(intent);
        finish();

    }

}
