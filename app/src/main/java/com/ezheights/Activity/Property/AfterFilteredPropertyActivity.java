package com.ezheights.Activity.Property;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ezheights.Adapter.PropertySaleMainAdapter;
import com.ezheights.Models.PropertySaleModel;
import com.ezheights.R;
import com.ezheights.Services.RequestGenerator;
import com.ezheights.Services.ResponseListener;
import com.ezheights.Utils.AppConstants;
import com.ezheights.Utils.AppPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AfterFilteredPropertyActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private PropertySaleMainAdapter mAdapter;
    LinearLayoutManager linearLayoutManager;
    String prop, cati, comm, towr, cty, minP, maxP, minA, maxA, minB, maxB, postBy, srtBy, rfNo, sldAgt, email;

    AppPreferences preferences;
    ArrayList<PropertySaleModel> arrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filtered_data_sale);

        preferences = new AppPreferences();
        prop = getIntent().getStringExtra("prop");
        cati = getIntent().getStringExtra("cati");
        comm = getIntent().getStringExtra("comm");
        towr = getIntent().getStringExtra("towr");
        cty = getIntent().getStringExtra("cty");
        minP = getIntent().getStringExtra("minP");
        maxP = getIntent().getStringExtra("maxP");
        minA = getIntent().getStringExtra("minA");
        maxA = getIntent().getStringExtra("maxA");
        minB = getIntent().getStringExtra("minB");
        maxB = getIntent().getStringExtra("maxB");
        sldAgt = getIntent().getStringExtra("postBy");
        postBy = getIntent().getStringExtra("select_agents1");
        srtBy = getIntent().getStringExtra("srtBy");
        rfNo = getIntent().getStringExtra("rfNo");
        email = preferences.getPreferences(getApplicationContext(), AppConstants.emailId);


        init();

    }

    private void init() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_filtered);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);


        if (prop == null) {
            prop = "";
        }
        if (cati == null) {
            cati = "";
        }
        if (comm == null) {
            comm = "";
        }
        if (towr == null) {
            towr = "";
        }
        if (cty == null) {
            cty = "";
        }
        if (minP == null) {
            minP = "";
        }
        if (maxP == null) {
            maxP = "";
        }
        if (minA == null) {
            minA = "";
        }
        if (maxA == null) {
            maxA = "";
        }
        if (minB == null) {
            minB = "";
        }
        if (maxB == null) {
            maxB = "";
        }
        if (postBy == null) {
            postBy = "";
        }
        if (srtBy == null) {
            srtBy = "";
        }
        if (rfNo == null) {
            rfNo = "";
        }
        if (sldAgt == null) {
            sldAgt = "";
        }
        if (email == null) {
            email = "";
        }
        getPropertySaleData();
    }

    private void getPropertySaleData() {


        String UrlFilterdData = AppConstants.ApplyPropertyFilter + "&txt_property=" + prop + "&txt_cat=" + cati + "&txt_comm=" + comm + "&txt_tower=" + towr + "&txt_city=" + cty +
                "&txt_priceMin=" + minP + "&txt_priceMax=" + maxP + "&txt_areaMin=" + minA + "&txt_areaMax=" + maxA + "&txt_bedMin=" + minB + "&txt_bedMax=" + maxB +
                "&txt_sort=" + srtBy + "&txt_ref=" + rfNo + "&txt_post=" + sldAgt + "&select_agents1=" + postBy + "&email=" + email;

        Log.e("Property Filter", UrlFilterdData);

        RequestGenerator.makeGetRequest(AfterFilteredPropertyActivity.this, UrlFilterdData, true, new ResponseListener() {
            @Override
            public void onError(VolleyError error) {
                Toast.makeText(AfterFilteredPropertyActivity.this, "Some Problem Occured", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(String string) throws JSONException {

                if (!string.equals("")) {

                    JSONObject jsonObject1 = new JSONObject(string);

                    JSONArray jsonArray = jsonObject1.getJSONArray("property");
                    arrayList.clear();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);

                        PropertySaleModel propertySaleModel = new PropertySaleModel();
                        propertySaleModel.setProperty_id(jsonObject2.getString("newItemID"));
                        propertySaleModel.setProperty_name(jsonObject2.getString("newTitle"));
                        propertySaleModel.setProperty_description(jsonObject2.getString("newTitle1"));
                        propertySaleModel.setProperty_image(jsonObject2.getString("newPrimaryImage"));
                        propertySaleModel.setProperty_price(jsonObject2.getString("prices"));
                        propertySaleModel.setProperty_area(jsonObject2.getString("newTotalArea"));
                        propertySaleModel.setProperty_no_bath(jsonObject2.getString("newBathrooms"));
                        propertySaleModel.setProperty_no_bed(jsonObject2.getString("newBedrooms"));
                        propertySaleModel.setProperty_like_status(jsonObject2.getString("LikeID"));


                        arrayList.add(propertySaleModel);

                    }
                    mAdapter = new PropertySaleMainAdapter(AfterFilteredPropertyActivity.this, arrayList);
                    recyclerView.setAdapter(mAdapter);
                }

            }


        });


    }
}
