package com.ezheights.Activity.Property;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ezheights.Adapter.PropertyRentMainAdapter;
import com.ezheights.Models.PropertyRentModel;
import com.ezheights.R;
import com.ezheights.Services.RequestGenerator;
import com.ezheights.Services.ResponseListener;
import com.ezheights.Utils.AppConstants;
import com.ezheights.Utils.AppPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PropertyRentActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView recyclerView;
    private PropertyRentMainAdapter mAdapter;

    ArrayList<PropertyRentModel> arrayList = new ArrayList<>();
    Activity mContext;
    FloatingActionButton filterButton;
    String searchKeyword;

    private boolean isLoading;
    private int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    String emailId, isLogin;
    private AppPreferences appPreferance;
    boolean lastcoloum = false;
    private ProgressBar progressBar1;
    LinearLayoutManager linearLayoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_rent);
        mContext = PropertyRentActivity.this;

        appPreferance = new AppPreferences();
        isLogin = appPreferance.getPreferences(getApplicationContext(), AppConstants.isLogin);
        emailId = appPreferance.getPreferences(getApplicationContext(), AppConstants.emailId);

        init();
    }

    private void init() {
        filterButton = (FloatingActionButton) findViewById(R.id.filter_property_rent);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_rent);

        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        searchKeyword = getIntent().getStringExtra("Search");
        if (searchKeyword == null) {
            searchKeyword = "";
        }
        if (emailId == null) {
            emailId = "";
        }

        getPropertyRentData();
        filterButton.setOnClickListener(this);
    }

    private void getPropertyRentData() {

        try {

//            JSONObject jsonObject = new JSONObject();
//            jsonObject.put("searchdata", searchKeyword);
//            jsonObject.put("email", "");
//
//            Log.d("Property Rent Request", jsonObject.toString());  String Url1 = AppConstants.PropertyForSaleData + "&searchdata=" + searchKeyword + "&email=" + "";

            String Url1 = AppConstants.PropertyForRentData + "&searchdata=" + searchKeyword + "&email=" + emailId;
            Log.d("UIUrl", Url1);
            RequestGenerator.makeGetRequest(mContext, Url1, true, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {
                    Toast.makeText(mContext, "Some Problem Occured", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(String string) throws JSONException {

                    if (!string.equals("")) {

                        Log.d("Property Rent Response", string);
                        JSONObject jsonObject1 = new JSONObject(string);

                        JSONArray jsonArray = jsonObject1.getJSONArray("property");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject2 = jsonArray.getJSONObject(i);

                            PropertyRentModel propertyRentModel = new PropertyRentModel();
                            propertyRentModel.setProperty_id(jsonObject2.getString("newItemID"));
                            propertyRentModel.setProperty_name(jsonObject2.getString("newTitle"));
                            propertyRentModel.setProperty_description(jsonObject2.getString("newTitle1"));
                            propertyRentModel.setProperty_image(jsonObject2.getString("newPrimaryImage"));
                            propertyRentModel.setProperty_price(jsonObject2.getString("prices"));
                            propertyRentModel.setProperty_area(jsonObject2.getString("newTotalArea"));
                            propertyRentModel.setProperty_no_bath(jsonObject2.getString("newBathrooms"));
                            propertyRentModel.setProperty_no_bed(jsonObject2.getString("newBedrooms"));
                            propertyRentModel.setProperty_like_status(jsonObject2.getString("LikeID"));

                            arrayList.add(propertyRentModel);

                        }
                        mAdapter = new PropertyRentMainAdapter(PropertyRentActivity.this, arrayList);
                        recyclerView.setAdapter(mAdapter);

                    }

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View v) {

        if (v == filterButton) {
            Intent intent = new Intent(mContext, FilterActivity.class);
            intent.putExtra("propType", "rent");
            Log.d("propType", "rent");
            startActivity(intent);

        }
    }
}
