package com.ezheights.Activity.Property;

import android.app.Dialog;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ezheights.Adapter.HomeViewPagerAdapter;
import com.ezheights.Models.PropSaleDetailModel;
import com.ezheights.Services.RequestGenerator;
import com.ezheights.Services.ResponseListener;
import com.ezheights.Utils.AppConstants;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import com.ezheights.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by NileshM on 24/3/17.
 */

public class PropertySaleDetailsActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {

    private TextView tv_ReadMore, tv_Title, tv_Price, tv_propHosted, tv_propRefNo, tv_BedCount, tv_BathCount, tv_AreaCount, tv_shortDes;
    CircleImageView img_Primary;
    private ImageView img_Facebook, img_Twitter, img_LinkedIn, img_Google, img_Share, img_Like, img_Back;
    private ViewPager viewPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;

    FloatingActionMenu materialDesignFAM;
    FloatingActionButton makeCall, makeSms;
    private GoogleMap mMap;
    private ArrayList<String> ImagesArray1 = new ArrayList<String>();
    ArrayList<PropSaleDetailModel> arrayList = new ArrayList<>();
    String proprId, logDes, titleDes, lat, langg, City;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_sale_details);


        /* For Map */
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(PropertySaleDetailsActivity.this);

        proprId = getIntent().getStringExtra("PropertyId");
        Log.d("propid", proprId);
        init();

    }

    private void init() {

        tv_ReadMore = (TextView) findViewById(R.id.tv_ReadMore);
        tv_Title = (TextView) findViewById(R.id.tv_Title);
        tv_Price = (TextView) findViewById(R.id.tv_Price);
        tv_propRefNo = (TextView) findViewById(R.id.tv_propRefNo);
        tv_propHosted = (TextView) findViewById(R.id.tv_propHosted);
        tv_BedCount = (TextView) findViewById(R.id.tv_BedCount);
        tv_BathCount = (TextView) findViewById(R.id.tv_BathCount);
        tv_AreaCount = (TextView) findViewById(R.id.tv_AreaCount);
        tv_shortDes = (TextView) findViewById(R.id.tv_shortDes);

        img_Primary = (CircleImageView) findViewById(R.id.img_Owner);

        img_Like = (ImageView) findViewById(R.id.img_Like);
        img_Share = (ImageView) findViewById(R.id.img_Share);
        img_Back = (ImageView) findViewById(R.id.img_Back);
        materialDesignFAM = (FloatingActionMenu) findViewById(R.id.floating_action_menu);
        makeCall = (FloatingActionButton) findViewById(R.id.make_call);
        makeSms = (FloatingActionButton) findViewById(R.id.make_sms);

        viewPager = (ViewPager) findViewById(R.id.img_Main);
        tv_ReadMore.setOnClickListener(this);
        img_Like.setOnClickListener(this);
        img_Share.setOnClickListener(this);
        img_Back.setOnClickListener(this);
        makeCall.setOnClickListener(this);
        makeSms.setOnClickListener(this);


        if (langg == null) {
            langg = "72.1818";
        }
        if (lat == null) {
            lat = "19.7272";
        }
        getAllDataDetail();
    }

    private void getAllDataDetail() {

//        try {
//
//
//            JSONObject jsonObject = new JSONObject();

//            jsonObject.put("ID", proprId);
        String UrlDetail = AppConstants.PropertyDetailsData + "&ID=" + proprId;
        Log.e("Property Detail Request", UrlDetail);
        RequestGenerator.makeGetRequest(PropertySaleDetailsActivity.this, UrlDetail, true, new ResponseListener() {
            @Override
            public void onError(VolleyError error) {
                Toast.makeText(PropertySaleDetailsActivity.this, "Some Problem Occured", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(String string) throws JSONException {

                if (!string.equals("")) {
                    Log.e("onSuccess", string);

                    JSONObject jsonObject1 = new JSONObject(string);

                    tv_Title.setText(jsonObject1.getString("title"));
                    tv_Price.setText("AED " + jsonObject1.getString("Price") + ".00");
                    tv_propHosted.setText("Hosted By " + jsonObject1.getString("CompanyName"));
                    tv_propRefNo.setText("Ref. No " + jsonObject1.getString("PropRef"));
                    tv_BedCount.setText(jsonObject1.getString("Bedrooms"));
                    tv_BathCount.setText(jsonObject1.getString("Bathrooms"));
                    tv_AreaCount.setText(jsonObject1.getString("TotalArea"));
                    tv_shortDes.setText(jsonObject1.getString("ShortDesc"));
                    logDes = jsonObject1.getString("LongDesc");
                    titleDes = jsonObject1.getString("title");
                    lat = jsonObject1.getString("map_lat");
                    langg = jsonObject1.getString("map_long");
                    City = jsonObject1.getString("city_name");

                    JSONArray jsonArray = jsonObject1.getJSONArray("imagesall");
                    ImagesArray1.clear();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        String ima = jsonArray.get(i).toString();
                        ImagesArray1.add(ima);
                    }


                    if (jsonObject1.getString("CompanyLogo").equals("")) {
                        Picasso.with(PropertySaleDetailsActivity.this).load(R.drawable.ezheights_logo).into(img_Primary);

                    } else {
                        Picasso.with(PropertySaleDetailsActivity.this).load(jsonObject1.getString("CompanyLogo")).placeholder(R.drawable.watermark_property).into(img_Primary);
                    }
                    setUpPager();

                }

            }
        });


    }

    @Override
    public void onClick(View v) {

        if (v == img_Back) {
            onBackPressed();

        }
        if (v == tv_ReadMore) {
            readMoreDialog();

        }

        if (v == img_Like) {


        }


        if (v == img_Share) {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_TEXT, "EZHeights");
            startActivity(Intent.createChooser(shareIntent, "Share link using"));

        }

        if (v == makeCall) {

            Toast.makeText(this, "Call Under process", Toast.LENGTH_SHORT).show();
        }
        if (v == makeSms) {

            Toast.makeText(this, "SMS Under process", Toast.LENGTH_SHORT).show();
        }

    }


    private void readMoreDialog() {

        final Dialog dialog = new Dialog(PropertySaleDetailsActivity.this, R.style.MyDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.read_more_dialog);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogSlideUpDownAnim;

        TextView toolbar_title = (TextView) dialog.findViewById(R.id.toolbar_title);
        TextView textView = (TextView) dialog.findViewById(R.id.tv_ReadMore);

        toolbar_title.setText(titleDes);
        textView.setText(logDes);
        dialog.show();

    }

    private void setUpPager() {
//        for (int i = 0; i < ImagesArray1.s; i++)
//            ImagesArray.add(IMAGES[i]);

        viewPager.setAdapter(new HomeViewPagerAdapter(PropertySaleDetailsActivity.this, ImagesArray1));
        NUM_PAGES = ImagesArray1.size();

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 2000, 2000);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney, Australia, and move the camera.
//        LatLng sydney = new LatLng(19.0760, 72.8777);
        LatLng sydney = new LatLng(Double.parseDouble(lat), Double.parseDouble(langg));
        mMap.addMarker(new MarkerOptions().position(sydney).title(City));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

    }


}
