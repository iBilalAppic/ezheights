package com.ezheights.Activity.Property;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ezheights.Activity.MainActivity;
import com.ezheights.Adapter.PropertySaleMainAdapter;
import com.ezheights.Models.PropertySaleModel;
import com.ezheights.R;
import com.ezheights.Services.RequestGenerator;
import com.ezheights.Services.ResponseListener;
import com.ezheights.Utils.AppConstants;
import com.ezheights.Utils.AppPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by NileshM on 22/3/17.
 */

public class PropertySaleActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView recyclerView;
    private PropertySaleMainAdapter mAdapter;
    ArrayList<PropertySaleModel> arrayList = new ArrayList<>();
    Activity mContext;
    String PropId, emailId, isLogin;
    FloatingActionButton filterButton;

    String searchKeyword;
    private boolean isLoading;
    private int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    private ProgressBar progressBar1;
    boolean lastcoloum = false;
    LinearLayoutManager linearLayoutManager;
    private AppPreferences appPreferance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_sale);
        mContext = PropertySaleActivity.this;
        appPreferance = new AppPreferences();
        isLogin = appPreferance.getPreferences(getApplicationContext(), AppConstants.isLogin);
        emailId = appPreferance.getPreferences(getApplicationContext(), AppConstants.emailId);

        Log.d("email prop", emailId);
        init();
    }

    private void init() {

        filterButton = (FloatingActionButton) findViewById(R.id.filter_property_sale);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        progressBar1 = (ProgressBar) findViewById(R.id.progress_bar);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        searchKeyword = getIntent().getStringExtra("Search");
        if (searchKeyword == null) {
            searchKeyword = "";
        }
        if (emailId == null) {
            emailId = "";
        }
        getPropertySaleData();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!(dy > 0)) {

                } else if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold) && !(lastcoloum)) {

                    isLoading = true;
                    progressBar1.setVisibility(View.VISIBLE);
                    progressBar1.setIndeterminate(true);

                    isLoading = true;

                }
            }

        });

        filterButton.setOnClickListener(this);
    }

    private void getPropertySaleData() {


        try {
//            JSONObject jsonObject = new JSONObject();
//            jsonObject.put("searchdata", searchKeyword);
//            jsonObject.put("email", "");
//
//            Log.d("Property Sale Request", jsonObject.toString());


            String Url1 = AppConstants.PropertyForSaleData + "&searchdata=" + searchKeyword + "&email=" + emailId;
            Log.d("UIUrl", Url1);
            RequestGenerator.makeGetRequest(mContext, Url1, true, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {
                    Toast.makeText(mContext, "Some Problem Occured", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(String string) throws JSONException {

                    if (!string.equals("")) {

                        Log.d("Property Sale Response", string);
                        JSONObject jsonObject1 = new JSONObject(string);

                        JSONArray jsonArray = jsonObject1.getJSONArray("property");
                        arrayList.clear();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject2 = jsonArray.getJSONObject(i);

                            PropertySaleModel propertySaleModel = new PropertySaleModel();
                            propertySaleModel.setProperty_id(jsonObject2.getString("newItemID"));
                            propertySaleModel.setProperty_name(jsonObject2.getString("newTitle"));
                            propertySaleModel.setProperty_description(jsonObject2.getString("newTitle1"));
                            propertySaleModel.setProperty_image(jsonObject2.getString("newPrimaryImage"));
                            propertySaleModel.setProperty_price(jsonObject2.getString("prices"));
                            propertySaleModel.setProperty_area(jsonObject2.getString("newTotalArea"));
                            propertySaleModel.setProperty_no_bath(jsonObject2.getString("newBathrooms"));
                            propertySaleModel.setProperty_no_bed(jsonObject2.getString("newBedrooms"));
                            propertySaleModel.setProperty_like_status(jsonObject2.getString("LikeID"));


                            arrayList.add(propertySaleModel);

                        }
                        mAdapter = new PropertySaleMainAdapter(PropertySaleActivity.this, arrayList);
//                        Log.d("proprtyIID", arrayList.get(0).getProperty_id());
                        recyclerView.setAdapter(mAdapter);
                    }

                }


            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        if (v == filterButton) {
            Intent intent = new Intent(mContext, FilterActivity.class);
            intent.putExtra("propType", "sale");
            Log.d("propType", "sale");
            startActivity(intent);

        }
    }
}
