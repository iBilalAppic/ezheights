package com.ezheights.Activity.Job;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ezheights.R;
import com.ezheights.Services.RequestGenerator;
import com.ezheights.Services.ResponseListener;
import com.ezheights.Utils.AppConstants;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import org.json.JSONException;
import org.json.JSONObject;

public class JobDetailActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tv_ReadMore, tv_Title, tv_Sal, tv_Exp, tv_Quali, tv_SalPaid, tv_EmpType, tv_PostedBy, tv_PostedOn, tv_ShortDes;
    private ImageView img_Facebook, img_Twitter, img_LinkedIn, img_Google, img_Share, img_Like, img_Back;

    FloatingActionMenu materialDesignFAM;
    FloatingActionButton makeCall, makeSms;

    String jobId, shortDesc, title, longDesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_detail);


        jobId = getIntent().getStringExtra("jobId");
        Log.e("jobId", jobId);
        init();
    }

    private void init() {

        tv_ReadMore = (TextView) findViewById(R.id.tv_ReadMore);
        tv_Title = (TextView) findViewById(R.id.tv_Title);
        tv_Sal = (TextView) findViewById(R.id.tv_Sal);
        tv_Exp = (TextView) findViewById(R.id.tv_Expe);
        tv_Quali = (TextView) findViewById(R.id.tv_Quali);
        tv_SalPaid = (TextView) findViewById(R.id.tv_SalPaid);
        tv_EmpType = (TextView) findViewById(R.id.tv_EmpType);
        tv_PostedBy = (TextView) findViewById(R.id.tv_postedBy);
        tv_PostedOn = (TextView) findViewById(R.id.tv_postedOn);
        tv_ShortDes = (TextView) findViewById(R.id.tv_ShortDesc);


        img_Like = (ImageView) findViewById(R.id.img_Like);
        img_Share = (ImageView) findViewById(R.id.img_Share);
        img_Back = (ImageView) findViewById(R.id.img_Back);

        materialDesignFAM = (FloatingActionMenu) findViewById(R.id.floating_action_menu);
        makeCall = (FloatingActionButton) findViewById(R.id.make_call);
        makeSms = (FloatingActionButton) findViewById(R.id.make_sms);


        tv_ReadMore.setOnClickListener(this);
        img_Like.setOnClickListener(this);
        img_Share.setOnClickListener(this);
        img_Back.setOnClickListener(this);
        makeCall.setOnClickListener(this);
        makeSms.setOnClickListener(this);

        getJobDetail();
    }

    private void getJobDetail() {

//        try {
//            JSONObject jsonObject = new JSONObject();
//            jsonObject.put("ID", jobId);
        String UrlJobDetail = AppConstants.JobDetailsData + "&ID=" + jobId;
//            Log.e("Job Detail Resquest", jsonObject.toString());
        RequestGenerator.makeGetRequest(JobDetailActivity.this, UrlJobDetail, true, new ResponseListener() {
            @Override
            public void onError(VolleyError error) {
                Toast.makeText(JobDetailActivity.this, "Some Problem Occured", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(String string) throws JSONException {

                if (!string.equals("")) {
                    JSONObject jsonObject1 = new JSONObject(string);
                    tv_Title.setText(jsonObject1.getString("title"));
                    tv_Sal.setText("AED " + jsonObject1.getString("salary"));
                    tv_Exp.setText(jsonObject1.getString("experiance"));
                    tv_Quali.setText(jsonObject1.getString("education"));
                    tv_SalPaid.setText(jsonObject1.getString("salary_period"));
                    tv_EmpType.setText(jsonObject1.getString("type"));
                    tv_PostedBy.setText("Catigory Id :" + jsonObject1.getString("cat_id"));
                    tv_PostedOn.setText("Posted On :" + jsonObject1.getString("date"));
                    tv_ShortDes.setText(jsonObject1.getString("shortdes"));

                    title = jsonObject1.getString("title");

                    longDesc = jsonObject1.getString("longdescription");

                }


            }
        });

//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public void onClick(View v) {
        if (v == img_Back) {
            onBackPressed();

        }

        if (v == tv_ReadMore) {
            readMoreDialog();

        }

        if (v == img_Like) {


        }

        if (v == img_Share) {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_TEXT, "EZHeights");
            startActivity(Intent.createChooser(shareIntent, "Share link using"));

        }

        if (v == makeCall) {

            Toast.makeText(this, "Call Under process", Toast.LENGTH_SHORT).show();
        }
        if (v == makeSms) {

            Toast.makeText(this, "SMS Under process", Toast.LENGTH_SHORT).show();
        }


    }


    private void readMoreDialog() {

        final Dialog dialog = new Dialog(JobDetailActivity.this, R.style.MyDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.read_more_dialog);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogSlideUpDownAnim;

        TextView toolbar_title = (TextView) dialog.findViewById(R.id.toolbar_title);
        TextView textView = (TextView) dialog.findViewById(R.id.tv_ReadMore);

        toolbar_title.setText(title);
        textView.setText(longDesc);
        dialog.show();

    }

}
