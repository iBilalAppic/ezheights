package com.ezheights.Activity.Job;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ezheights.Adapter.JobMainAdapter;
import com.ezheights.Models.JobMainModel;
import com.ezheights.R;
import com.ezheights.Services.RequestGenerator;
import com.ezheights.Services.ResponseListener;
import com.ezheights.Utils.AppConstants;
import com.ezheights.Utils.AppPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class JobActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView recyclerView_job;
    private JobMainAdapter mAdapter;

    ArrayList<JobMainModel> arrayList = new ArrayList<>();
    Activity mContext;

    private ProgressBar progressBar1;
    FloatingActionButton filterButton;
    LinearLayoutManager mLayoutManager1;
    String searchKeyword;
    String emailId, isLogin;
    private AppPreferences appPreferance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job);
        mContext = JobActivity.this;

        appPreferance = new AppPreferences();
        isLogin = appPreferance.getPreferences(getApplicationContext(), AppConstants.isLogin);
        emailId = appPreferance.getPreferences(getApplicationContext(), AppConstants.emailId);

        init();
    }

    private void init() {

        filterButton = (FloatingActionButton) findViewById(R.id.filter_job);
        progressBar1 = (ProgressBar) findViewById(R.id.progress_bar);
        recyclerView_job = (RecyclerView) findViewById(R.id.recyclerView_job);
        mLayoutManager1 = new LinearLayoutManager(this);
        recyclerView_job.setLayoutManager(mLayoutManager1);
        mLayoutManager1.setOrientation(LinearLayoutManager.VERTICAL);


        searchKeyword = getIntent().getStringExtra("Search");
        if (searchKeyword == null) {
            searchKeyword = "";
        }
        if (emailId == null) {
            emailId = "";
        }
        filterButton.setOnClickListener(this);


        getJobMainData();
//        mAdapter = new JobMainAdapter(JobActivity.this);
//        recyclerView_job.setAdapter(mAdapter);

    }

    private void getJobMainData() {
        String UrlJobMainData = AppConstants.JobDefaultData + "&search=" + searchKeyword + "&email=" + emailId;
        Log.e("Job Request", UrlJobMainData);


//        try {
//            JSONObject jsonObject = new JSONObject();
//            jsonObject.put("search", searchKeyword);
//            jsonObject.put("email", "");

        RequestGenerator.makeGetRequest(JobActivity.this, UrlJobMainData, true, new ResponseListener() {
            @Override
            public void onError(VolleyError error) {
                Toast.makeText(mContext, "Some Problem Occured", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onSuccess(String string) throws JSONException {
                if (!string.equals("")) {
                    Log.e("Classfied Response", string);
                    JSONObject jsonObject1 = new JSONObject(string);

                    JSONArray jsonArray = jsonObject1.getJSONArray("jobs");
                    arrayList.clear();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);

                        JobMainModel jobMainModel = new JobMainModel();
                        jobMainModel.setJobid(jsonObject2.getString("jobid"));
                        jobMainModel.setTitle(jsonObject2.getString("title"));
                        jobMainModel.setPrice(jsonObject2.getString("price"));
                        jobMainModel.setNewexperiance(jsonObject2.getString("newexperiance"));
                        jobMainModel.setNeweducation(jsonObject2.getString("neweducation"));
                        jobMainModel.setEmptype(jsonObject2.getString("emptype"));
                        jobMainModel.setFavourite(jsonObject2.getString("favourite"));

                        arrayList.add(jobMainModel);

                    }

                    mAdapter = new JobMainAdapter(JobActivity.this, arrayList);
//                    Log.d("MotorIID", arrayList.get(0).getMotor_id());
                    recyclerView_job.setAdapter(mAdapter);
                }
            }
        });
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(mContext, FilterJobData.class);
        startActivity(intent);

    }
}
