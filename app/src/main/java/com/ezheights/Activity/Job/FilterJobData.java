package com.ezheights.Activity.Job;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ezheights.Activity.Motor.FilterMotorData;
import com.ezheights.R;
import com.ezheights.Services.RequestGenerator;
import com.ezheights.Services.ResponseListener;
import com.ezheights.Utils.AppConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FilterJobData extends AppCompatActivity implements View.OnClickListener {

    Context mContext;
    private RelativeLayout selectTypeLL, categoryTypeLL, lookingLL, nationalityLL, currntLocationLL, experienceLL, salaryRangeLL;
    RadioGroup radiogrp_selectType, radiogrp_categoryType, radiogrp_nationality, radiogrp_currntLocation, radiogrp_experience, radiogrp_salaryRange;
    private EditText lookingFor;
    private LinearLayout selectType, categoryType, looking, nationality, currntLocation, experience, salaryRange;
    private View view1, view2, view3, view4, view5, view6, view7;
    private TextView txt1, txt2, txt3, txt4, txt5, txt6, txt7;
    private TextView clearAll, cancelBtn, appltFilter;


    String sltTyp, catTyp, lkfr, nat, loc, exp, sal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_job_data);

        mContext = FilterJobData.this;

        init();
    }

    private void init() {
        selectTypeLL = (RelativeLayout) findViewById(R.id.selectTypeMainLayout);
        categoryTypeLL = (RelativeLayout) findViewById(R.id.categoryTypeMainLayout);
        lookingLL = (RelativeLayout) findViewById(R.id.lookinForMainLayout);
        nationalityLL = (RelativeLayout) findViewById(R.id.nationalityMainLayout);
        currntLocationLL = (RelativeLayout) findViewById(R.id.currentLocaMainLayout);
        experienceLL = (RelativeLayout) findViewById(R.id.experenceMainLayout);
        salaryRangeLL = (RelativeLayout) findViewById(R.id.salaryRangeMainLayout);

        selectType = (LinearLayout) findViewById(R.id.selectType);
        categoryType = (LinearLayout) findViewById(R.id.categoryType);
        looking = (LinearLayout) findViewById(R.id.lookinFor);
        nationality = (LinearLayout) findViewById(R.id.nationality);
        currntLocation = (LinearLayout) findViewById(R.id.currentLoca);
        experience = (LinearLayout) findViewById(R.id.experence);
        salaryRange = (LinearLayout) findViewById(R.id.salaryRange);

        lookingFor = (EditText) findViewById(R.id.lookinForText);

        view1 = (View) findViewById(R.id.viewJob1);
        view2 = (View) findViewById(R.id.viewJob2);
        view3 = (View) findViewById(R.id.viewJob3);
        view4 = (View) findViewById(R.id.viewJob4);
        view5 = (View) findViewById(R.id.viewJob5);
        view6 = (View) findViewById(R.id.viewJob6);
        view7 = (View) findViewById(R.id.viewJob7);

        txt1 = (TextView) findViewById(R.id.txtJob1);
        txt2 = (TextView) findViewById(R.id.txtJob2);
        txt3 = (TextView) findViewById(R.id.txtJob3);
        txt4 = (TextView) findViewById(R.id.txtJob4);
        txt5 = (TextView) findViewById(R.id.txtJob5);
        txt6 = (TextView) findViewById(R.id.txtJob6);
        txt7 = (TextView) findViewById(R.id.txtJob7);

        clearAll = (TextView) findViewById(R.id.clearAll);
        cancelBtn = (TextView) findViewById(R.id.cancel);
        appltFilter = (TextView) findViewById(R.id.applyfilter);

        radiogrp_selectType = (RadioGroup) findViewById(R.id.radiogrp_selectType);
        radiogrp_categoryType = (RadioGroup) findViewById(R.id.radiogrp_categoryType);
        radiogrp_nationality = (RadioGroup) findViewById(R.id.radiogrp_nationality);
        radiogrp_currntLocation = (RadioGroup) findViewById(R.id.radiogrp_curntLoca);
        radiogrp_experience = (RadioGroup) findViewById(R.id.radiogrp_experence);
        radiogrp_salaryRange = (RadioGroup) findViewById(R.id.radiogrp_salaryRange);


        selectType.setOnClickListener(this);
        categoryType.setOnClickListener(this);
        looking.setOnClickListener(this);
        nationality.setOnClickListener(this);
        currntLocation.setOnClickListener(this);
        experience.setOnClickListener(this);
        salaryRange.setOnClickListener(this);

        clearAll.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);
        appltFilter.setOnClickListener(this);


        if (sltTyp == null) {
            sltTyp = "";
        }
        if (catTyp == null) {
            catTyp = "";
        }
        if (lkfr == null) {
            lkfr = "";
        }
        if (sal == null) {
            sal = "";
        }
        if (nat == null) {
            nat = "";
        }
        if (loc == null) {
            loc = "";
        }
        if (exp == null) {
            exp = "";
        }

        lkfr = lookingFor.getText().toString();

        getALLJobFilter();

    }

    private void getALLJobFilter() {

        RequestGenerator.makeGetRequest(FilterJobData.this, AppConstants.PreJobFilter, true, new ResponseListener() {
            @Override
            public void onError(VolleyError error) {
                Toast.makeText(FilterJobData.this, "Problem Occured", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(String string) throws JSONException {
                if (!string.equals("")) {
                    JSONObject jsonObject = new JSONObject(string);

                    JSONArray jsonArraySelType = jsonObject.getJSONArray("cat_detail");
                    radiogrp_selectType.removeAllViews();
                    for (int i = 0; i < jsonArraySelType.length(); i++) {
                        JSONObject jsonObject2 = jsonArraySelType.getJSONObject(i);
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonObject2.getString("CatName"));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_selectType.addView(rdbtn_for_cat, rprms);
                        radiogrp_selectType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        sltTyp = (String) btn.getText();
                                        return;
                                    }
                                }
                            }
                        });

                    }
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
                    JSONArray jsonArrayCatType = jsonObject.getJSONArray("type");
                    radiogrp_categoryType.removeAllViews();
                    for (int i = 0; i < jsonArrayCatType.length(); i++) {
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        final RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonArrayCatType.getString(i));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_categoryType.addView(rdbtn_for_cat, rprms);

                        radiogrp_categoryType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        catTyp = (String) btn.getText();

                                        return;
                                    }
                                }
                            }
                        });
                    }

/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
                    JSONArray jsonArrayNatio = jsonObject.getJSONArray("Country_detail");
                    radiogrp_nationality.removeAllViews();
                    for (int i = 0; i < jsonArrayNatio.length(); i++) {
                        JSONObject jsonObject2 = jsonArrayNatio.getJSONObject(i);
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonObject2.getString("CountryName"));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_nationality.addView(rdbtn_for_cat, rprms);
                        radiogrp_nationality.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        nat
                                                = (String) btn.getText();
                                        return;
                                    }
                                }
                            }
                        });

                    }

/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

                    JSONArray jsonArrayCurLoca = jsonObject.getJSONArray("location");
                    radiogrp_currntLocation.removeAllViews();
                    for (int i = 0; i < jsonArrayCurLoca.length(); i++) {
                        JSONObject jsonObject2 = jsonArrayCurLoca.getJSONObject(i);
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonObject2.getString("LocationName"));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_currntLocation.addView(rdbtn_for_cat, rprms);
                        radiogrp_currntLocation.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        loc = (String) btn.getText();
                                        return;
                                    }
                                }
                            }
                        });

                    }
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
                    JSONArray jsonArrayExp = jsonObject.getJSONArray("Experience");
                    radiogrp_experience.removeAllViews();
                    for (int i = 0; i < jsonArrayExp.length(); i++) {
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonArrayExp.getString(i));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_experience.addView(rdbtn_for_cat, rprms);

                        radiogrp_experience.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        exp = (String) btn.getText();
                                        return;
                                    }
                                }
                            }
                        });
                    }

/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
                    JSONArray jsonArraySal = jsonObject.getJSONArray("Salary");
                    radiogrp_salaryRange.removeAllViews();
                    for (int i = 0; i < jsonArraySal.length(); i++) {
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonArraySal.getString(i));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_salaryRange.addView(rdbtn_for_cat, rprms);

                        radiogrp_salaryRange.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        sal = (String) btn.getText();
                                        return;
                                    }
                                }
                            }
                        });
                    }

                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == clearAll) {

        }
        if (v == cancelBtn) {
            onBackPressed();

        }
        if (v == appltFilter) {

            applyJobFilter();
        }

        if (v == selectType) {
            selectTypeLL.setVisibility(View.VISIBLE);
            categoryTypeLL.setVisibility(View.GONE);
            lookingLL.setVisibility(View.GONE);
            nationalityLL.setVisibility(View.GONE);
            currntLocationLL.setVisibility(View.GONE);
            experienceLL.setVisibility(View.GONE);
            salaryRangeLL.setVisibility(View.GONE);

            view1.setVisibility(View.VISIBLE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);


            txt1.setTextColor(Color.parseColor("#000000"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
        }
        if (v == categoryType) {
            selectTypeLL.setVisibility(View.GONE);
            categoryTypeLL.setVisibility(View.VISIBLE);
            lookingLL.setVisibility(View.GONE);
            nationalityLL.setVisibility(View.GONE);
            currntLocationLL.setVisibility(View.GONE);
            experienceLL.setVisibility(View.GONE);
            salaryRangeLL.setVisibility(View.GONE);

            view1.setVisibility(View.GONE);
            view2.setVisibility(View.VISIBLE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);


            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#000000"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));

        }
        if (v == looking) {

            selectTypeLL.setVisibility(View.GONE);
            categoryTypeLL.setVisibility(View.GONE);
            lookingLL.setVisibility(View.VISIBLE);
            nationalityLL.setVisibility(View.GONE);
            currntLocationLL.setVisibility(View.GONE);
            experienceLL.setVisibility(View.GONE);
            salaryRangeLL.setVisibility(View.GONE);

            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.VISIBLE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#000000"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
        }
        if (v == nationality) {

            selectTypeLL.setVisibility(View.GONE);
            categoryTypeLL.setVisibility(View.GONE);
            lookingLL.setVisibility(View.GONE);
            nationalityLL.setVisibility(View.VISIBLE);
            currntLocationLL.setVisibility(View.GONE);
            experienceLL.setVisibility(View.GONE);
            salaryRangeLL.setVisibility(View.GONE);

            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.VISIBLE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#000000"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));

        }
        if (v == currntLocation) {
            selectTypeLL.setVisibility(View.GONE);
            categoryTypeLL.setVisibility(View.GONE);
            lookingLL.setVisibility(View.GONE);
            nationalityLL.setVisibility(View.GONE);
            currntLocationLL.setVisibility(View.VISIBLE);
            experienceLL.setVisibility(View.GONE);
            salaryRangeLL.setVisibility(View.GONE);

            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.VISIBLE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#000000"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));

        }
        if (v == experience) {
            selectTypeLL.setVisibility(View.GONE);
            categoryTypeLL.setVisibility(View.GONE);
            lookingLL.setVisibility(View.GONE);
            nationalityLL.setVisibility(View.GONE);
            currntLocationLL.setVisibility(View.GONE);
            experienceLL.setVisibility(View.VISIBLE);
            salaryRangeLL.setVisibility(View.GONE);

            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.VISIBLE);
            view7.setVisibility(View.GONE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#000000"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));

        }
        if (v == salaryRange) {

            selectTypeLL.setVisibility(View.GONE);
            categoryTypeLL.setVisibility(View.GONE);
            lookingLL.setVisibility(View.GONE);
            nationalityLL.setVisibility(View.GONE);
            currntLocationLL.setVisibility(View.GONE);
            experienceLL.setVisibility(View.GONE);
            salaryRangeLL.setVisibility(View.VISIBLE);

            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.VISIBLE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#000000"));
        }

    }

    private void applyJobFilter() {

        Intent intent = new Intent(FilterJobData.this, AfterFilterJob.class);

        intent.putExtra("sltTyp", sltTyp);
        intent.putExtra("catTyp", catTyp);
        intent.putExtra("lkfr", lkfr);
        intent.putExtra("nat", nat);
        intent.putExtra("loc", loc);
        intent.putExtra("exp", exp);
        intent.putExtra("sal", sal);
        startActivity(intent);
        finish();
    }
}
