package com.ezheights.Activity.Job;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ezheights.Activity.Motor.AfterFilteredMotor;
import com.ezheights.Adapter.JobMainAdapter;
import com.ezheights.Models.JobMainModel;
import com.ezheights.R;
import com.ezheights.Services.RequestGenerator;
import com.ezheights.Services.ResponseListener;
import com.ezheights.Utils.AppConstants;
import com.ezheights.Utils.AppPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AfterFilterJob extends AppCompatActivity {

    private RecyclerView recyclerView;

    private JobMainAdapter mAdapter;

    ArrayList<JobMainModel> arrayList = new ArrayList<>();
    LinearLayoutManager mLayoutManager1;

    AppPreferences preferences;
    String sltTyp, catTyp, lkfr, nat, loc, exp, sal, emailId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_after_filter_job);

        preferences = new AppPreferences();
        sltTyp = getIntent().getStringExtra("sltTyp");
        catTyp = getIntent().getStringExtra("catTyp");
        lkfr = getIntent().getStringExtra("lkfr");
        nat = getIntent().getStringExtra("nat");
        loc = getIntent().getStringExtra("loc");
        exp = getIntent().getStringExtra("exp");
        sal = getIntent().getStringExtra("sal");
        emailId = preferences.getPreferences(getApplicationContext(), AppConstants.emailId);

        init();
    }

    private void init() {

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_filtered);
        mLayoutManager1 = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager1);
        mLayoutManager1.setOrientation(LinearLayoutManager.VERTICAL);


        if (sltTyp == null) {
            sltTyp = "";
        }
        if (catTyp == null) {
            catTyp = "";
        }
        if (lkfr == null) {
            lkfr = "";
        }
        if (sal == null) {
            sal = "";
        }
        if (nat == null) {
            nat = "";
        }
        if (loc == null) {
            loc = "";
        }
        if (exp == null) {
            exp = "";
        }
        if (emailId == null) {
            emailId = "";
        }

        getFilterJob();
    }

    private void getFilterJob() {


        String UrlAfterFilterJob = AppConstants.ApplyJobFilter + "&select_type=" + sltTyp + "&select_cat=" + catTyp + "&txt_title=" + lkfr + "&select_salr=" + sal + "&select_nat=" + nat +
                "&select_city=" + loc + "&select_exp=" + exp + "&email=" + emailId;

        Log.e("After Filter Job", UrlAfterFilterJob);
        RequestGenerator.makeGetRequest(AfterFilterJob.this, UrlAfterFilterJob, true, new ResponseListener() {
            @Override
            public void onError(VolleyError error) {
                Toast.makeText(AfterFilterJob.this, "Some Problem Occured", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(String string) throws JSONException {
                if (!string.equals("")) {
                    Log.e("Response", string);
                    JSONObject jsonObject1 = new JSONObject(string);

                    JSONArray jsonArray = jsonObject1.getJSONArray("jobs");
                    arrayList.clear();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);

                        JobMainModel jobMainModel = new JobMainModel();
                        jobMainModel.setJobid(jsonObject2.getString("jobid"));
                        jobMainModel.setTitle(jsonObject2.getString("Title"));
                        jobMainModel.setPrice(jsonObject2.getString("Salary"));
                        jobMainModel.setNewexperiance(jsonObject2.getString("experiance"));
                        jobMainModel.setNeweducation(jsonObject2.getString("education"));
                        jobMainModel.setEmptype(jsonObject2.getString("emptype"));
                        jobMainModel.setFavourite(jsonObject2.getString("favourite"));

                        arrayList.add(jobMainModel);

                    }

                    mAdapter = new JobMainAdapter(AfterFilterJob.this, arrayList);
                    recyclerView.setAdapter(mAdapter);
                }
            }
        });


    }
}
