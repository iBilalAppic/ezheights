package com.ezheights.Activity.Motor;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ezheights.Adapter.MotorSaleMainAdapter;
import com.ezheights.Models.MotorSaleModel;
import com.ezheights.R;
import com.ezheights.Services.RequestGenerator;
import com.ezheights.Services.ResponseListener;
import com.ezheights.Utils.AppConstants;
import com.ezheights.Utils.AppPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MotorSaleActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView recyclerView_motor;
    private MotorSaleMainAdapter mAdapter;


    ArrayList<MotorSaleModel> arrayList = new ArrayList<>();
    Activity mContext;
    String PropId, emailId, isLogin;

    private ProgressBar progressBar1;
    FloatingActionButton filterButton;
    LinearLayoutManager mLayoutManager1;
    String searchKeyword;
    private AppPreferences appPreferance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_motor_sale);
        mContext = MotorSaleActivity.this;

        appPreferance = new AppPreferences();
        isLogin = appPreferance.getPreferences(getApplicationContext(), AppConstants.isLogin);
        emailId = appPreferance.getPreferences(getApplicationContext(), AppConstants.emailId);
        init();
    }

    private void init() {

        filterButton = (FloatingActionButton) findViewById(R.id.filter_motor_sale);
        recyclerView_motor = (RecyclerView) findViewById(R.id.recyclerView_motor);
        progressBar1 = (ProgressBar) findViewById(R.id.progress_bar);
        mLayoutManager1 = new LinearLayoutManager(this);
        recyclerView_motor.setLayoutManager(mLayoutManager1);
        mLayoutManager1.setOrientation(LinearLayoutManager.VERTICAL);

        searchKeyword = getIntent().getStringExtra("Search");
        if (searchKeyword == null) {
            searchKeyword = "";
        }
        if (emailId == null) {
            emailId = "";
        }
        filterButton.setOnClickListener(this);

        getMotorSaleData();

    }

    private void getMotorSaleData() {

        String UrlMotorMainData = AppConstants.MotorSaleData + "&search=" + searchKeyword + "&email=" + emailId;
        Log.e("Motor Request", UrlMotorMainData);


        RequestGenerator.makeGetRequest(mContext, UrlMotorMainData, true, new ResponseListener() {
            @Override
            public void onError(VolleyError error) {
                Toast.makeText(mContext, "Some Problem Occured", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(String string) throws JSONException {
                if (!string.equals("")) {

                    Log.e("Motor Sale Response", string);
                    JSONObject jsonObject1 = new JSONObject(string);

                    JSONArray jsonArray = jsonObject1.getJSONArray("default_content");
                    arrayList.clear();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);

                        MotorSaleModel motorSaleModel = new MotorSaleModel();
                        motorSaleModel.setMotor_id(jsonObject2.getString("newItemID"));
                        motorSaleModel.setMotor_title(jsonObject2.getString("newTitle"));
                        motorSaleModel.setMotor_verfied_status(jsonObject2.getString("newVerified"));
                        motorSaleModel.setMotor_price(jsonObject2.getString("price"));
                        motorSaleModel.setManufac_Year(jsonObject2.getString("MfgYear"));
                        motorSaleModel.setMotor_Color(jsonObject2.getString("Color"));
                        motorSaleModel.setNo_Of_Door(jsonObject2.getString("Doors"));
                        motorSaleModel.setMotor_image(jsonObject2.getString("image"));
                        motorSaleModel.setMotor_fav(jsonObject2.getString("favourite"));
                        arrayList.add(motorSaleModel);

                    }

                    mAdapter = new MotorSaleMainAdapter(MotorSaleActivity.this, arrayList);
                    recyclerView_motor.setAdapter(mAdapter);
                }
            }
        });

    }

    @Override
    public void onClick(View v) {
        if (v == filterButton) {
            Intent intent = new Intent(mContext, FilterMotorData.class);
            startActivity(intent);

        }
    }
}

