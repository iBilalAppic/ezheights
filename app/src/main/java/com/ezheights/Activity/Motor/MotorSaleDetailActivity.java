package com.ezheights.Activity.Motor;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ezheights.R;
import com.ezheights.Services.RequestGenerator;
import com.ezheights.Services.ResponseListener;
import com.ezheights.Utils.AppConstants;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class MotorSaleDetailActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tv_ReadMore, tv_Title, tv_Price_detail, postedDate, postedById, manYear, colorDetail, noOfDoorDetail, noOfCylenDetail, deatilType, descriptionShort;
    private ImageView img_Facebook, img_Twitter, img_LinkedIn, img_Google, img_Share, img_Like, img_Back, mainImage;
    CircleImageView img_Owner_detail;

    FloatingActionMenu materialDesignFAM;
    FloatingActionButton makeCall, makeSms;
    String motorId, lat, lang, shortDes, longDesc, title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_motor_sale_detail);

        motorId = getIntent().getStringExtra("motorId");
        Log.d("motorId", motorId);
        init();

    }

    private void init() {

        tv_ReadMore = (TextView) findViewById(R.id.tv_ReadMore);
        tv_Title = (TextView) findViewById(R.id.tv_Title_detail);
        tv_Price_detail = (TextView) findViewById(R.id.tv_Price_detail);
        postedDate = (TextView) findViewById(R.id.postedDate);
        postedById = (TextView) findViewById(R.id.postedById);
        manYear = (TextView) findViewById(R.id.manYear);
        colorDetail = (TextView) findViewById(R.id.colorDetail);
        noOfDoorDetail = (TextView) findViewById(R.id.noOfDoorDetail);
        noOfCylenDetail = (TextView) findViewById(R.id.noOfCylenDetail);
        deatilType = (TextView) findViewById(R.id.deatilType);
        descriptionShort = (TextView) findViewById(R.id.descriptionShort);

        mainImage = (ImageView) findViewById(R.id.img_Main_detail);
        img_Owner_detail = (CircleImageView) findViewById(R.id.img_Owner_detail);

        img_Like = (ImageView) findViewById(R.id.img_Like);
        img_Share = (ImageView) findViewById(R.id.img_Share);
        img_Back = (ImageView) findViewById(R.id.img_Back);
        makeCall = (FloatingActionButton) findViewById(R.id.make_call);
        makeSms = (FloatingActionButton) findViewById(R.id.make_sms);


        tv_ReadMore.setOnClickListener(this);
        img_Like.setOnClickListener(this);
        img_Share.setOnClickListener(this);
        img_Back.setOnClickListener(this);
        makeCall.setOnClickListener(this);
        makeSms.setOnClickListener(this);

        getAllMotorDetail();
    }

    private void getAllMotorDetail() {

//        try {
//            JSONObject jsonObject = new JSONObject();
//            jsonObject.put("ID", motorId);
//            Log.e("Motor Detail", jsonObject.toString());

        String UrlMotorDetailData = AppConstants.MotorDetailsData + "&ID=" + motorId;
        Log.e("Motor Request", UrlMotorDetailData);
        RequestGenerator.makeGetRequest(MotorSaleDetailActivity.this, UrlMotorDetailData, true, new ResponseListener() {
            @Override
            public void onError(VolleyError error) {
                Toast.makeText(MotorSaleDetailActivity.this, "Some Problem Occured", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(String string) throws JSONException {

                if (!string.equals("")) {
                    Log.e("Motor Detail Res", string);
                    JSONObject jsonObject1 = new JSONObject(string);
                    tv_Title.setText(jsonObject1.getString("newTitle"));
                    tv_Price_detail.setText("AED " + jsonObject1.getString("newPrice") + ".00");
                    postedDate.setText("Posted Date :" + jsonObject1.getString("postedDate"));
                    manYear.setText(jsonObject1.getString("newMfgYear"));
                    colorDetail.setText(jsonObject1.getString("newColor"));
                    noOfDoorDetail.setText(jsonObject1.getString("newDoors"));
                    noOfCylenDetail.setText(jsonObject1.getString("newTotalCylinder"));
                    deatilType.setText(jsonObject1.getString("newFuelType"));
                    descriptionShort.setText(jsonObject1.getString("newShortDesc"));

                    title = jsonObject1.getString("newTitle");
                    longDesc = jsonObject1.getString("newLongDesc");

                    Picasso.with(MotorSaleDetailActivity.this).load(jsonObject1.getString("PrimaryImage")).placeholder(R.drawable.watermark_property).into(img_Owner_detail);
                    Picasso.with(MotorSaleDetailActivity.this).load(jsonObject1.getString("PrimaryImage")).placeholder(R.drawable.watermark_property).into(mainImage);


                }
            }
        });


    }

    @Override
    public void onClick(View v) {

        if (v == img_Back) {
            onBackPressed();

        }

        if (v == tv_ReadMore) {
            readMoreDialog();

        }

        if (v == img_Like) {

        }

        if (v == img_Share) {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_TEXT, "EZHeights");
            startActivity(Intent.createChooser(shareIntent, "Share link using"));

        }
        if (v == makeCall) {

            Toast.makeText(this, "Call Under process", Toast.LENGTH_SHORT).show();
        }
        if (v == makeSms) {

            Toast.makeText(this, "SMS Under process", Toast.LENGTH_SHORT).show();
        }


    }

    private void readMoreDialog() {

        final Dialog dialog = new Dialog(MotorSaleDetailActivity.this, R.style.MyDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.read_more_dialog);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogSlideUpDownAnim;

        TextView toolbar_title = (TextView) dialog.findViewById(R.id.toolbar_title);
        TextView textView = (TextView) dialog.findViewById(R.id.tv_ReadMore);

        toolbar_title.setText(title);
        textView.setText(longDesc);
        dialog.show();

    }


}
