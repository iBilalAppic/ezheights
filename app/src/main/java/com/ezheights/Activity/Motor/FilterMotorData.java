package com.ezheights.Activity.Motor;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ezheights.R;
import com.ezheights.Services.RequestGenerator;
import com.ezheights.Services.ResponseListener;
import com.ezheights.Utils.AppConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FilterMotorData extends AppCompatActivity implements View.OnClickListener {

    Context mContext;
    private RelativeLayout vehicleTypeLL, cityLL, lookingLL, minManfLL, maxManfLL, minPriceLL, maxPriceLL, bodyTypeLL, conditionLL, cylinderLL, fuelTypeLL, postedByLL;
    RadioGroup radiogrp_vehicleType, radiogrp_city, radiogrp_minManf, radiogrp_maxManf, radiogrp_minPrice, radiogrp_maxPrice, radiogrp_bodyType, radiogrp_condition, radiogrp_cylinder, radiogrp_fuelType, radiogrp_postedBy;
    private EditText lookingFor;
    private LinearLayout vehicleType, city, looking, minManf, maxManf, minPrice, maxPrice, bodyType, condition, cylinder, fuelType, postedBy;
    private View view1, view2, view3, view4, view5, view6, view7, view8, view9, view10, view11, view12;
    private TextView txt1, txt2, txt3, txt4, txt5, txt6, txt7, txt8, txt9, txt10, txt11, txt12;
    private TextView clearAll, cancelBtn, appltFilter;


    String vehType, cty, lkfr, minMan, maxMan, minP, maxP, bdyTy, condi, cyl, flTy, postBy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_motor_data);

        mContext = FilterMotorData.this;

        init();

    }

    private void init() {
        vehicleTypeLL = (RelativeLayout) findViewById(R.id.vehicleTypeMainLayout);
        cityLL = (RelativeLayout) findViewById(R.id.motorCityMainLayout);
        lookingLL = (RelativeLayout) findViewById(R.id.motorlookinForMainLayout);
        minManfLL = (RelativeLayout) findViewById(R.id.minManfYearMain);
        maxManfLL = (RelativeLayout) findViewById(R.id.maxManfYearMain);
        minPriceLL = (RelativeLayout) findViewById(R.id.minPriceMotorMain);
        maxPriceLL = (RelativeLayout) findViewById(R.id.maxPriceMotorMain);
        bodyTypeLL = (RelativeLayout) findViewById(R.id.bodyTypeMain);
        conditionLL = (RelativeLayout) findViewById(R.id.motorConditionMain);
        cylinderLL = (RelativeLayout) findViewById(R.id.noOfCylinderMain);
        fuelTypeLL = (RelativeLayout) findViewById(R.id.fuelTypeMain);
        postedByLL = (RelativeLayout) findViewById(R.id.postedByMainLayout);

        vehicleType = (LinearLayout) findViewById(R.id.vehicleType);
        city = (LinearLayout) findViewById(R.id.motorCity);
        looking = (LinearLayout) findViewById(R.id.motorLooking);
        minManf = (LinearLayout) findViewById(R.id.manufMinYear);
        maxManf = (LinearLayout) findViewById(R.id.manufMaxYear);
        minPrice = (LinearLayout) findViewById(R.id.motorMinPrice);
        maxPrice = (LinearLayout) findViewById(R.id.motorMaxPrice);
        bodyType = (LinearLayout) findViewById(R.id.bodyType);
        condition = (LinearLayout) findViewById(R.id.motorCondition);
        cylinder = (LinearLayout) findViewById(R.id.noOfCylinder);
        fuelType = (LinearLayout) findViewById(R.id.fuleType);
        postedBy = (LinearLayout) findViewById(R.id.propertyPostedBy);

        lookingFor = (EditText) findViewById(R.id.lookinForText);

        view1 = (View) findViewById(R.id.viewmotorfilter1);
        view2 = (View) findViewById(R.id.viewmotorfilter2);
        view3 = (View) findViewById(R.id.viewmotorfilter3);
        view4 = (View) findViewById(R.id.viewmotorfilter4);
        view5 = (View) findViewById(R.id.viewmotorfilter5);
        view6 = (View) findViewById(R.id.viewmotorfilter6);
        view7 = (View) findViewById(R.id.viewmotorfilter7);
        view8 = (View) findViewById(R.id.viewmotorfilter8);
        view9 = (View) findViewById(R.id.viewmotorfilter9);
        view10 = (View) findViewById(R.id.viewmotorfilter10);
        view11 = (View) findViewById(R.id.viewmotorfilter11);
        view12 = (View) findViewById(R.id.viewmotorfilter12);

        txt1 = (TextView) findViewById(R.id.txt1);
        txt2 = (TextView) findViewById(R.id.txt2);
        txt3 = (TextView) findViewById(R.id.txt3);
        txt4 = (TextView) findViewById(R.id.txt4);
        txt5 = (TextView) findViewById(R.id.txt5);
        txt6 = (TextView) findViewById(R.id.txt6);
        txt7 = (TextView) findViewById(R.id.txt7);
        txt8 = (TextView) findViewById(R.id.txt8);
        txt9 = (TextView) findViewById(R.id.txt9);
        txt10 = (TextView) findViewById(R.id.txt10);
        txt11 = (TextView) findViewById(R.id.txt11);
        txt12 = (TextView) findViewById(R.id.txt12);

        radiogrp_vehicleType = (RadioGroup) findViewById(R.id.radiogrp_vehicleType);
        radiogrp_city = (RadioGroup) findViewById(R.id.radiogrp_city);
        radiogrp_minManf = (RadioGroup) findViewById(R.id.radiogrp_minManfYear);
        radiogrp_maxManf = (RadioGroup) findViewById(R.id.radiogrp_maxManfYear);
        radiogrp_minPrice = (RadioGroup) findViewById(R.id.radiogrp_minPrice);
        radiogrp_maxPrice = (RadioGroup) findViewById(R.id.radiogrp_maxPrice);
        radiogrp_bodyType = (RadioGroup) findViewById(R.id.radiogrp_bodyType);
        radiogrp_condition = (RadioGroup) findViewById(R.id.radiogrp_motorCondition);
        radiogrp_cylinder = (RadioGroup) findViewById(R.id.radiogrp_cylinder);
        radiogrp_fuelType = (RadioGroup) findViewById(R.id.radiogrp_fuelType);
        radiogrp_postedBy = (RadioGroup) findViewById(R.id.radiogrp_postedBy);


        clearAll = (TextView) findViewById(R.id.clearAll);
        cancelBtn = (TextView) findViewById(R.id.cancel);
        appltFilter = (TextView) findViewById(R.id.applyfilter);


        vehicleType.setOnClickListener(this);
        city.setOnClickListener(this);
        looking.setOnClickListener(this);
        minManf.setOnClickListener(this);
        maxManf.setOnClickListener(this);
        minPrice.setOnClickListener(this);
        maxPrice.setOnClickListener(this);
        bodyType.setOnClickListener(this);
        condition.setOnClickListener(this);
        cylinder.setOnClickListener(this);
        fuelType.setOnClickListener(this);
        postedBy.setOnClickListener(this);

        clearAll.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);
        appltFilter.setOnClickListener(this);

        if (vehType == null) {
            vehType = "";
        }
        if (cty == null) {
            cty = "";
        }
        if (lkfr == null) {
            lkfr = "";
        }
        if (minMan == null) {
            minMan = "";
        }
        if (maxMan == null) {
            maxMan = "";
        }
        if (minP == null) {
            minP = "";
        }
        if (maxP == null) {
            maxP = "";
        }

        if (bdyTy == null) {
            bdyTy = "";
        }
        if (condi == null) {
            condi = "";
        }
        if (cyl == null) {
            cyl = "";
        }
        if (flTy == null) {
            flTy = "";
        }
        if (postBy == null) {
            postBy = "";
        }


        lkfr = lookingFor.getText().toString();


        getALLMotorFilter();

    }

    private void getALLMotorFilter() {

        RequestGenerator.makeGetRequest(FilterMotorData.this, AppConstants.PreMotorFilter, true, new ResponseListener() {
            @Override
            public void onError(VolleyError error) {
                Toast.makeText(FilterMotorData.this, "Problem Occured", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(String string) throws JSONException {

                if (!string.equals("")) {
                    JSONObject jsonObject = new JSONObject(string);
                /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
                    JSONArray jsonArrayVehType = jsonObject.getJSONArray("categoty");

                    for (int i = 0; i < jsonArrayVehType.length(); i++) {
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        final RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonArrayVehType.getString(i));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_vehicleType.addView(rdbtn_for_cat, rprms);

                        radiogrp_vehicleType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        vehType = (String) btn.getText();

                                        return;
                                    }
                                }
                            }
                        });
                    }

/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

                    JSONArray jsonArrayCity = jsonObject.getJSONArray("city_detail");
                    radiogrp_city.removeAllViews();
                    for (int i = 0; i < jsonArrayCity.length(); i++) {
                        JSONObject jsonObject2 = jsonArrayCity.getJSONObject(i);
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonObject2.getString("cityName"));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_city.addView(rdbtn_for_cat, rprms);
                        radiogrp_city.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        cty = (String) btn.getText();
                                        return;
                                    }
                                }
                            }
                        });

                    }
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
                    JSONArray jsonArrayminmanfYear = jsonObject.getJSONArray("year");
                    radiogrp_minManf.removeAllViews();
                    for (int i = 0; i < jsonArrayminmanfYear.length(); i++) {
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonArrayminmanfYear.getString(i));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_minManf.addView(rdbtn_for_cat, rprms);

                        radiogrp_minManf.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        minMan = (String) btn.getText();
                                        return;
                                    }
                                }
                            }
                        });
                    }

/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
                    JSONArray jsonArraymaxmanfYear = jsonObject.getJSONArray("year");
                    radiogrp_maxManf.removeAllViews();
                    for (int i = 0; i < jsonArraymaxmanfYear.length(); i++) {
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonArraymaxmanfYear.getString(i));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_maxManf.addView(rdbtn_for_cat, rprms);

                        radiogrp_maxManf.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        maxMan = (String) btn.getText();
                                        return;
                                    }
                                }
                            }
                        });
                    }

/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
                    JSONArray jsonArrayminPrice = jsonObject.getJSONArray("price");
                    radiogrp_minPrice.removeAllViews();
                    for (int i = 0; i < jsonArrayminPrice.length(); i++) {
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonArrayminPrice.getString(i));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_minPrice.addView(rdbtn_for_cat, rprms);

                        radiogrp_minPrice.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        minP = (String) btn.getText();
                                        return;
                                    }
                                }
                            }
                        });
                    }

/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
                    JSONArray jsonArraymaxPrice = jsonObject.getJSONArray("price");
                    radiogrp_maxPrice.removeAllViews();
                    for (int i = 0; i < jsonArraymaxPrice.length(); i++) {
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonArraymaxPrice.getString(i));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_maxPrice.addView(rdbtn_for_cat, rprms);

                        radiogrp_maxPrice.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        maxP = (String) btn.getText();
                                        return;
                                    }
                                }
                            }
                        });
                    }

/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
                    JSONArray jsonArrayBodyType = jsonObject.getJSONArray("bodyType");
                    radiogrp_bodyType.removeAllViews();
                    for (int i = 0; i < jsonArrayBodyType.length(); i++) {
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonArrayBodyType.getString(i));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_bodyType.addView(rdbtn_for_cat, rprms);

                        radiogrp_bodyType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        bdyTy = (String) btn.getText();
                                        return;
                                    }
                                }
                            }
                        });
                    }

/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
                    JSONArray jsonArrayMotorCon = jsonObject.getJSONArray("condition");
                    radiogrp_condition.removeAllViews();
                    for (int i = 0; i < jsonArrayMotorCon.length(); i++) {
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonArrayMotorCon.getString(i));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_condition.addView(rdbtn_for_cat, rprms);

                        radiogrp_condition.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        condi = (String) btn.getText();
                                        return;
                                    }
                                }
                            }
                        });
                    }

/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
                    JSONArray jsonArrayMotorCyl = jsonObject.getJSONArray("Cylinder");
                    radiogrp_cylinder.removeAllViews();
                    for (int i = 0; i < jsonArrayMotorCyl.length(); i++) {
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonArrayMotorCyl.getString(i));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_cylinder.addView(rdbtn_for_cat, rprms);

                        radiogrp_cylinder.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        cyl = (String) btn.getText();
                                        return;
                                    }
                                }
                            }
                        });
                    }

/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
                    JSONArray jsonArrayMotorFuel = jsonObject.getJSONArray("fuel");
                    radiogrp_fuelType.removeAllViews();
                    for (int i = 0; i < jsonArrayMotorFuel.length(); i++) {
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonArrayMotorFuel.getString(i));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_fuelType.addView(rdbtn_for_cat, rprms);

                        radiogrp_fuelType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        flTy = (String) btn.getText();
                                        return;
                                    }
                                }
                            }
                        });
                    }

/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

                    JSONArray jsonArrayUser = jsonObject.getJSONArray("user_type");
                    radiogrp_postedBy.removeAllViews();
                    for (int i = 0; i < jsonArrayUser.length(); i++) {
                        RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                        RadioButton rdbtn_for_cat = new RadioButton(getApplicationContext());
                        rdbtn_for_cat.setText(jsonArrayUser.getString(i));
                        Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
                        drawable.setBounds(0, 0, 60, 80);
                        rdbtn_for_cat.setCompoundDrawables(null, null, drawable, null);
                        rdbtn_for_cat.setGravity(Gravity.CENTER_VERTICAL);
                        rdbtn_for_cat.setButtonDrawable(null);
                        rdbtn_for_cat.setButtonDrawable(android.R.color.transparent);
                        rdbtn_for_cat.setTextColor(Color.parseColor("#000000"));
                        radiogrp_postedBy.addView(rdbtn_for_cat, rprms);

                        radiogrp_postedBy.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                                for (int i = 0; i < rg.getChildCount(); i++) {
                                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                                    if (btn.getId() == checkedId) {
                                        postBy = (String) btn.getText();
                                        return;
                                    }
                                }
                            }
                        });
                    }


                }

            }
        });
    }

    @Override
    public void onClick(View v) {


        if (v == clearAll) {

        }
        if (v == cancelBtn) {
            onBackPressed();

        }
        if (v == appltFilter) {
            Log.d("", postBy);
            applyMotorFilter();
        }

        if (v == vehicleType) {

            vehicleTypeLL.setVisibility(View.VISIBLE);
            cityLL.setVisibility(View.GONE);
            lookingLL.setVisibility(View.GONE);
            minManfLL.setVisibility(View.GONE);
            maxManfLL.setVisibility(View.GONE);
            minPriceLL.setVisibility(View.GONE);
            maxPriceLL.setVisibility(View.GONE);
            bodyTypeLL.setVisibility(View.GONE);
            conditionLL.setVisibility(View.GONE);
            cylinderLL.setVisibility(View.GONE);
            fuelTypeLL.setVisibility(View.GONE);
            postedByLL.setVisibility(View.GONE);

            view1.setVisibility(View.VISIBLE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);
            view8.setVisibility(View.GONE);
            view9.setVisibility(View.GONE);
            view10.setVisibility(View.GONE);
            view11.setVisibility(View.GONE);
            view12.setVisibility(View.GONE);

            txt1.setTextColor(Color.parseColor("#000000"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
            txt8.setTextColor(Color.parseColor("#bdbdbd"));
            txt9.setTextColor(Color.parseColor("#bdbdbd"));
            txt10.setTextColor(Color.parseColor("#bdbdbd"));
            txt11.setTextColor(Color.parseColor("#bdbdbd"));
            txt12.setTextColor(Color.parseColor("#bdbdbd"));


        }
        if (v == city) {

            vehicleTypeLL.setVisibility(View.GONE);
            cityLL.setVisibility(View.VISIBLE);
            lookingLL.setVisibility(View.GONE);
            minManfLL.setVisibility(View.GONE);
            maxManfLL.setVisibility(View.GONE);
            minPriceLL.setVisibility(View.GONE);
            maxPriceLL.setVisibility(View.GONE);
            bodyTypeLL.setVisibility(View.GONE);
            conditionLL.setVisibility(View.GONE);
            cylinderLL.setVisibility(View.GONE);
            fuelTypeLL.setVisibility(View.GONE);
            postedByLL.setVisibility(View.GONE);


            view1.setVisibility(View.GONE);
            view2.setVisibility(View.VISIBLE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);
            view8.setVisibility(View.GONE);
            view9.setVisibility(View.GONE);
            view10.setVisibility(View.GONE);
            view11.setVisibility(View.GONE);
            view12.setVisibility(View.GONE);


            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#000000"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
            txt8.setTextColor(Color.parseColor("#bdbdbd"));
            txt9.setTextColor(Color.parseColor("#bdbdbd"));
            txt10.setTextColor(Color.parseColor("#bdbdbd"));
            txt11.setTextColor(Color.parseColor("#bdbdbd"));
            txt12.setTextColor(Color.parseColor("#bdbdbd"));


        }
        if (v == looking) {

            vehicleTypeLL.setVisibility(View.GONE);
            cityLL.setVisibility(View.GONE);
            lookingLL.setVisibility(View.VISIBLE);
            minManfLL.setVisibility(View.GONE);
            maxManfLL.setVisibility(View.GONE);
            minPriceLL.setVisibility(View.GONE);
            maxPriceLL.setVisibility(View.GONE);
            bodyTypeLL.setVisibility(View.GONE);
            conditionLL.setVisibility(View.GONE);
            cylinderLL.setVisibility(View.GONE);
            fuelTypeLL.setVisibility(View.GONE);
            postedByLL.setVisibility(View.GONE);

            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.VISIBLE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);
            view8.setVisibility(View.GONE);
            view9.setVisibility(View.GONE);
            view10.setVisibility(View.GONE);
            view11.setVisibility(View.GONE);
            view12.setVisibility(View.GONE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#000000"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
            txt8.setTextColor(Color.parseColor("#bdbdbd"));
            txt9.setTextColor(Color.parseColor("#bdbdbd"));
            txt10.setTextColor(Color.parseColor("#bdbdbd"));
            txt11.setTextColor(Color.parseColor("#bdbdbd"));
            txt12.setTextColor(Color.parseColor("#bdbdbd"));

        }
        if (v == minManf) {

            vehicleTypeLL.setVisibility(View.GONE);
            cityLL.setVisibility(View.GONE);
            lookingLL.setVisibility(View.GONE);
            minManfLL.setVisibility(View.VISIBLE);
            maxManfLL.setVisibility(View.GONE);
            minPriceLL.setVisibility(View.GONE);
            maxPriceLL.setVisibility(View.GONE);
            bodyTypeLL.setVisibility(View.GONE);
            conditionLL.setVisibility(View.GONE);
            cylinderLL.setVisibility(View.GONE);
            fuelTypeLL.setVisibility(View.GONE);
            postedByLL.setVisibility(View.GONE);

            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.VISIBLE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);
            view8.setVisibility(View.GONE);
            view9.setVisibility(View.GONE);
            view10.setVisibility(View.GONE);
            view11.setVisibility(View.GONE);
            view12.setVisibility(View.GONE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#000000"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
            txt8.setTextColor(Color.parseColor("#bdbdbd"));
            txt9.setTextColor(Color.parseColor("#bdbdbd"));
            txt10.setTextColor(Color.parseColor("#bdbdbd"));
            txt11.setTextColor(Color.parseColor("#bdbdbd"));
            txt12.setTextColor(Color.parseColor("#bdbdbd"));
        }
        if (v == maxManf) {

            vehicleTypeLL.setVisibility(View.GONE);
            cityLL.setVisibility(View.GONE);
            lookingLL.setVisibility(View.GONE);
            minManfLL.setVisibility(View.GONE);
            maxManfLL.setVisibility(View.VISIBLE);
            minPriceLL.setVisibility(View.GONE);
            maxPriceLL.setVisibility(View.GONE);
            bodyTypeLL.setVisibility(View.GONE);
            conditionLL.setVisibility(View.GONE);
            cylinderLL.setVisibility(View.GONE);
            fuelTypeLL.setVisibility(View.GONE);
            postedByLL.setVisibility(View.GONE);

            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.VISIBLE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);
            view8.setVisibility(View.GONE);
            view9.setVisibility(View.GONE);
            view10.setVisibility(View.GONE);
            view11.setVisibility(View.GONE);
            view12.setVisibility(View.GONE);


            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#000000"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
            txt8.setTextColor(Color.parseColor("#bdbdbd"));
            txt9.setTextColor(Color.parseColor("#bdbdbd"));
            txt10.setTextColor(Color.parseColor("#bdbdbd"));
            txt11.setTextColor(Color.parseColor("#bdbdbd"));
            txt12.setTextColor(Color.parseColor("#bdbdbd"));

        }
        if (v == minPrice) {

            vehicleTypeLL.setVisibility(View.GONE);
            cityLL.setVisibility(View.GONE);
            lookingLL.setVisibility(View.GONE);
            minManfLL.setVisibility(View.GONE);
            maxManfLL.setVisibility(View.GONE);
            minPriceLL.setVisibility(View.VISIBLE);
            maxPriceLL.setVisibility(View.GONE);
            bodyTypeLL.setVisibility(View.GONE);
            conditionLL.setVisibility(View.GONE);
            cylinderLL.setVisibility(View.GONE);
            fuelTypeLL.setVisibility(View.GONE);
            postedByLL.setVisibility(View.GONE);

            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.VISIBLE);
            view7.setVisibility(View.GONE);
            view8.setVisibility(View.GONE);
            view9.setVisibility(View.GONE);
            view10.setVisibility(View.GONE);
            view11.setVisibility(View.GONE);
            view12.setVisibility(View.GONE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#000000"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
            txt8.setTextColor(Color.parseColor("#bdbdbd"));
            txt9.setTextColor(Color.parseColor("#bdbdbd"));
            txt10.setTextColor(Color.parseColor("#bdbdbd"));
            txt11.setTextColor(Color.parseColor("#bdbdbd"));
            txt12.setTextColor(Color.parseColor("#bdbdbd"));

        }
        if (v == maxPrice) {
            vehicleTypeLL.setVisibility(View.GONE);
            cityLL.setVisibility(View.GONE);
            lookingLL.setVisibility(View.GONE);
            minManfLL.setVisibility(View.GONE);
            maxManfLL.setVisibility(View.GONE);
            minPriceLL.setVisibility(View.GONE);
            maxPriceLL.setVisibility(View.VISIBLE);
            bodyTypeLL.setVisibility(View.GONE);
            conditionLL.setVisibility(View.GONE);
            cylinderLL.setVisibility(View.GONE);
            fuelTypeLL.setVisibility(View.GONE);
            postedByLL.setVisibility(View.GONE);


            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.VISIBLE);
            view8.setVisibility(View.GONE);
            view9.setVisibility(View.GONE);
            view10.setVisibility(View.GONE);
            view11.setVisibility(View.GONE);
            view12.setVisibility(View.GONE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#000000"));
            txt8.setTextColor(Color.parseColor("#bdbdbd"));
            txt9.setTextColor(Color.parseColor("#bdbdbd"));
            txt10.setTextColor(Color.parseColor("#bdbdbd"));
            txt11.setTextColor(Color.parseColor("#bdbdbd"));
            txt12.setTextColor(Color.parseColor("#bdbdbd"));

        }
        if (v == bodyType) {

            vehicleTypeLL.setVisibility(View.GONE);
            cityLL.setVisibility(View.GONE);
            lookingLL.setVisibility(View.GONE);
            minManfLL.setVisibility(View.GONE);
            maxManfLL.setVisibility(View.GONE);
            minPriceLL.setVisibility(View.GONE);
            maxPriceLL.setVisibility(View.GONE);
            bodyTypeLL.setVisibility(View.VISIBLE);
            conditionLL.setVisibility(View.GONE);
            cylinderLL.setVisibility(View.GONE);
            fuelTypeLL.setVisibility(View.GONE);
            postedByLL.setVisibility(View.GONE);


            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);
            view8.setVisibility(View.VISIBLE);
            view9.setVisibility(View.GONE);
            view10.setVisibility(View.GONE);
            view11.setVisibility(View.GONE);
            view12.setVisibility(View.GONE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
            txt8.setTextColor(Color.parseColor("#000000"));
            txt9.setTextColor(Color.parseColor("#bdbdbd"));
            txt10.setTextColor(Color.parseColor("#bdbdbd"));
            txt11.setTextColor(Color.parseColor("#bdbdbd"));
            txt12.setTextColor(Color.parseColor("#bdbdbd"));

        }
        if (v == condition) {

            vehicleTypeLL.setVisibility(View.GONE);
            cityLL.setVisibility(View.GONE);
            lookingLL.setVisibility(View.GONE);
            minManfLL.setVisibility(View.GONE);
            maxManfLL.setVisibility(View.GONE);
            minPriceLL.setVisibility(View.GONE);
            maxPriceLL.setVisibility(View.GONE);
            bodyTypeLL.setVisibility(View.GONE);
            conditionLL.setVisibility(View.VISIBLE);
            cylinderLL.setVisibility(View.GONE);
            fuelTypeLL.setVisibility(View.GONE);
            postedByLL.setVisibility(View.GONE);

            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);
            view8.setVisibility(View.GONE);
            view9.setVisibility(View.VISIBLE);
            view10.setVisibility(View.GONE);
            view11.setVisibility(View.GONE);
            view12.setVisibility(View.GONE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
            txt8.setTextColor(Color.parseColor("#bdbdbd"));
            txt9.setTextColor(Color.parseColor("#000000"));
            txt10.setTextColor(Color.parseColor("#bdbdbd"));
            txt11.setTextColor(Color.parseColor("#bdbdbd"));
            txt12.setTextColor(Color.parseColor("#bdbdbd"));
        }
        if (v == cylinder) {

            vehicleTypeLL.setVisibility(View.GONE);
            cityLL.setVisibility(View.GONE);
            lookingLL.setVisibility(View.GONE);
            minManfLL.setVisibility(View.GONE);
            maxManfLL.setVisibility(View.GONE);
            minPriceLL.setVisibility(View.GONE);
            maxPriceLL.setVisibility(View.GONE);
            bodyTypeLL.setVisibility(View.GONE);
            conditionLL.setVisibility(View.GONE);
            cylinderLL.setVisibility(View.VISIBLE);
            fuelTypeLL.setVisibility(View.GONE);
            postedByLL.setVisibility(View.GONE);

            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);
            view8.setVisibility(View.GONE);
            view9.setVisibility(View.GONE);
            view10.setVisibility(View.VISIBLE);
            view11.setVisibility(View.GONE);
            view12.setVisibility(View.GONE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
            txt8.setTextColor(Color.parseColor("#bdbdbd"));
            txt9.setTextColor(Color.parseColor("#bdbdbd"));
            txt10.setTextColor(Color.parseColor("#000000"));
            txt11.setTextColor(Color.parseColor("#bdbdbd"));
            txt12.setTextColor(Color.parseColor("#bdbdbd"));

        }
        if (v == fuelType) {
            vehicleTypeLL.setVisibility(View.GONE);
            cityLL.setVisibility(View.GONE);
            lookingLL.setVisibility(View.GONE);
            minManfLL.setVisibility(View.GONE);
            maxManfLL.setVisibility(View.GONE);
            minPriceLL.setVisibility(View.GONE);
            maxPriceLL.setVisibility(View.GONE);
            bodyTypeLL.setVisibility(View.GONE);
            conditionLL.setVisibility(View.GONE);
            cylinderLL.setVisibility(View.GONE);
            fuelTypeLL.setVisibility(View.VISIBLE);
            postedByLL.setVisibility(View.GONE);


            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);
            view8.setVisibility(View.GONE);
            view9.setVisibility(View.GONE);
            view10.setVisibility(View.GONE);
            view11.setVisibility(View.VISIBLE);
            view12.setVisibility(View.GONE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
            txt8.setTextColor(Color.parseColor("#bdbdbd"));
            txt9.setTextColor(Color.parseColor("#bdbdbd"));
            txt10.setTextColor(Color.parseColor("#bdbdbd"));
            txt11.setTextColor(Color.parseColor("#000000"));
            txt12.setTextColor(Color.parseColor("#bdbdbd"));

        }
        if (v == postedBy) {
            vehicleTypeLL.setVisibility(View.GONE);
            cityLL.setVisibility(View.GONE);
            lookingLL.setVisibility(View.GONE);
            minManfLL.setVisibility(View.GONE);
            maxManfLL.setVisibility(View.GONE);
            minPriceLL.setVisibility(View.GONE);
            maxPriceLL.setVisibility(View.GONE);
            bodyTypeLL.setVisibility(View.GONE);
            conditionLL.setVisibility(View.GONE);
            cylinderLL.setVisibility(View.GONE);
            fuelTypeLL.setVisibility(View.GONE);
            postedByLL.setVisibility(View.VISIBLE);

            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);
            view8.setVisibility(View.GONE);
            view9.setVisibility(View.GONE);
            view10.setVisibility(View.GONE);
            view11.setVisibility(View.GONE);
            view12.setVisibility(View.VISIBLE);

            txt1.setTextColor(Color.parseColor("#bdbdbd"));
            txt2.setTextColor(Color.parseColor("#bdbdbd"));
            txt3.setTextColor(Color.parseColor("#bdbdbd"));
            txt4.setTextColor(Color.parseColor("#bdbdbd"));
            txt5.setTextColor(Color.parseColor("#bdbdbd"));
            txt6.setTextColor(Color.parseColor("#bdbdbd"));
            txt7.setTextColor(Color.parseColor("#bdbdbd"));
            txt8.setTextColor(Color.parseColor("#bdbdbd"));
            txt9.setTextColor(Color.parseColor("#bdbdbd"));
            txt10.setTextColor(Color.parseColor("#bdbdbd"));
            txt11.setTextColor(Color.parseColor("#bdbdbd"));
            txt12.setTextColor(Color.parseColor("#000000"));
        }

    }

    private void applyMotorFilter() {

        Intent intent = new Intent(FilterMotorData.this, AfterFilteredMotor.class);
        intent.putExtra("txt_cat", vehType);
        intent.putExtra("txt_city", cty);
        intent.putExtra("txt_model", lkfr);
        intent.putExtra("txt_yearMin", minMan);
        intent.putExtra("txt_yearMax", maxMan);
        intent.putExtra("txt_priceMin", minP);
        intent.putExtra("txt_priceMax", maxP);
        intent.putExtra("txt_bodytype", bdyTy);
        intent.putExtra("txt_condition", condi);
        intent.putExtra("txt_cylinder", cyl);
        intent.putExtra("User_type", postBy);
        intent.putExtra("txt_fuel", flTy);
        startActivity(intent);
        finish();
    }
}
