package com.ezheights.Activity.Motor;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ezheights.Adapter.MotorSaleMainAdapter;
import com.ezheights.Models.MotorSaleModel;
import com.ezheights.R;
import com.ezheights.Services.RequestGenerator;
import com.ezheights.Services.ResponseListener;
import com.ezheights.Utils.AppConstants;
import com.ezheights.Utils.AppPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AfterFilteredMotor extends AppCompatActivity {


    private RecyclerView recyclerView;
    private MotorSaleMainAdapter mAdapter;
    ArrayList<MotorSaleModel> arrayList = new ArrayList<>();
    LinearLayoutManager mLayoutManager1;
    AppPreferences preferences;
    String vehType, cty, lkfr, minMan, maxMan, minP, maxP, bdyTy, condi, cyl, flTy, postBy, emailId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filtered_motor_data);
        preferences = new AppPreferences();

        vehType = getIntent().getStringExtra("txt_cat");
        cty = getIntent().getStringExtra("txt_city");
        lkfr = getIntent().getStringExtra("txt_model");
        minMan = getIntent().getStringExtra("txt_yearMin");
        maxMan = getIntent().getStringExtra("txt_yearMax");
        minP = getIntent().getStringExtra("txt_priceMin");
        maxP = getIntent().getStringExtra("txt_priceMax");
        bdyTy = getIntent().getStringExtra("txt_bodytype");
        condi = getIntent().getStringExtra("txt_condition");
        cyl = getIntent().getStringExtra("txt_cylinder");
        postBy = getIntent().getStringExtra("User_type");
        flTy = getIntent().getStringExtra("txt_fuel");
        emailId = preferences.getPreferences(getApplicationContext(), AppConstants.emailId);
        Log.d("All", vehType + cty + minMan);

        init();
    }

    public void init() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_filtered);
        mLayoutManager1 = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager1);
        mLayoutManager1.setOrientation(LinearLayoutManager.VERTICAL);

        if (vehType == null) {
            vehType = "";
        }
        if (cty == null) {
            cty = "";
        }
        if (lkfr == null) {
            lkfr = "";
        }
        if (minMan == null) {
            minMan = "";
        }
        if (maxMan == null) {
            maxMan = "";
        }
        if (minP == null) {
            minP = "";
        }
        if (maxP == null) {
            maxP = "";
        }

        if (bdyTy == null) {
            bdyTy = "";
        }
        if (condi == null) {
            condi = "";
        }
        if (cyl == null) {
            cyl = "";
        }
        if (flTy == null) {
            flTy = "";
        }
        if (postBy == null) {
            postBy = "";
        }
        if (emailId == null) {
            emailId = "";
        }
        getFilterMotor();

    }

    private void getFilterMotor() {

        String UrlAfterFilterMotor = AppConstants.ApplyMotorFilter + "&txt_cat=" + vehType + "&txt_city=" + cty + "&txt_model=" + lkfr + "&txt_yearMin=" + minMan + "&txt_yearMax=" + maxMan +
                "&txt_priceMin=" + minP + "&txt_priceMax=" + maxP + "&txt_bodytype=" + bdyTy + "&txt_condition=" + condi + "&txt_cylinder=" + cyl + "&User_type=" + postBy + "&txt_fuel=" + flTy + "&email=" + emailId;

        Log.e("Motor Fil Req", UrlAfterFilterMotor);

        RequestGenerator.makeGetRequest(AfterFilteredMotor.this, UrlAfterFilterMotor, true, new ResponseListener() {
            @Override
            public void onError(VolleyError error) {
                Toast.makeText(AfterFilteredMotor.this, "Some Problem Occured", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(String string) throws JSONException {
                if (!string.equals("")) {

                    JSONObject jsonObject1 = new JSONObject(string);

                    JSONArray jsonArray = jsonObject1.getJSONArray("motors");
                    arrayList.clear();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);

                        MotorSaleModel motorSaleModel = new MotorSaleModel();
                        motorSaleModel.setMotor_id(jsonObject2.getString("ItemID"));
                        motorSaleModel.setMotor_title(jsonObject2.getString("Title"));
                        motorSaleModel.setMotor_verfied_status(jsonObject2.getString("Verified"));
                        motorSaleModel.setMotor_price(jsonObject2.getString("Price"));
                        motorSaleModel.setManufac_Year(jsonObject2.getString("MfgYear"));
                        motorSaleModel.setMotor_Color(jsonObject2.getString("Color"));
                        motorSaleModel.setNo_Of_Door(jsonObject2.getString("Doors"));
                        motorSaleModel.setMotor_image(jsonObject2.getString("Images"));
                        motorSaleModel.setMotor_fav(jsonObject2.getString("favourite"));
                        arrayList.add(motorSaleModel);

                    }

                    mAdapter = new MotorSaleMainAdapter(AfterFilteredMotor.this, arrayList);
                    recyclerView.setAdapter(mAdapter);
                }
            }
        });
    }
}
