package com.ezheights.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ezheights.Activity.Classified.AfterClassifiedFiltered;
import com.ezheights.Activity.Classified.ClassifiedActivity;
import com.ezheights.Activity.Classified.ClassifiedDetailsActivity;
import com.ezheights.Models.ClassifiedModel;
import com.ezheights.R;

import java.util.ArrayList;

/**
 * Created by Bilal Khan on 30/3/17.
 */

public class ClassifiedMainAdapter extends RecyclerView.Adapter<ClassifiedMainAdapter.ViewHolder> {

    ArrayList<ClassifiedModel> arrayList = new ArrayList<>();
    private Context mContext;

    String ClassiFiedId;

    public ClassifiedMainAdapter(ClassifiedActivity classifiedActivity, ArrayList<ClassifiedModel> arrayList) {

        this.mContext = classifiedActivity;
        this.arrayList = arrayList;
    }

    public ClassifiedMainAdapter(AfterClassifiedFiltered afterClassifiedFiltered, ArrayList<ClassifiedModel> arrayList) {
        this.mContext = afterClassifiedFiltered;
        this.arrayList = arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_classifeid_list, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.title.setText(arrayList.get(position).getTitle());
        holder.price.setText(arrayList.get(position).getPrice());
        holder.age.setText(arrayList.get(position).getAge());
        holder.usage.setText(arrayList.get(position).getItemUseage());
        holder.condi.setText(arrayList.get(position).getCondition());

        Glide.with(mContext).load(arrayList.get(position).getImage())
                .thumbnail(0.5f)
                .crossFade()
                .placeholder(R.drawable.watermark_property)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.mainImage);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private RelativeLayout rL_Main;
        ImageView mainImage;
        TextView title, price, age, usage, condi;


        public ViewHolder(View itemView) {
            super(itemView);

            rL_Main = (RelativeLayout) itemView.findViewById(R.id.rL_Main);
            mainImage = (ImageView) itemView.findViewById(R.id.mainImageOfClassified);
            title = (TextView) itemView.findViewById(R.id.tv_title_Classi);
            price = (TextView) itemView.findViewById(R.id.tv_Price_Classi);
            age = (TextView) itemView.findViewById(R.id.tv_YearCount_Classi);
            usage = (TextView) itemView.findViewById(R.id.tv_Usage_Classi);
            condi = (TextView) itemView.findViewById(R.id.tv_Condition_Classi);


            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            ClassiFiedId = arrayList.get(getAdapterPosition()).getItemID();
            Intent intent = new Intent(mContext, ClassifiedDetailsActivity.class);
            intent.putExtra("ClassiFiedId", ClassiFiedId);
            mContext.startActivity(intent);

        }
    }

}
