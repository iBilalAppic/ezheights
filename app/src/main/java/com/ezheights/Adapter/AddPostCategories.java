package com.ezheights.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.ezheights.Activity.MainActivity;
import com.ezheights.Models.AddPostCategory;
import com.ezheights.R;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by Bilal Khan on 21/4/17.
 */

public class AddPostCategories extends RecyclerView.Adapter<AddPostCategories.ViewHolder> {
    ArrayList<AddPostCategory> arrayList;
    private Context mContext;
    ArrayList<JSONArray> subCategoryModels = new ArrayList<>();

    String ClassiFiedId;

    public AddPostCategories(MainActivity mainActivity, ArrayList<AddPostCategory> arrayListPostClassifiedAdd) {

        this.mContext = mainActivity;
        this.arrayList = arrayListPostClassifiedAdd;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_list, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.catName.setText(arrayList.get(position).getCatName());

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView catName;

        public ViewHolder(View itemView) {
            super(itemView);
            catName = (TextView) itemView.findViewById(R.id.CategoryName);

        }
    }
}
