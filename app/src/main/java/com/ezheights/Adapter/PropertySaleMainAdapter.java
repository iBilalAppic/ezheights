package com.ezheights.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ezheights.Activity.LoginActivity;
import com.ezheights.Activity.MainActivity;
import com.ezheights.Activity.Property.AfterFilteredPropertyActivity;
import com.ezheights.Activity.Property.PropertySaleDetailsActivity;
import com.ezheights.Activity.Property.PropertySaleActivity;
import com.ezheights.Activity.SheduleVisitForm;
import com.ezheights.Models.PropertySaleModel;
import com.ezheights.R;
import com.ezheights.Utils.AppConstants;
import com.ezheights.Utils.AppPreferences;

import java.util.ArrayList;

/**
 * Created by Bilal on 22/3/17.
 */

public class PropertySaleMainAdapter extends RecyclerView.Adapter<PropertySaleMainAdapter.ViewHolder> {

    private Context mContext;
    String propId;
    ArrayList<PropertySaleModel> arrayList = new ArrayList<>();
    AppPreferences appPreferance;
    String isLogin, emailId;

    public PropertySaleMainAdapter(PropertySaleActivity propertySaleActivity, ArrayList<PropertySaleModel> arrayList) {
        this.mContext = propertySaleActivity;
        this.arrayList = arrayList;

    }

    public PropertySaleMainAdapter(AfterFilteredPropertyActivity filteredDataActivitySale, ArrayList<PropertySaleModel> arrayList) {

        this.mContext = filteredDataActivitySale;
        this.arrayList = arrayList;

    }

    @Override
    public PropertySaleMainAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_detail_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PropertySaleMainAdapter.ViewHolder holder, int position) {

        holder.proprtyTitle.setText(arrayList.get(position).getProperty_name());
        Glide.with(mContext).load(arrayList.get(position).getProperty_image())
                .thumbnail(0.5f)
                .crossFade()
                .placeholder(R.drawable.watermark_property)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.propertyImage);
        holder.propertPrice.setText("AED " + arrayList.get(position).getProperty_price());
        holder.noOfBed.setText(arrayList.get(position).getProperty_no_bed());
        holder.noOfBath.setText(arrayList.get(position).getProperty_no_bath());
        holder.areaSize.setText(arrayList.get(position).getProperty_area());

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private RelativeLayout rL_Main;
        private LinearLayout lL_Shedule_Visit, lL_ImageLayout;
        ImageView propertyImage, likeStauts;
        TextView proprtyTitle, propertPrice, noOfBed, noOfBath, areaSize;


        public ViewHolder(View itemView) {
            super(itemView);

            appPreferance = new AppPreferences();
            isLogin = appPreferance.getPreferences(mContext, AppConstants.isLogin);
            emailId = appPreferance.getPreferences(mContext, AppConstants.emailId);
            rL_Main = (RelativeLayout) itemView.findViewById(R.id.rL_Main);
            lL_ImageLayout = (LinearLayout) itemView.findViewById(R.id.lL_One);
            lL_Shedule_Visit = (LinearLayout) itemView.findViewById(R.id.lL_Three);
            propertyImage = (ImageView) itemView.findViewById(R.id.propertyImage);
            likeStauts = (ImageView) itemView.findViewById(R.id.likeStatus);
            proprtyTitle = (TextView) itemView.findViewById(R.id.tv_titlee);
            propertPrice = (TextView) itemView.findViewById(R.id.tv_Price);
            noOfBed = (TextView) itemView.findViewById(R.id.tv_BedCount);
            noOfBath = (TextView) itemView.findViewById(R.id.tv_BathCount);
            areaSize = (TextView) itemView.findViewById(R.id.tv_AreaCount);


            lL_ImageLayout.setOnClickListener(this);
            lL_Shedule_Visit.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

            if (v == lL_ImageLayout) {
                propId = arrayList.get(getAdapterPosition()).getProperty_id();
                Intent intent = new Intent(mContext, PropertySaleDetailsActivity.class);
                intent.putExtra("PropertyId", propId);
                mContext.startActivity(intent);
            }
            if (v == lL_Shedule_Visit) {
                if (isLogin.equals("")) {
                    Intent intent = new Intent(mContext, LoginActivity.class);
                    mContext.startActivity(intent);
                } else {
                    Intent intent = new Intent(mContext, SheduleVisitForm.class);
                    intent.putExtra("emailId", emailId);
                    mContext.startActivity(intent);
                }
            }

        }
    }
}
