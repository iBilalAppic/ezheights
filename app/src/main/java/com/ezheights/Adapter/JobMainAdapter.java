package com.ezheights.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ezheights.Activity.Job.AfterFilterJob;
import com.ezheights.Activity.Job.JobActivity;
import com.ezheights.Activity.Job.JobDetailActivity;
import com.ezheights.Models.JobMainModel;
import com.ezheights.R;

import java.util.ArrayList;

/**
 * Created by Bilal Khan on 31/3/17.
 */

public class JobMainAdapter extends RecyclerView.Adapter<JobMainAdapter.ViewHolder> {
    private Context mContext;
    ArrayList<JobMainModel> arrayList = new ArrayList<>();
    String jobId;

    public JobMainAdapter(JobActivity jobActivity, ArrayList<JobMainModel> arrayList) {

        this.mContext = jobActivity;
        this.arrayList = arrayList;
    }

    public JobMainAdapter(AfterFilterJob afterFilterJob, ArrayList<JobMainModel> arrayList) {
        this.mContext = afterFilterJob;
        this.arrayList = arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_jobs_list, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.title.setText(arrayList.get(position).getTitle());
        holder.salary.setText("AED " + arrayList.get(position).getPrice());
        holder.expe.setText(arrayList.get(position).getNewexperiance());
        holder.qualifi.setText(arrayList.get(position).getNeweducation());
        holder.empType.setText(arrayList.get(position).getEmptype());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private RelativeLayout rL_Main;
        ImageView compnayImage;
        TextView title, salary, expe, qualifi, empType;

        public ViewHolder(View itemView) {
            super(itemView);

            rL_Main = (RelativeLayout) itemView.findViewById(R.id.rL_Main_Motor);
            compnayImage = (ImageView) itemView.findViewById(R.id.compnayImage);
            title = (TextView) itemView.findViewById(R.id.jobTitle);
            salary = (TextView) itemView.findViewById(R.id.tv_salary);
            expe = (TextView) itemView.findViewById(R.id.tv_exp);
            qualifi = (TextView) itemView.findViewById(R.id.tv_Qualification);
            empType = (TextView) itemView.findViewById(R.id.empType);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            jobId = arrayList.get(getAdapterPosition()).getJobid();
            Intent intent = new Intent(mContext, JobDetailActivity.class);
            intent.putExtra("jobId", jobId);
            mContext.startActivity(intent);

        }
    }
}
