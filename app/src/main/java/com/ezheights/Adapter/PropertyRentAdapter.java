package com.ezheights.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ezheights.Activity.MainActivity;
import com.ezheights.Activity.Property.PropertyRentDetailsActivity;
import com.ezheights.Models.PropertyRentModel;
import com.ezheights.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by NileshM on 22/3/17.
 */

public class PropertyRentAdapter extends RecyclerView.Adapter<PropertyRentAdapter.ViewHolder> {
    private ArrayList<PropertyRentModel> arrayListRent;
    private Context mContext;
    String propId;

    public PropertyRentAdapter(MainActivity mainActivity, ArrayList<PropertyRentModel> arrayListRent) {
        this.mContext = mainActivity;
        this.arrayListRent = arrayListRent;
    }

    @Override
    public PropertyRentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_property_sale, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PropertyRentAdapter.ViewHolder holder, int position) {

        Picasso.with(mContext).load(arrayListRent.get(position).getProperty_image()).placeholder(R.drawable.ezheights_logo).into(holder.iv_PropertySale);

        holder.tv_Title.setText(arrayListRent.get(position).getProperty_id());
        holder.tv_SubTitle.setText(arrayListRent.get(position).getProperty_name());

    }

    @Override
    public int getItemCount() {
        return arrayListRent.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_PropertySale;
        private TextView tv_Title, tv_SubTitle;
        RelativeLayout mainLL;

        public ViewHolder(View itemView) {
            super(itemView);
            mainLL = (RelativeLayout) itemView.findViewById(R.id.mainLL);
            iv_PropertySale = (ImageView) itemView.findViewById(R.id.iv_PropertySale);
            tv_Title = (TextView) itemView.findViewById(R.id.tv_Title);
            tv_SubTitle = (TextView) itemView.findViewById(R.id.tv_SubTitle);

            mainLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    propId = arrayListRent.get(getAdapterPosition()).getProperty_id();
                    Intent intent = new Intent(mContext, PropertyRentDetailsActivity.class);
                    intent.putExtra("PropertyId", propId);
                    mContext.startActivity(intent);
                }
            });
        }
    }
}
