package com.ezheights.Adapter;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ezheights.Activity.Property.PropertyRentDetailsActivity;
import com.ezheights.Activity.Property.PropertySaleDetailsActivity;
import com.ezheights.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Bilal Khan on 11/4/17.
 */

public class HomeViewPagerAdapter extends PagerAdapter {

    private ArrayList<Integer> IMAGES;
    private ArrayList<String> ImagesArray1 = new ArrayList<String>();
    private LayoutInflater inflater;
    private Context context;

    public HomeViewPagerAdapter(PropertySaleDetailsActivity applicationContext, ArrayList<String> ImagesArray1) {
        this.context = applicationContext;
        this.ImagesArray1 = ImagesArray1;
        inflater = LayoutInflater.from(context);
    }


    public HomeViewPagerAdapter(PropertyRentDetailsActivity applicationContext, ArrayList<String> ImagesArray1) {
        this.context = applicationContext;
        this.ImagesArray1 = ImagesArray1;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup view, int position, Object object) {
        view.removeView((View) object);
    }

    @Override
    public int getCount() {
        return ImagesArray1.size();
    }


    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.item_image_viewpager, view, false);

        assert imageLayout != null;
        final ImageView imageView = (ImageView) imageLayout
                .findViewById(R.id.image);


//        imageView.setImageResource(IMAGES.get(position));
        Picasso.with(context).load(ImagesArray1.get(position)).placeholder(R.drawable.watermark_property).into(imageView);


        view.addView(imageLayout, 0);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

}