package com.ezheights.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ezheights.Activity.MainActivity;
import com.ezheights.Activity.Property.PropertySaleDetailsActivity;
import com.ezheights.Models.PropertySaleModel;
import com.ezheights.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by NileshM on 22/3/17.
 */

public class PropertySaleAdapter extends RecyclerView.Adapter<PropertySaleAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<PropertySaleModel> arrayListSale;
    private String propId;


    public PropertySaleAdapter(MainActivity mainActivity, ArrayList<PropertySaleModel> arrayListSale) {
        this.mContext = mainActivity;
        this.arrayListSale = arrayListSale;

    }

    @Override
    public PropertySaleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_property_sale, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PropertySaleAdapter.ViewHolder holder, int position) {


        Picasso.with(mContext).load(arrayListSale.get(position).getProperty_image()).placeholder(R.drawable.ezheights_logo).into(holder.iv_PropertySale);

        holder.tv_Title.setText(arrayListSale.get(position).getProperty_id());
        holder.tv_SubTitle.setText(arrayListSale.get(position).getProperty_name());

    }

    @Override
    public int getItemCount() {
        return arrayListSale.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_PropertySale;
        private TextView tv_Title, tv_SubTitle;
        RelativeLayout mainLL;

        public ViewHolder(View itemView) {
            super(itemView);
            mainLL = (RelativeLayout) itemView.findViewById(R.id.mainLL);
            iv_PropertySale = (ImageView) itemView.findViewById(R.id.iv_PropertySale);
            tv_Title = (TextView) itemView.findViewById(R.id.tv_Title);
            tv_SubTitle = (TextView) itemView.findViewById(R.id.tv_SubTitle);

            mainLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    propId = arrayListSale.get(getAdapterPosition()).getProperty_id();
                    Intent intent = new Intent(mContext, PropertySaleDetailsActivity.class);
                    intent.putExtra("PropertyId", propId);
                    mContext.startActivity(intent);
                }
            });

        }
    }
}
