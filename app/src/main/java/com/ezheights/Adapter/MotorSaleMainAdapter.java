package com.ezheights.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ezheights.Activity.Motor.AfterFilteredMotor;
import com.ezheights.Activity.Motor.MotorSaleActivity;
import com.ezheights.Activity.Motor.MotorSaleDetailActivity;
import com.ezheights.Models.MotorSaleModel;
import com.ezheights.R;

import java.util.ArrayList;

/**
 * Created by Bilal Khan on 29/3/17.
 */

public class MotorSaleMainAdapter extends RecyclerView.Adapter<MotorSaleMainAdapter.ViewHolder> {

    ArrayList<MotorSaleModel> arrayList;
    private Context mContext;
    private String motorId;

    public MotorSaleMainAdapter(MotorSaleActivity motorSaleActivity, ArrayList<MotorSaleModel> arrayList) {

        this.mContext = motorSaleActivity;
        this.arrayList = arrayList;
    }

    public MotorSaleMainAdapter(AfterFilteredMotor afterFilteredMotor, ArrayList<MotorSaleModel> arrayList) {
        this.mContext = afterFilteredMotor;
        this.arrayList = arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_motor_list, parent, false);
        return new MotorSaleMainAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Glide.with(mContext).load(arrayList.get(position).getMotor_image())
                .thumbnail(0.5f)
                .crossFade()
                .placeholder(R.drawable.watermark_property)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.motorImage);
        holder.motorTitle.setText(arrayList.get(position).getMotor_title());
        if (arrayList.get(position).getMotor_price().equals("Ask for Price")) {
            holder.motorPrice.setText(arrayList.get(position).getMotor_price());
        } else {
            holder.motorPrice.setText("AED " + arrayList.get(position).getMotor_price() + ".00");
        }
        holder.motorColor.setText(arrayList.get(position).getMotor_Color());
        holder.noOfDoor.setText(arrayList.get(position).getNo_Of_Door());
        holder.manufactureYear.setText(arrayList.get(position).getManufac_Year());
        motorId = arrayList.get(position).getMotor_id();

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private RelativeLayout rL_Main;
        private LinearLayout lL_ImageLayout;
        ImageView motorImage, motorlikeStauts;
        TextView motorTitle, motorPrice, noOfDoor, motorColor, manufactureYear;

        public ViewHolder(View itemView) {
            super(itemView);
            rL_Main = (RelativeLayout) itemView.findViewById(R.id.rL_Main_Motor);
            lL_ImageLayout = (LinearLayout) itemView.findViewById(R.id.lL_One_Motor);
            motorImage = (ImageView) itemView.findViewById(R.id.motorImage);
            motorTitle = (TextView) itemView.findViewById(R.id.tv_title_motor);
            motorPrice = (TextView) itemView.findViewById(R.id.tv_Price_motor);
            noOfDoor = (TextView) itemView.findViewById(R.id.tv_NoOfDoor);
            motorColor = (TextView) itemView.findViewById(R.id.tv_MotorColor);
            manufactureYear = (TextView) itemView.findViewById(R.id.tv_ManfYearCount);


            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            motorId = arrayList.get(getAdapterPosition()).getMotor_id();
            Intent intent = new Intent(mContext, MotorSaleDetailActivity.class);
            intent.putExtra("motorId", motorId);
            mContext.startActivity(intent);


        }
    }
}
