package com.ezheights.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ezheights.Activity.Classified.ClassifiedDetailsActivity;
import com.ezheights.Activity.MainActivity;
import com.ezheights.Activity.Motor.MotorSaleDetailActivity;
import com.ezheights.Models.ClassifiedModel;
import com.ezheights.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by NileshM on 22/3/17.
 */

public class ClassifiedsAdapter extends RecyclerView.Adapter<ClassifiedsAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<ClassifiedModel> arrayListClassi;
    String ClassiFiedId;

    public ClassifiedsAdapter(MainActivity mainActivity, ArrayList<ClassifiedModel> arrayListClassi) {
        this.mContext = mainActivity;
        this.arrayListClassi = arrayListClassi;
    }

    @Override
    public ClassifiedsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_property_sale, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ClassifiedsAdapter.ViewHolder holder, int position) {

        Picasso.with(mContext).load(arrayListClassi.get(position).getImage()).placeholder(R.drawable.ezheights_logo).into(holder.iv_PropertySale);

        holder.tv_Title.setText(arrayListClassi.get(position).getItemID());
        holder.tv_SubTitle.setText(arrayListClassi.get(position).getTitle());

    }

    @Override
    public int getItemCount() {
        return arrayListClassi.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_PropertySale;
        private TextView tv_Title, tv_SubTitle;
        private RelativeLayout mainLL;

        public ViewHolder(View itemView) {
            super(itemView);
            mainLL = (RelativeLayout) itemView.findViewById(R.id.mainLL);
            iv_PropertySale = (ImageView) itemView.findViewById(R.id.iv_PropertySale);
            tv_Title = (TextView) itemView.findViewById(R.id.tv_Title);
            tv_SubTitle = (TextView) itemView.findViewById(R.id.tv_SubTitle);
            mainLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ClassiFiedId = arrayListClassi.get(getAdapterPosition()).getItemID();
                    Intent intent = new Intent(mContext, ClassifiedDetailsActivity.class);
                    intent.putExtra("ClassiFiedId", ClassiFiedId);
                    mContext.startActivity(intent);
                }
            });
        }
    }
}
