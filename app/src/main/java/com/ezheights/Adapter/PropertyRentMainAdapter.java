package com.ezheights.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ezheights.Activity.Property.PropertyRentActivity;
import com.ezheights.Activity.Property.PropertyRentDetailsActivity;
import com.ezheights.Activity.SheduleVisitForm;
import com.ezheights.Models.PropertyRentModel;
import com.ezheights.R;

import java.util.ArrayList;

/**
 * Created by Bilal Khan on 30/3/17.
 */

public class PropertyRentMainAdapter extends RecyclerView.Adapter<PropertyRentMainAdapter.ViewHolder> {

    private Context mContext;
    ArrayList<PropertyRentModel> arrayList = new ArrayList<>();
    String propId;

    public PropertyRentMainAdapter(PropertyRentActivity propertyRentActivity, ArrayList<PropertyRentModel> arrayList) {
        this.mContext = propertyRentActivity;
        this.arrayList = arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_propert_rent, parent, false);
        return new PropertyRentMainAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.proprtyTitle.setText(arrayList.get(position).getProperty_name());
        Glide.with(mContext).load(arrayList.get(position).getProperty_image())
                .thumbnail(0.5f)
                .crossFade()
                .placeholder(R.drawable.watermark_property)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.propertyImage);
        holder.propertPrice.setText("AED " + arrayList.get(position).getProperty_price());
        holder.noOfBed.setText(arrayList.get(position).getProperty_no_bed());
        holder.noOfBath.setText(arrayList.get(position).getProperty_no_bath());
        holder.areaSize.setText(arrayList.get(position).getProperty_area());

        final Boolean selected = true;

        holder.likeStauts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selected == true) {
                    holder.likeStauts.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_like_red));
                    Toast.makeText(mContext, "Liked", Toast.LENGTH_SHORT).show();
                } else if (selected == false) {
                    holder.likeStauts.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_like_icon));
                    Toast.makeText(mContext, "Not Liked", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private RelativeLayout rL_Main;
        private LinearLayout lL_Shedule_Visit, lL_ImageLayout;
        ImageView propertyImage, likeStauts;
        TextView proprtyTitle, propertPrice, noOfBed, noOfBath, areaSize;

        public ViewHolder(View itemView) {
            super(itemView);

            rL_Main = (RelativeLayout) itemView.findViewById(R.id.rL_Main);
            lL_ImageLayout = (LinearLayout) itemView.findViewById(R.id.lL_One);
            lL_Shedule_Visit = (LinearLayout) itemView.findViewById(R.id.lL_Three);
            propertyImage = (ImageView) itemView.findViewById(R.id.property_rent_Image);
            likeStauts = (ImageView) itemView.findViewById(R.id.likedStatus);
            proprtyTitle = (TextView) itemView.findViewById(R.id.tv_rent_title);
            propertPrice = (TextView) itemView.findViewById(R.id.tv_rent_Price);
            noOfBed = (TextView) itemView.findViewById(R.id.tv_rent_BedCount);
            noOfBath = (TextView) itemView.findViewById(R.id.tv_rent_BathCount);
            areaSize = (TextView) itemView.findViewById(R.id.tv_rent_AreaCount);

            lL_ImageLayout.setOnClickListener(this);
            lL_Shedule_Visit.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (v == lL_ImageLayout) {
                propId = arrayList.get(getAdapterPosition()).getProperty_id();
                Intent intent = new Intent(mContext, PropertyRentDetailsActivity.class);
                intent.putExtra("PropertyId", propId);
                mContext.startActivity(intent);
            }
            if (v == lL_Shedule_Visit) {
                Intent intent = new Intent(mContext, SheduleVisitForm.class);
                mContext.startActivity(intent);
            }
        }
    }
}
