package com.ezheights.Utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by NileshM on 24/3/17.
 */

public class AppPreferences {

    public void setPreferences(Context context, String key, String value) {

        SharedPreferences.Editor editor = context.getSharedPreferences("EZHeights", Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();

    }


    public  String getPreferences(Context context, String key) {

        SharedPreferences prefs = context.getSharedPreferences("EZHeights",	Context.MODE_PRIVATE);
        String position = prefs.getString(key, "");
        return position;
    }


    public void clearSharedPreference(Context context) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings = context.getSharedPreferences("EZHeights", Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.clear();
        editor.commit();
    }

}
