package com.ezheights.Utils;

/**
 * Created by Bilal Khan on 28/3/17.
 */

public class AppConstants {

    //    http://35.154.160.221/mobileService.php?servicename=SaleProperty
    public static final String Main_Url = "http://35.154.160.221/mobileService.php?servicename=";

    public static final String HomeData = "http://35.154.160.221/appservice/home_content?servicename=home_content";

    public static final String PropertyForSaleData = "http://35.154.160.221/mobileService.php?servicename=SaleProperty";
    public static final String PropertyForRentData = "http://35.154.160.221/mobileService.php?servicename=RentProperty";
    public static final String PropertyDetailsData = "http://35.154.160.221/appservice/propertyDetail?servicename=propertyDetail";
    public static final String PrePropertyFilter = "http://35.154.160.221/mobileService.php?servicename=filter_content";
    public static final String ApplyPropertyFilter = "http://35.154.160.221/mobileService.php?servicename=filter_apply";


    public static final String LoadMoreService = "http://35.154.160.221/mobileService.php?servicename=load_more";


    public static final String MotorSaleData = "http://35.154.160.221/appservice/motors_default?servicename=motors_defaultcontent";
    public static final String MotorDetailsData = "http://35.154.160.221/appservice/motorsDetail?servicename=motorsDetail";
    public static final String PreMotorFilter = "http://35.154.160.221/appservice/motors_filter?servicename=motorsFiltercontent";
    public static final String ApplyMotorFilter = "http://35.154.160.221/appservice/motorsfilter_apply?servicename=filterApply";


    public static final String ClassifiedSaleData = "http://35.154.160.221/appservice/classifieds_default?servicename=classifieds_defaultcontent";
    public static final String ClassifiedDetailsData = "http://35.154.160.221/appservice/classifiedsDetail?servicename=classifieds_detail";
    public static final String PreClassifiedFilter = "http://35.154.160.221/appservice/classifieds_filter?servicename=classifieds_filtercontent";
    public static final String ApplyClassifiedFilter = "http://35.154.160.221/appservice/classifiedsfilter_apply?servicename=filterApply";


    public static final String JobDefaultData = "http://35.154.160.221/appservice/jobs_default?servicename=jobsDefaultcontent";
    public static final String JobDetailsData = "http://35.154.160.221/appservice/jobsDetail?servicename=jobsDetail";
    public static final String PreJobFilter = "http://35.154.160.221/appservice/jobs_filter?servicename=jobs_filtercontent";
    public static final String ApplyJobFilter = "http://35.154.160.221/appservice/jobsfilter_apply?servicename=filterApply";

    public static final String Registration = "http://35.154.160.221/appservice/registration?servicename=registration";
    public static final String Login = "http://35.154.160.221/appservice/login?servicename=login";


    /*AddPost Service*/
    public static final String GetCategoriesForAddPost = "http://35.154.160.221/appservice/category?servicename=Categories";


    public static final String isLogin = "login";
    public static String emailId;
    public static String loginType;

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    public static final String TWITTER_KEY = "uD0J8HvU96YGG8VP6a773kFoc";
    public static final String TWITTER_SECRET = "CcsIEVYEvdV5IpWA3LcLqqgtldWDCKmQfbViMnxQYh1OyavQ25";
//        TwitterAuthConfig authConfig = new TwitterAuthConfig(AppConstants.TWITTER_KEY, AppConstants.TWITTER_SECRET);
//        Fabric.with(this, new TwitterCore(authConfig), new TweetComposer());


}
